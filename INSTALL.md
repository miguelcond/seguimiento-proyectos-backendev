# Manual de instalación - Sistema de Seguimiento a Proyectos Productivos (backend) para Debian Jessie|Stretch

## Paquetes requeridos

| Paquete | Descripción |
| ---------- | ---------- |
| Node.js  | Node.js javascript del lado de servidor, versión LTS 6.9.5 o superior|
| Postgresql | Base de datos PostgreSQL v9.4 o superior |
| git | Software para control de versiones |
| npm | Manejador de paquetes por defecto para Node.js, versión 3.10.5 |
| nvm | Manejador de versiones de Node.js |

## Instalación de utilitarios y herramientas

```sh
sudo apt-get install build-essential
sudo apt-get install curl wget git make
```
## Instalación de programas externos.

### wkhtmltopdf (Jessie)

Instalar paquetes requeridos para `wkhtmltopdf`.

```sh
sudo apt-get install libxfont1 xfonts-75dpi \
xfonts-base xfonts-encodings xfonts-utils fontconfig \
libfontconfig1 libjpeg62-turbo libx11-6 libxext6 libxrender1
```

Descargar el programa [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html). O descargar con wget.

```sh
wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.2.1/wkhtmltox-0.12.2.1_linux-jessie-amd64.deb
```
Luego de tener el paquete descargado, instalar con:

```
sudo dpkg -i wkhtmltox-0.12.2.1_linux-jessie-amd64.deb
```

### wkhtmltopdf (Stretch)

Instalar paquetes requeridos para `wkhtmltopdf`.

```sh
sudo apt-get install -y openssl build-essential libssl-dev libxrender-dev git-core libx11-dev libxext-dev libfontconfig1-dev libfreetype6-dev fontconfig

wget -c https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz

tar xvf wkhtmltox*.tar.xz
sudo mv wkhtmltox/bin/wkhtmlto* /usr/bin
```

## Definición de usuario donde se instalará el proyecto (Opcional).

La instalación se realizá sobre un usuario del sistema.

Si se requiere se puede adicionar e instalar sobre el usuario `nodejs`.

```sh
sudo adduser nodejs
sudo adduser nodejs sudo
sudo su - nodejs
```

Luego todas las operaciones se realizarán sobre el usuario definido.

## Instalación de nvm (node version manager)

Instalación de [nvm](https://github.com/creationix/nvm)

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
```

Actualizar variables de entorno.
```
. /home/nodejs/.bashrc
```

Instalación de node.js LTS versión 6.9.5 utilizando `nvm`.

```sh
nvm install 6.9.5
```

Verificamos con:

```sh
node -v
```
## Paquetes NPM

Instalar de manera global:
```
npm install --global sequelize sequelize-cli pg
```

## Instalación del proyecto

Clonar el código fuente desde el repositorio

```sh
git clone -b FDI-master https://gitlab.geo.gob.bo/agetic/seguimiento-proyectos-backend.git  seguimiento-proyectos-backend
```

Es posible que al descargar el proyecto con HTTPs, nos lance el siguiente error:

```sh
Cloning into 'nombre-del-proyecto'...
fatal: unable to access 'https://url-del-proyecto.git/': server certificate verification failed. CAfile: /etc/ssl/certs/ca-certificates.crt CRLfile: none
```
Configurar lo siguiente e intentar nuevamente la clonación:
```sh
git config --global http.sslverify false
git clone -b FDI-master https://gitlab.geo.gob.bo/agetic/seguimiento-proyectos-backend.git  seguimiento-proyectos-backend
```

Ingresar a la carpeta:
```sh
cd seguimiento-proyectos-backend
```

Verificar que se encuentra en la rama `master` ó en el último tag correspondiente por ejemplo `FDI-v1.0.0`

```sh
git branch
```
Si se encuentra en otra rama, para cambiar a la rama `master` ejecutar:

```sh
git checkout master
```

Ó para cambiar al tag `FDI-v1.0.0` ejecutar:

```sh
git checkout FDI-v1.0.0
```

Instalar las dependencias npm
```sh
npm install
```

## Instalación de la base de datos

Instalación de postgres

```sh
sudo apt-get install postgresql postgresql-contrib
```

> Nota. Puede ser necesario cambiar el estilo de formato de fechas a dd/mm/yyyy
> Ver el archivo de configuración de postgres `postgresql.conf`.
> datestyle = 'iso, dmy'


## Instalación de postgis (Jessie)

```sh
sudo apt-get install postgis postgresql-9.4-postgis-2.1
sudo /etc/init.d/postgresql restart
```

## Instalación de postgis (Stretch)

```sh
sudo apt-get install postgis postgresql-9.6-postgis-2.3
sudo /etc/init.d/postgresql restart
```


## Configuración de la base de datos
Crear la extensión postgis

```sh
sudo -u postgres psql -d template1 -c "CREATE EXTENSION IF NOT EXISTS postgis;"
```
Crear usuario para la base de datos con una contraseña:

```sh
sudo -u postgres psql -d template1 -c "CREATE USER seguimiento WITH PASSWORD '******';"
```
Crear la base de datos para seguimiento con todos los privilegios:

```sh
sudo -u postgres psql -d template1 -c "CREATE DATABASE seguimiento_fdi OWNER seguimiento;"
```

## Configuraciones

### Conexión a la base de datos

Replicar archivo de configuración para la conexión a la base de datos.
```sh
cd seguimiento-proyectos-backend
cp src/config/config-example.json src/config/config.json
```
Editar el archivo `src/config/config.json`, modificar las siguientes configuraciones:

* username - nombre de usuario de la base de datos
* password - contraseña del usuario de la base de datos
* database - nombre de la base de datos
* host - servidor donde se encuentra la base de datos
* demás variables

>**NOTA.-** En caso de poner un host distinto a localhost o 127.0.0.1, tomar en cuenta que debe [configurar postgreSQL para habilitar el acceso a conexiones remotas](http://www.evaristogz.com/instalacion-postgresql-acceso-remoto-externo/).

### Interoperabilidad y configuraciones generales

La configuración se define en el archivo : `src/config/config.js`, debe realizar una copia del archivo de configuración de ejemplo `src/config/config-example.js`.

```sh
cp src/config/config-example.js src/config/config.js
```

#### Segip

En el archivo de configuración editar los valores de la siguiente sección.

```
...
segip: {
  url: 'https://interoperabilidad.agetic.gob.bo',
  path: '/fake/segip/v2/',
  credenciales: {
    apikey: 'key'
  },
  tokenKong: 'Bearer <-- solicitar el token a interoperabilidad -->'
}
...
```

#### Envío de Correos

Configuración para envío de correos desde el sistema.

```
...
correo: {
      host: "smtp.dominio.gob.bo",
      port: 587,
      secure: false,
      ignoreTLS: false,
      tls: { rejectUnauthorized: false },
      auth: {
        user: "ejemplo@dominio.gob.bo",
        pass: "contraseña-secreta-de-ejemplo"
      }
    },
...
```

#### Configuración de parámetros del sistema

```
...
sistema: {
  urlBase: 'https://dominio.gob.bo/fdi/#!',
  urlLogin: 'https://dominio.gob.bo/fdi/#!/login',
  urlLogo: 'https://dominio.gob.bo/fdi-api/imagen/logo-fdi.jpg',
  origen: 'SSP <ssp@dominio.gob.bo>',
  soporte: ['user@dominio.gob.bo']
}
...
```

* *urlBase*: Url base del frontend seguimiento de proyectos
* *urlLogin*: Url de la vista de login del sistema de seguimiento de proyectos
* *urlLogo*: Url donde está ubicado el logo de la institución
* *origen*: Nombre del origen de los correos ej. SSP
* *soporte*: Nombre del correo remitente para los correos enviados desde el sistema


## Ejecución de aplicación

### Iniciar la aplicación

Las opciones de ejecución son las siguientes:
+ Genera o regenera las tablas necesarias en la base de datos y ejecuta los seeders y migrations para el entorno de producción.

```sh
npm run setup         # desarrollo
npm run setup --prod  #  produccion
```

+ Levantar el sistema en entorno de producción.

Se debe realizar con una de las siguientes opciones

## Configuración con PM2 (opcion 1)

Instalar el gestor de procesos PM2 con el siguiente comando

```sh
$ npm install pm2 -g
```

#### Iniciar y detener procesos

Instalar el interprete babel utilizado
```sh
$ npm install -g babel-cli
```

##### Iniciar en modo desarrollo

Iniciar la aplicación Backend con nombre `seguimiento-api`  (En modo desarrollo no se debe iniciar con instancias)
```sh
$ pm2 start --interpreter babel-node index.js --name seguimiento-api
```

> Recuerda que estos comandos solo trabajan en modo `fork_mode`. Para ejecutar en modo cluster puedes utilizar la siguiente alternativa.

##### Iniciar utilizando Require (produccion)

Crear un archivo `server.js` con el siguiente contenido
```sh
// server.js
require('babel-register');
require('./index.js');
```

Ahora puedes ejecutar la aplicación con nombre `seguimiento-api`
```sh
$ env NODE_ENV=production pm2 start server.js --name seguimiento-api
```

ó Iniciar la aplicación Backend con 2 instancias, y nombre de la aplicación `seguimiento-api`
```sh
$ env NODE_ENV=production pm2 start server.js -i 2 --name seguimiento-api
```

##### Detener un procesos

Para detener el proceso se usa el comando `stop`
```sh
$ pm2 stop seguimiento-api
```

Para reiniciarlo el comando `restart`
```sh
$ pm2 restart seguimiento-api
```

#### Mantenimiento de los procesos

Para mostrar la información de los procesos se utiliza el comando `list`
```sh
$ pm2 list
```

Para mostrar los últimos logs se utiliza el comando `log`
```sh
$ pm2 log
```

#### Generación del script de reinicio automático cuando se enciende el servidor

Para generar los scripts de arranque se utiliza el siguiente comando. Y luego leer y ejecutar el comando que se genera
```sh
$ pm2 startup
```

Guardar la lista de procesos para reiniciar el servidor con el siguiente comando
```sh
$ pm2 save
```

Eliminar los scripts de inicio con e siguiente comando
```sh
$ pm2 unstartup systemd
```

## Configuración con supervisor (opcion 2)

Instalar supervisord

```
sudo apt-get install supervisor
```

Configuración de supervisor para servicio.

Creamos un archivo de configuración en el directorio: `/etc/supervisor/conf.d`. El archivo debe tener extensión `.conf`. Un ejemplo de configuración para el despliegue de la aplicación guardado en el archivo: `/etc/supervisor/conf.d/seguimiento-fdi-api.conf` es:

```
[program:seguimiento-fdi-api]
command=/home/nodejs/.nvm/versions/node/v6.9.5/bin/npm start --prod
directory=/home/nodejs/seguimiento-proyectos-backend/
process_name=seguimiento-fdi-api
autostart=true
autorestart=true
startsecs=1
stopwaitsecs=1
stdout_logfile=/home/nodejs/seguimiento-proyectos-backend/log/produccion.log
stderr_logfile=/home/nodejs/seguimiento-proyectos-backend/log/produccion.log
user=nodejs
```

Tomar encuenta que `/home/nodejs/...`, el valor de `usuario` debe ser reemplazado por el usuario en el que se inicio la instalación.

Los parámetros de configuración son:
* [program:seguimiento-fdi-api], define el nombre del programa("seguimiento-fdi-api").
* command, comando que se ejecuta. Este parámetro es configurado con una versión de node.js instalado con `n`.
* directory, directorio donde se ejecuta el comando anterior.
* process_name, nombre del proceso, en este caso "seguimiento-fdi-api", notar que tiene el mismo nombre del programa.
* autostart, se indica que el programa debe ser iniciado cuando se reinicia el sistema, true se inicia automaticamente y false para inicio manual.
* autorestart, define cómo supervisor debe gestionar en caso de que el programa haya dejado de funcionar. true, indica que se reinicia el programa cuando el mismo deje de funcionar, false no reinicia cuando programa no este funcionando.
* startsecs, Segundos que debe permanecer ejecutándose el servicio para que se considere que ha sido lanzado correctamente.
* stopwaitsecs, especifica cuánto tiempo supervisord debe esperar al programa, antes de darse por vencido y envió de una señal SIGKILL.
* stdout_logfile, dirección de archivo de logs del programa. Archivo debe estar creado antes de iniciar el programa.
* stderr_logfile, dirección de archivo de logs de error del programa. Archivo debe estar creado antes de iniciar el programa.
* user, usuario que ejecutará el programa.

Para más información sobre los parámetros de configuración ver [supervisor-configuración](http://supervisord.org/configuration.html).

Luego hacemos saber a supervisor del programa configurado
```sh
sudo supervisorctl reread
```

Activar modificaciones de configuración del programa. Ejecutar este comando cuando se realiza cualquier modificación sobre el archivo de configuración `.conf` en este caso `seguimiento-fdi-api`.

```sh
sudo supervisorctl update
```

Con esto ya se tiene desplegado el servicio, se puede revisar con `sudo supervisorctl`, o  `tail -f seguimiento-proyectos-backend/log/produccion.log`(dependiendo del directorio donde se instaló).

Parar programa
```sh
sudo supervisorctl stop seguimiento-fdi-api
```

Iniciar programa
```sh
sudo supervisorctl start seguimiento-fdi-api
```
Parar programa
```sh
sudo supervisorctl stop seguimiento-fdi-api
```
Iniciar programa
```sh
sudo supervisorctl start seguimiento-fdi-api
```

Reiniciar "supervisor"
Cuando se hagan cambios y se requiere reiniciar el servicio "supervisor" para que se ejecute la aplicación:
```sh
sudo /etc/init.d/supervisor restart
```
Para verificar que la aplicación este efectivamente corriendo, se puede ejecutar el siguiente comando, y verificar que la aplicación este corriendo en el puerto configurado:
```sh
netstat -ltpn

Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      -               
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -               
tcp        0      0 0.0.0.0:5432            0.0.0.0:*               LISTEN      -               
tcp6       0      0 :::4000                 :::*                    LISTEN      32274/nodejs
```

O se puede revisar las tareas del `supervisor`, buscar el nombre de la tarea y su respectivo estado:

```sh
sudo supervisorctl
```
```sh
seguimiento-fdi-api                   RUNNING    pid 4617, uptime 3 days, 21:41:05
```

#### Cosas que se deben tomar en cuenta

##### Reconfigurar TimeZone

Verificar TimeZone

```sh
date +"%Z %z"
```
Cambiar timezone

```sh
$ sudo timedatectl set-timezone "America/La_Paz"
$ sudo dpkg-reconfigure tzdata
```

Seleccionar America/La_Paz

##### Verificar TimeZone de PostgreSQL

Editar el archivo `/etc/postgresql/9.4/main/postgresql.conf` y revizar los siguientes valores

```
# - Locale and Formatting -

datestyle = 'iso, dmy'
timezone = 'localtime'

log_timezone = 'localtime'
```

Reiniciar el servicio de postgresql si se modificó algún dato

```
$ sudo service postgresql restart
```
