// 'use strict';

module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert('provincia', [{
      "codigo": "0101",
      "nombre": "Oropeza",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0102",
      "nombre": "Azurduy",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0103",
      "nombre": "Zudáñez",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0104",
      "nombre": "Tomina",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0105",
      "nombre": "Hernando Siles",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0106",
      "nombre": "Yamparáez",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0107",
      "nombre": "Nor Cinti",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0108",
      "nombre": "Belisario Boeto",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0109",
      "nombre": "Sur Cinti",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0110",
      "nombre": "Luis Calvo",
      "codigo_departamento": "01",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0201",
      "nombre": "Murillo",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0202",
      "nombre": "Omasuyos",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0203",
      "nombre": "Pacajes",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0204",
      "nombre": "Camacho",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0205",
      "nombre": "Muñecas",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0206",
      "nombre": "Larecaja",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0207",
      "nombre": "Franz Tamayo",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0208",
      "nombre": "Ingavi",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0209",
      "nombre": "Loayza",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0210",
      "nombre": "Inquisivi",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0211",
      "nombre": "Sud Yungas",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0212",
      "nombre": "Los Andes",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0213",
      "nombre": "Aroma",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0214",
      "nombre": "Nor Yungas",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0215",
      "nombre": "Abel Iturralde",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0216",
      "nombre": "Bautista Saavedra",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0217",
      "nombre": "Manco Kapac",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0218",
      "nombre": "Gualberto Villarroel",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0219",
      "nombre": "General José Manuel Pando",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0220",
      "nombre": "Caranavi",
      "codigo_departamento": "02",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0301",
      "nombre": "Cercado",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0302",
      "nombre": "Campero",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0303",
      "nombre": "Ayopaya",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0304",
      "nombre": "Esteban Arze",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0305",
      "nombre": "Arani",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0306",
      "nombre": "Arque",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0307",
      "nombre": "Capinota",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0308",
      "nombre": "Germán Jordán",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0309",
      "nombre": "Quillacollo",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0310",
      "nombre": "Chapare",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0311",
      "nombre": "Tapacarí",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0312",
      "nombre": "Carrasco",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0313",
      "nombre": "Mizque",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0314",
      "nombre": "Punata",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0315",
      "nombre": "Bolivar",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0316",
      "nombre": "Tiraque",
      "codigo_departamento": "03",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0401",
      "nombre": "Cercado",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0402",
      "nombre": "Abaroa",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0403",
      "nombre": "Carangas",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0404",
      "nombre": "Sajama",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0405",
      "nombre": "Litoral",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0406",
      "nombre": "Poopó",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0407",
      "nombre": "Pantaleón Dalence",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0408",
      "nombre": "Ladislao Cabrera",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0409",
      "nombre": "Sabaya",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0410",
      "nombre": "Saucarí",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0411",
      "nombre": "Tomás Barrón",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0412",
      "nombre": "Sur Carangas",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0413",
      "nombre": "San Pedro de Totora",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0414",
      "nombre": "Sebastián Pagador",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0415",
      "nombre": "Mejillones",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0416",
      "nombre": "Nor Carangas",
      "codigo_departamento": "04",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0501",
      "nombre": "Tomás Frías",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0502",
      "nombre": "Rafael Bustillo",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0503",
      "nombre": "Cornelio Saavedra",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0504",
      "nombre": "Chayanta",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0505",
      "nombre": "Chárcas",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0506",
      "nombre": "Nor Chichas",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0507",
      "nombre": "Alonso de Ibáñez",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0508",
      "nombre": "Sur Chichas",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0509",
      "nombre": "Nor Lipez",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0510",
      "nombre": "Sur Lipez",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0511",
      "nombre": "José María Linares",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0512",
      "nombre": "Antonio Quijarro",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0513",
      "nombre": "General Bilbao",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0514",
      "nombre": "Daniel Campos",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0515",
      "nombre": "Modesto Omiste",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0516",
      "nombre": "Enrique Baldivieso",
      "codigo_departamento": "05",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0601",
      "nombre": "Cercado",
      "codigo_departamento": "06",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0602",
      "nombre": "Arce",
      "codigo_departamento": "06",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0603",
      "nombre": "Gran Chaco",
      "codigo_departamento": "06",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0604",
      "nombre": "Avilés",
      "codigo_departamento": "06",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0605",
      "nombre": "Méndez",
      "codigo_departamento": "06",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0606",
      "nombre": "O'Connor",
      "codigo_departamento": "06",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0701",
      "nombre": "Andrés Ibáñez",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0702",
      "nombre": "Warnes",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0703",
      "nombre": "Velasco",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0704",
      "nombre": "Ichilo",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0705",
      "nombre": "Chiquitos",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0706",
      "nombre": "Sara",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0707",
      "nombre": "Cordillera",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0708",
      "nombre": "Vallegrande",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0709",
      "nombre": "Florida",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0710",
      "nombre": "Obispo Santistéban",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0711",
      "nombre": "Ñuflo De Chávez",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0712",
      "nombre": "Angel Sandoval",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0713",
      "nombre": "Manuel María Caballero",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0714",
      "nombre": "Germán Busch",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0715",
      "nombre": "Guarayos",
      "codigo_departamento": "07",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0801",
      "nombre": "Cercado",
      "codigo_departamento": "08",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0802",
      "nombre": "Vaca Diez",
      "codigo_departamento": "08",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0803",
      "nombre": "General José Ballivián",
      "codigo_departamento": "08",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0804",
      "nombre": "Yacuma",
      "codigo_departamento": "08",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0805",
      "nombre": "Moxos",
      "codigo_departamento": "08",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0806",
      "nombre": "Marbán",
      "codigo_departamento": "08",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0807",
      "nombre": "Mamoré",
      "codigo_departamento": "08",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0808",
      "nombre": "Iténez",
      "codigo_departamento": "08",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0901",
      "nombre": "Nicolás Suárez",
      "codigo_departamento": "09",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0902",
      "nombre": "Manuripi",
      "codigo_departamento": "09",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0903",
      "nombre": "Madre de Dios",
      "codigo_departamento": "09",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0904",
      "nombre": "Abuná",
      "codigo_departamento": "09",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, {
      "codigo": "0905",
      "nombre": "Federico Román",
      "codigo_departamento": "09",
      "_usuario_creacion": 1,
      "_usuario_modificacion": 1,
      "_fecha_creacion": new Date(),
      "_fecha_modificacion": new Date()
    }, ], {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
