// 'use strict';

module.exports = {
  up(queryInterface) {
    if (process.env.CREATEUSERS) {
      const usuariosRoles = [
        {fid_usuario: 1, fid_rol: 1},
        // {fid_usuario: 2, fid_rol: 7},
        // {fid_usuario: 3, fid_rol: 7},
        // {fid_usuario: 4, fid_rol: 7},
        // {fid_usuario: 5, fid_rol: 6},
        // {fid_usuario: 6, fid_rol: 6},
        // {fid_usuario: 7, fid_rol: 6},
        // {fid_usuario: 8, fid_rol: 3},
        // {fid_usuario: 9, fid_rol: 3},
        // {fid_usuario: 10, fid_rol: 3},
        // {fid_usuario: 11, fid_rol: 4},
        // {fid_usuario: 12, fid_rol: 4},
        // {fid_usuario: 13, fid_rol: 4},
        // {fid_usuario: 14, fid_rol: 4},
        // {fid_usuario: 15, fid_rol: 4},
        // {fid_usuario: 16, fid_rol: 4},
        // {fid_usuario: 17, fid_rol: 4},
        // {fid_usuario: 18, fid_rol: 4},
        // {fid_usuario: 19, fid_rol: 4},
        // {fid_usuario: 20, fid_rol: 5},
        // {fid_usuario: 21, fid_rol: 5},
        // {fid_usuario: 22, fid_rol: 5},
        // {fid_usuario: 23, fid_rol: 5},
        // {fid_usuario: 24, fid_rol: 5},
        // {fid_usuario: 25, fid_rol: 5},
        // {fid_usuario: 26, fid_rol: 5},
        // {fid_usuario: 27, fid_rol: 5},
        // {fid_usuario: 28, fid_rol: 5},
        // {fid_usuario: 29, fid_rol: 5},

        {fid_usuario: 2, fid_rol: 11},
        {fid_usuario: 3, fid_rol: 12},
        {fid_usuario: 4, fid_rol: 13},
        {fid_usuario: 5, fid_rol: 13},
        {fid_usuario: 6, fid_rol: 14},
        {fid_usuario: 7, fid_rol: 15},
        // {fid_usuario: 8, fid_rol: 16},
      ];
      for (let i in usuariosRoles) {
        usuariosRoles[i].estado = 'ACTIVO';
        usuariosRoles[i]._usuario_creacion = '1';
        usuariosRoles[i]._fecha_creacion = new Date();
        usuariosRoles[i]._fecha_modificacion = new Date();
      }
      return queryInterface.bulkInsert('usuario_rol', usuariosRoles, {});
    } else {
      return queryInterface.bulkInsert('usuario_rol', [
        {
          fid_usuario: 1,
          fid_rol: 1,
          estado: 'ACTIVO',
          _usuario_creacion: '1',
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date()
        }
      ], {});
    }
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
