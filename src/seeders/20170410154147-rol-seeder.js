// 'use strict';

module.exports = {
  up(queryInterface) {
    const roles = [
      { id_rol: 1, nombre: 'ADMIN', descripcion: 'Administrador', peso: 0 },
      { id_rol: 2, nombre: 'JEFE_TECNICO_UPRE', descripcion: 'Jefe Técnico', peso: 0, estado: 'INACTIVO' },
      { id_rol: 3, nombre: 'ENCAR_REGIONAL_UPRE', descripcion: 'Encargado Regional', peso: 0, estado: 'INACTIVO' },
      { id_rol: 4, nombre: 'RESP_DEPARTAMENTAL_UPRE', descripcion: 'Responsable Departamental', peso: 0, estado: 'INACTIVO' },
      { id_rol: 5, nombre: 'TECNICO_UPRE', descripcion: 'Técnico', peso: 0, estado: 'INACTIVO' },
      { id_rol: 6, nombre: 'FINANCIERO_UPRE', descripcion: 'Técnico Financiero', peso: 0, estado: 'INACTIVO' },
      { id_rol: 7, nombre: 'LEGAL_UPRE', descripcion: 'Técnico Legal', peso: 0, estado: 'INACTIVO' },
      { id_rol: 8, nombre: 'EMPRESA', descripcion: 'Empresa', peso: 0, estado: 'INACTIVO' },
      { id_rol: 9, nombre: 'SUPERVISOR', descripcion: 'Supervisor', peso: 0, estado: 'INACTIVO' },
      { id_rol: 10, nombre: 'FISCAL', descripcion: 'Fiscal', peso: 0, estado: 'INACTIVO' },

      { id_rol: 11, nombre: 'RESP_RECEPCION_FDI', descripcion: 'Responsable de Recepción', peso: 0 },
      { id_rol: 12, nombre: 'JEFE_REVISION_FDI', descripcion: 'Jefe de la Unidad de Revisión', peso: 0 },
      { id_rol: 13, nombre: 'TECNICO_FDI', descripcion: 'Técnico Responsable', peso: 0 },
      { id_rol: 14, nombre: 'LEGAL_FDI', descripcion: 'Técnico Legal', peso: 0 },
      { id_rol: 15, nombre: 'FINANCIERO_FDI', descripcion: 'Técnico Financiero', peso: 0 },
      { id_rol: 16, nombre: 'SUPERVISOR_FDI', descripcion: 'Supervisor', peso: 0 },
      { id_rol: 17, nombre: 'FISCAL_FDI', descripcion: 'Fiscal', peso: 0 },
      { id_rol: 18, nombre: 'RESP_GAM_GAIOC', descripcion: 'Responsable de GAM/GAIOC', peso: 0 }
    ];
    for (let i in roles) {
      roles[i].estado = !roles[i].estado?'ACTIVO':roles[i].estado;
      roles[i]._usuario_creacion = '1';
      roles[i]._fecha_creacion = new Date();
      roles[i]._fecha_modificacion = new Date();
    }
    return queryInterface.bulkInsert('rol', roles, {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
