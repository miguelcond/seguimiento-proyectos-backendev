// 'use strict';

module.exports = {
  up(queryInterface) {
    if (process.env.CREATEUSERS) {
      const usuarios = [
        { usuario: 'admin', contrasena: '672caf27f5363dc833bda5099775e891', fid_persona: 1, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        // { usuario: 'legal', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 2, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        // { usuario: 'legal2', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 3, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        // { usuario: 'legal3', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 4, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        // { usuario: 'financiero', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 5, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        // { usuario: 'financiero2', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 6, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        // { usuario: 'financiero3', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 7, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        // { usuario: 'encar_regional', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 8, fcod_departamento: '{02,04,05}' },
        // { usuario: 'encar_regional2', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 9, fcod_departamento: '{01,03,06}' },
        // { usuario: 'encar_regional3', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 10, fcod_departamento: '{07,08,09}' },
        // { usuario: 'encar_deptal', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 11, fcod_departamento: '{02}' },
        // { usuario: 'encar_deptal2', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 12, fcod_departamento: '{01}' },
        // { usuario: 'encar_deptal3', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 13, fcod_departamento: '{03}' },
        // { usuario: 'encar_deptal4', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 14, fcod_departamento: '{04}' },
        // { usuario: 'encar_deptal5', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 15, fcod_departamento: '{05}' },
        // { usuario: 'encar_deptal6', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 16, fcod_departamento: '{06}' },
        // { usuario: 'encar_deptal7', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 17, fcod_departamento: '{07}' },
        // { usuario: 'encar_deptal8', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 18, fcod_departamento: '{08}' },
        // { usuario: 'encar_deptal9', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 19, fcod_departamento: '{09}' },
        // { usuario: 'tecnico1', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 20, fcod_departamento: '{02}' },
        // { usuario: 'tecnico2', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 21, fcod_departamento: '{02}' },
        // { usuario: 'tecnico3', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 22, fcod_departamento: '{01}' },
        // { usuario: 'tecnico4', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 23, fcod_departamento: '{03}' },
        // { usuario: 'tecnico5', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 24, fcod_departamento: '{04}' },
        // { usuario: 'tecnico6', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 25, fcod_departamento: '{05}' },
        // { usuario: 'tecnico7', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 26, fcod_departamento: '{06}' },
        // { usuario: 'tecnico8', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 27, fcod_departamento: '{07}' },
        // { usuario: 'tecnico9', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 28, fcod_departamento: '{08}' },
        // { usuario: 'tecnico10', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 29, fcod_departamento: '{09}' },
        { usuario: 'recepcionfdi', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 2, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        { usuario: 'revisionfdi', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 3, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' , cargo:'Jefe Area Revision Tecnica de Proyectos'},
        { usuario: 'tecnicofdi', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 4, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' , cargo:'Tecnico Revisor'},
        { usuario: 'tecnicofdi2', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 5, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' , cargo:'Tecnico Revisor'},
        { usuario: 'legalfdi', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 6, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        { usuario: 'financierofdi', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 7, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
        // { usuario: 'supervisorfdi', contrasena: '202cb962ac59075b964b07152d234b70', fid_persona: 8, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
      ];
      for (let i in usuarios) {
        usuarios[i].fid_institucion = 1;
        if(i>=1) {
          usuarios[i].fid_institucion = 2;
        }
        usuarios[i]._usuario_creacion = '1';
        usuarios[i]._fecha_creacion = new Date();
        usuarios[i]._fecha_modificacion = new Date();
      }
      return queryInterface.bulkInsert('usuario', usuarios, {});
    } else {
      const usuarios = [
        { usuario: 'admin', contrasena: '672caf27f5363dc833bda5099775e891', fid_persona: 1, fcod_departamento: '{01,02,03,04,05,06,07,08,09}' },
      ];
      for (let i in usuarios) {
        usuarios[i]._usuario_creacion = '1';
        usuarios[i]._fecha_creacion = new Date();
        usuarios[i]._fecha_modificacion = new Date();
      }
      return queryInterface.bulkInsert('usuario', usuarios, {});
    }
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
