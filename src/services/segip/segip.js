import request from 'request';
import config from '../../config/config';
import logger from '../../lib/logger';

/**
 * Obtiene datos de la persona basado en datos como el número de documento(ci),
 * complemento y su fecha de nacimiento.
 * @param  {Objeto} datos Objeto con los datos necesarios
 * @return {Promesa}      Retorna una promesa
 */
module.exports.obtenerPersona_obsoleto = (datos) => {
  const segip = config().segip;
  return new Promise((resolve, reject) => {
    let url = `${segip.url}${segip.path}personas/`;
    if (datos) {
      if (datos.ci) {
        url = `${url}${datos.ci}`;
        if (datos.complemento) {
          url = `${url}?complemento=${datos.complemento}`;
          if (datos.fecha_nacimiento) {
            url = `${url}&fecha_nacimiento=${datos.fecha_nacimiento}`;
          }
        } else if (datos.fecha_nacimiento) {
          url = `${url}?fecha_nacimiento=${datos.fecha_nacimiento}`;
        }
      }
    }
    logger.info(`[GET] ${url}`);
    request({
      url,
      method: 'GET',
      rejectUnauthorized: false,
      headers: {
        Authorization: segip.tokenKong,
        apikey: segip.credenciales.apikey
      },
      json: true
    }, (err, res) => {
      if (err) {
        switch (err.code) {
          case 'ENOTFOUND':
          case 'EAI_AGAIN':
          case 'ECONNRESET':
          case 'ECONNREFUSED':
            err.message = 'No se pudo establecer conexión con segip.';
            break;
        }
        return reject(err);
      }
      console.log(res.body && res.body.ConsultaDatoPersonaEnJsonResult && JSON.parse(res.body.ConsultaDatoPersonaEnJsonResult.DatosPersonaEnFormatoJson));
      return resolve(res.body);
    });
  });
};

/**
 * Certifica una persona, basado en el número de documento(ci).
 * @param  {Numero} ci Número de documento a certificar
 * @return {Promesa}   Retorna una promesa
 */
module.exports.consultaDatoPersonaCertificacion = (ci) => {
  return new Promise((resolve, reject) => {
    request({
      url: `${config().segip.url}${config().segip.path}personas/certificacion/${ci}`,
      method: 'GET',
      rejectUnauthorized: false,
      headers: {
        Authorization: config().segip.tokenKong,
        apikey: config().segip.credenciales.apikey
      },
      json: true
    }, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res.body);
    });
  });
};

/**
 * Obtiene los datos de una persona, basado en un codigo.
 * @param  {[type]} codigo [description]
 * @return {[type]}        [description]
 */
module.exports.obtenerDocumentoPorCodigoUnico = (codigo) => {
  return new Promise((resolve, reject) => {
    request({
      url: `${config().segip.url}${config().segip.path}personas/codigo/${codigo}`,
      method: 'GET',
      rejectUnauthorized: false,
      headers: {
        Authorization: config().segip.tokenKong,
        apikey: config().segip.credenciales.apikey
      },
      json: true
    }, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res.body);
    });
  });
};

/**
 * [consultaDatoPersonaContrastacion description]
 * @param  {[type]} listaCampo  [description]
 * @param  {[type]} tipoPersona [description]
 * @return {[type]}             [description]
 */
module.exports.consultaDatoPersonaContrastacion = (listaCampo, tipoPersona) => {
  return new Promise((resolve, reject) => {
    const lista = JSON.stringify(listaCampo);
    let url = `${config().segip.url}${config().segip.path}personas/contrastacion?tipo_persona=${tipoPersona}&lista_campo=${lista}`;
    // Codificar la URL
    url = encodeURI(url);
    request({
      url,
      method: 'GET',
      rejectUnauthorized: false,
      headers: {
        Authorization: config().segip.tokenKong,
        apikey: config().segip.credenciales.apikey
      },
      json: true
    }, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res.body);
    });
  });
};

/**
 * [estado description]
 * @return {[type]} [description]
 */
module.exports.estado = () => {
  return new Promise((resolve, reject) => {
    request({
      url: `${config().segip.url}${config().segip.path}status`,
      method: 'GET',
      rejectUnauthorized: false,
      headers: {
        Authorization: config().segip.tokenKong,
        apikey: config().segip.credenciales.apikey
      },
      json: true
    }, (err, res) => {
      if (err) {
        return reject(err);
      }
      return resolve(res);
    });
  });
};
