import request from 'request';
import config from '../../config/config';
import logger from '../../lib/logger';

/**
 * Obtiene el login de una empresa basado en el nit y usuario.
 * @param  {Cadena} nit de la empresa
 * @param  {Cadena} usuario para autenticar
 * @param  {Cadena} clave para autenticar
 * @return {Promesa} Retorna una promesa
 */
module.exports.login = (nit, usuario, clave) => {
  return new Promise((resolve, reject) => {
    let url = `${config().impuestos.url}${config().impuestos.path}login`;
    logger.info(`[POST] ${url}`);
    request({
      url,
      method: 'POST',
      json: {
        nit: nit,
        usuario: usuario,
        clave: clave
      },
      rejectUnauthorized: false,
      headers: {
        Authorization: config().impuestos.tokenKong
      }
    }, (err, res) => {
      if (err) {
        switch (err.code) {
          case 'ENOTFOUND':
          case 'EAI_AGAIN':
          case 'ECONNRESET':
          case 'ECONNREFUSED':
            err.message = 'No se pudo establecer conexión con impuestos.';
            break;
        }
        return reject(err);
      }
      return resolve(res.body);
    });
  });
};
