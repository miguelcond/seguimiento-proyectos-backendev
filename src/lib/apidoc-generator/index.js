'use strict';
/**
* @name Apidoc Generator
* @version 1.0.0
* @author Wilmer Alex Quispe Quispe
* @description Librería que permite crear generar un archivo con la documentación de los servicios.
* EL formato de salida esta basado en los estándares que establece APIDOCJS  http://apidocjs.com/
*
* Caracteristicas:
* - El formato de los campos es el mismo que usa Sequelize.
* - Puede cargar los campos de los modelos a partir de una instancia de la clase Sequelize.
* - Es posible crear campos personalizados (campos que no se encuentran en ningun modelo).
* - Es posible obtener dichos campos para formar la propiedad INPUT que se requiere para la documentación.
* Ejemplos: Para cargar los campos y posteriormente utilizarlos para generar el apidoc.

const { field } = require('apidoc-generator');

field.import(sequelize);

const INPUT = {
  prop1: field.model1('property1'),
  prop2: field.model1('property2'),
  prop3: field.model1('property3'),
  obj1: {
    prop4: field.model2('property1', { allowNull: false }),
    prop5: field.model2('property2', { allowNull: false, example: 'Ejemplo de entrada' }),
    prop6: field.model2('property3', { allowNull: true, comment: 'Comentario' })
  }
}

field.add('custom', {
  estado: { type: Sequelize.STRING(), comment: 'Estado en el que se encuentra la petición.', example: 'success' },
  mensaje: { type: Sequelize.STRING(), comment: 'Breve mensaje que explica el resultado obtenido.' }
})

const OUTPUT = {
  estado: field.custom('estado'),
  mensaje: field.custom('mensaje')
}

* Ejemplo: Para generar el apidoc:
const { apidoc } = require('apidoc-generator')

apidoc.create({ output: path.resolve(__dirname, './apidoc.route.js'), log: true });

apidoc.get('/path/to/resource', {
  name: 'Nombre ÚNICO que identifica a un recurso',
  group: 'Grupo al que pertenece el recurso.',
  description: 'Breve descripción de lo que hace el recurso.',
  input: INPUT,
  output: OUTPUT
})
*/
module.exports = {
  apidoc: require('./apidoc'),
  field: require('./field.js')
};
