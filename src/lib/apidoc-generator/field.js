'use strict';
const _ = require('lodash');
/**
* @name Apidoc Generator
* @version 1.0.0
* @author Wilmer Alex Quispe Quispe
* @description Librería que permite crear generar un archivo con la documentación de los servicios.
*/

/**
* Crea un campo a partir del campo de un modelo sequelize.
* @param {Model} model Modelo sequelize.
* @param {String} field Nombre del campo del modelo.
* @param {Object} properties Propiedades que serán reemplazadas en lugar de las propiedades del modelo original.
*/
function field (model, field, properties = {}) {
  const FIELD = _.cloneDeep(model.rawAttributes[field]);
  for (let prop in properties) {
    FIELD[prop] = properties[prop];
  }
  return FIELD;
}

/**
* Importa los campos de todos los modelos a partir de una instancia sequelize.
* @param {Sequelize} sequelize Instancia de la clase Sequelize.
*/
field.import = (sequelize) => {
  field.sequelize = sequelize;
  for (let i in sequelize.models) {
    const modelName = sequelize.models[i].tableName;
    field[modelName] = (fieldName, properties) => {
      const FIELD = _.clone(sequelize.models[i].attributes[fieldName]);
      for (let prop in properties) {
        FIELD[prop] = properties[prop];
      }
      return FIELD;
    };
  }
};

/**
* Adiciona un modelo con campos personalizados.
* @param {String} modelName Nombre del modelo.
* @param {Object} fields Propiedades del modelo (campos de tipo Sequelize)
*/
field.add = (modelName, fields) => {
  const attributes = field.sequelize.define(modelName, fields).attributes;
  delete field.sequelize.models[modelName];
  field.sequelize.modelManager.models.pop();
  field[modelName] = (fieldName, properties) => {
    const FIELD = _.clone(attributes[fieldName]);
    for (let prop in properties) {
      FIELD[prop] = properties[prop];
    }
    return FIELD;
  };
};

module.exports = field;
