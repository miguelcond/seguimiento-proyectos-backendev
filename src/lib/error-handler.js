import logger from './logger';

/**
* Módulo error-handler.
* Módulo para controlar todos los errores.
* @module
*/
module.exports = (app) => {
  /**
  * Este middleware debe adicionarse al final de todas las rutas. De esta forma,
  * cualquier tipo de error pasará por este módulo.
  *
  * Para asegurarse que esto ocurra, no debe existir nungún otro middleware declarado
  * con estas caracteristicas.

  Ejemplo 1: Para enviar un error desde un controlador.

  let controlador = (req, res, next) => {
    return next(new Error(`Error controlado por la libreria error-handler`));
  }

  Otra forma  que produce el mismo resultado:

  let controlador = (req, res, next) => {
    throw new Error(`Error controlado por la libreria error-handler`);
  }

  Se pueden utilizar instancias de las clases que se encuentran en 'src/lib/errors'

  import errors from './src/lib/errors';
  let controlador = (req, res, next) => {
    throw new errors.ValidationError(`Error controlado por la libreria error-handler`);
  }
  */
  app.use((err, req, res, next) => {
    // Errores lanzados por la libreria 'express-validation'.
    // NOTA: Los mensajes de error producidos por la librería 'express-validator',
    // siempre comienzan con la frase 'validation error'
    if (err.message === 'validation error') {
      let errors = [];
      for (let i in err.errors) {
        let error = err.errors[i];
        errors.push({
          campo: error.field,
          ubicacion: error.location,
          mensajes: error.messages
        });
      }
      return res.status(422).json({
        finalizado: false,
        codigo: 422,
        mensaje: 'Error de validación',
        errores: errors
      });
    }

    // Errores lanzados por la aplicación.
    // Estos errores son instancias de las clases que se encuentran en
    // la dirección ./src/lib/errors
    if ((err.name === 'UnauthorizedError') || (err.name === 'NotFoundError') ||
        (err.name === 'ValidationError') || (err.name === 'NoPermissionError') ||
        (err.name === 'BadRequestError') || (err.name === 'DataImportError') ||
        (err.name === 'EmailError') || (err.name === 'IncorrectUsage') ||
        (err.name === 'Maintenance') || (err.name === 'MethodNotAllowedError') ||
        (err.name === 'RequestEntityTooLargeError') || (err.name === 'UnsupportedMediaTypeError') ||
        (err.name === 'TokenRevocationError') || (err.name === 'TooManyRequestsError')) {
      return res.status(err.statusCode).json({
        finalizado: false,
        codigo: err.statusCode,
        mensaje: err.message,
        errores: err.errors,
        erroresValidacion: err.validationErrors
      });
    }

    // Errores lanzados por sequelize.
    if (err.name === 'SequelizeValidationError') {
      let errors = [];
      for (let i in err.errors) {
        let error = err.errors[i];
        errors.push({
          campo: error.path,
          ubicacion: error.path,
          mensajes: error.message
        });
      }
      return res.status(422).json({
        finalizado: false,
        codigo: 422,
        mensaje: 'Error de validación',
        errores: errors
      });
    }
    if (err.name === 'SequelizeUniqueConstraintError') {
      let mensaje = '';
      if (err.parent.detail.startsWith('Ya existe la llave')) {
        let detail = err.parent.detail;
        let a = detail.indexOf('(');
        let b = detail.indexOf(')', a);
        let c = detail.indexOf('(', b);
        let d = detail.indexOf(')', c);
        if ((a >= 0) && (b >= 0) && (c >= 0) && (d >= 0)) {
          let fields = detail.substr(a + 1, (b - (a + 1)));
          let values = detail.substr(c + 1, (d - (c + 1)));
          mensaje = `El '${fields}' ingresado '${values}' corresponde a otro usuario registrado.`;
        } else {
          mensaje = err.parent.detail;
        }
      }
      return res.status(422).json({
        finalizado: false,
        codigo: 422,
        mensaje: mensaje
      });
    }
    if (err.name === 'SequelizeForeignKeyConstraintError') {
      return res.status(422).json({
        finalizado: false,
        codigo: 422,
        mensaje: err.message
      });
    }
    // Errores de conexión
    // Estos errores pueden ser por la falta de INTERNET, El servidor demora en responder, etc.
    // Hace falta detallar estos errores para que muestre un mensaje adecuado.
    if (err.code) {
      if ((err.code === 'ENOTFOUND') || (err.code === 'EAI_AGAIN') || (err.code === 'ECONNRESET') || (err.code === 'ECONNREFUSED')) {
        logger.error(err);
        return res.status(500).json({
          finalizado: false,
          codigo: 500,
          mensaje: err.message || 'No se pudo establecer conexión con el servidor.'
        });
      }
    }

    // verifica si hay errores en el formato json, en la peticion entrante
    if (err instanceof SyntaxError) {
      logger.error(err);
      return res.status(400).json({
        finalizado: false,
        codigo: 400,
        mensaje: 'Problemas en el formato JSON'
      });
    }

    logger.info('\n\n  //////////////// ERROR INTERNO \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\n');
    logger.error(err);
    return res.status(500).json({
      finalizado: false,
      codigo: 500,
      mensaje: (process.env === 'production') ? 'Hubo un problema al procesar la petición.' : err.message
    });
  });
};
