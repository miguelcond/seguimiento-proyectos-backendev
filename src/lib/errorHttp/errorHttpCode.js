'use strict';

const HttpCodeMsgError = require('./httpCodeMsgError');

const errorHttpCode = {
  newInstanceError (code, mensaje) {
    const error = new HttpCodeMsgError(code, mensaje);
    return error;
  },
  errorBadRequest (mensaje) {
    return this.newInstanceError(400, mensaje);
  },
  errorUnauthorized (mensaje) {
    return this.newInstanceError(401, mensaje);
  },
  errorForbidden (mensaje) {
    return this.newInstanceError(403, mensaje);
  },
  errorNotFound (mensaje) {
    return this.newInstanceError(404, mensaje);
  },
  errorConflict (mensaje) {
    return this.newInstanceError(409, mensaje);
  },
  errorConditionFailed (mensaje) {
    return this.newInstanceError(412, mensaje);
  },
  errorServer (mensaje) {
    return this.newInstanceError(500, mensaje);
  },
  errorFinish (res, error, mensaje) {
    res.errorInf= error.stack;
    if (error instanceof HttpCodeMsgError) error.sendResponse(res);
    else res.status(500).json({tipoMensaje: 'ERROR', mensaje: error.message});
  },
};

module.exports = errorHttpCode;
