/*
Archivo con los recursos necesarios para poder enviar correo.
*/
const nodemailer = require('nodemailer');
const logger = require('./logger');
const configuracion = require('./../config/config')();
const config = configuracion.correo;
const transporte = nodemailer.createTransport(config);

/**
* Envia el un correo con parametros necesarios
* @param {type} pDatos  Contiene los datos necesarios para en el envio del correo.
* @returns {Promise} Retorna una promesa sobre el envio del correo.
*/
function enviar (pDatos) {
  return new Promise((resolve, reject) => {
    const opciones = {
      from: configuracion.sistema.origen,
      to: pDatos.para,
      subject: pDatos.titulo,
      //  text: pDatos.titulo,
      html: pDatos.html
    };
    transporte.sendMail(opciones, (pError, pInfo) => {
      if (pError) {
        logger.error(`El correo a ${pDatos.para} no pudo ser enviado | ${pError}`);
        let msg = pError;
        if (pError.message.indexOf('No recipients defined') > -1) msg.message = 'El correo destino no es valido.';
        if (pError.message.indexOf('getaddrinfo ENOTFOUND') > -1) msg.message = 'En este momento no se puede realizar el envio de correo.';
        if (pError.message.indexOf('connect ECONNREFUSED') > -1) msg.message = 'En este momento no se puede realizar el envio de correo.';
        if (pError.message.indexOf('140770FC') > -1) msg.message = 'En este momento no se puede realizar el envio de correo.';
        if (pError.message.indexOf('Invalid login') > -1) msg.message = 'En este momento no se puede realizar el envio de correo.';
        if (pError.message.indexOf('unable to verify the first certificate') > -1) msg.message = 'En este momento no se puede realizar el envio de correo.';
        return reject(msg);
      }
      logger.info(`El correo a ${pDatos.para} fue enviado | ${pInfo.response}`);
      return resolve();
    });
  });
}

module.exports = {
  enviar
};
