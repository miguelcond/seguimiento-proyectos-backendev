/* eslint prefer-rest-params: 0 */
/* eslint func-names: 0 */
import bodyParser from 'body-parser';
import express from 'express';
import cors from 'cors';
import jwt from 'jwt-simple';
import helmet from 'helmet';
import sequelizeCrud from './sequelize-crud';
import i18n from './i18n';
import logger from './logger';
import errorHttp from './errorHttp/errorHttpCode';
import errors from './errors';
import path from 'path';
import { apidoc } from './apidoc-generator';

module.exports = (app) => {
  // Constante que almacena la congifuracion.
  const configuracion = app.src.config.config;
  // Establece el puerto
  app.set('port', configuracion.puerto);
  app.errorHandler = errorHttp;
  // Establece la llave secreta
  app.set('secretAGETIC', configuracion.jwtSecret);
  app.use(helmet());

  // //Showtests
  app.use((req, res, next) => {
    res.locals.showTests = app.get('env') !== 'production' &&
      req.query.test === '1';
    next();
  });

  // Realiza el uso y configuracion de cors.
  app.use(cors({
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE, OPTIONS',
    preflightContinue: true,
    headers: 'Cache-Control, Pragma, Content-Type, Authorization, Content-Length, X-Requested-With',
    'Access-Control-Allow-Headers': 'Authorization, Content-Type',
    // 'Cache-Control': 'private, no-cache, no-store, must-revalidate',
    // 'Expires': '-1',
    'Pragma': 'no-cache'
  }));

  // Realiza el uso de 'bodyParser' para la recepcion de Json como body.
  app.use(bodyParser.json());

  // Realiza el uso de la autenticacion de passport.
  app.use(app.src.auth.initialize());

  // //eliminar ids en caso de que lo envien por si quieren hacer sqlinjection
  // app.use((req, res, next) => {
  //     delete req.body.id;
  //     next();
  // });
  // para generar un espacio publico, archivos estaticos
  app.use(express.static('public'));

  if (process.env.APIDOC === 'true') {
    // Para generar el APIDOC
    apidoc.create({ output: path.resolve(__dirname, './../../apidoc/apidoc.route.js') });
  }

  // Autenticación -- JWTOKEN
  app.use(configuracion.api.main, (req, res, next) => {
    const RolRuta = app.src.db.models.rol_ruta;
    const Ruta = app.src.db.models.ruta;
    const Usuario = app.src.db.models.usuario;

    if (req.method === 'OPTIONS') return next();

    if (req.headers.authorization) {
    // check header or url parameters or post parameters for token
      const token = req.headers.authorization.split(' ')[1];
      // decode token
      if (token) {
        let tokenDecoded;
        try {
          tokenDecoded = jwt.decode(token, app.get('secretAGETIC'));
          if (!tokenDecoded.vencimiento) {
            throw new Error(`El token es inválido`);
          }
        } catch (error) {
          return next(new errors.UnauthorizedError(`El token es inválido.`));
        }
        // verifies secret and checks exp
        try {
          if ((new Date(tokenDecoded.vencimiento).getTime()) <= Date.now()) {
            throw new errors.UnauthorizedError(`Su sesión ha expirado, debe autenticarse nuevamente.`);
          }
          RolRuta.findAll({
            attributes: ['method_get', 'method_post', 'method_put', 'method_delete'],
            where: {
              fid_rol: {
                $in: tokenDecoded.roles
              },
              estado: 'ACTIVO'
            },
            include: [{
              model: Ruta,
              as: 'ruta',
              attributes: ['ruta'],
              where: {
                estado: 'ACTIVO'
              }
            }]
          }).then((rolesRutasRes) => {
            let rutaPermitida = false;
            for (let i = 0; i < rolesRutasRes.length; i += 1) {
              const ruta = rolesRutasRes[i];
              if (req.originalUrl === ruta.ruta.ruta ||
                  req.originalUrl.substring(0, req.originalUrl.length - 1) === ruta.ruta.ruta ||
                  req.originalUrl.indexOf(ruta.ruta.ruta) >= 0) {
                if ((req.method === 'GET' && ruta.method_get) ||
                    (req.method === 'POST' && ruta.method_post) ||
                    (req.method === 'PUT' && ruta.method_put) ||
                    (req.method === 'DELETE' && ruta.method_delete)) {
                  rutaPermitida = true;
                  break;
                }
              }
            }
            if (rutaPermitida) {
              // Insertando los datos para auditoria en el req.body
              req.body.audit_usuario = {
                id_usuario: tokenDecoded.id_usuario,
                id_persona: tokenDecoded.id_persona,
                // Si se envia el rol se toma esta como prioritario
                id_rol: (req.query && req.query.rol && parseInt(req.query.rol)>0 && tokenDecoded.roles.indexOf(parseInt(req.query.rol))>=0) ? parseInt(req.query.rol):
                        (req.body && parseInt(req.body.rol)>0 && tokenDecoded.roles.indexOf(parseInt(req.body.rol))>=0)? parseInt(req.body.rol): tokenDecoded.id_rol,
                roles: tokenDecoded.roles,
                usuario: tokenDecoded.usuario
              };
              Usuario.findOne({
                attributes: ['fid_institucion'],
                where: {
                  id_usuario: tokenDecoded.id_usuario,
                }
              }).then((user)=>{
                req.body.audit_usuario.institucion = {
                  id: user.fid_institucion,
                }
                console.log('');
                console.log('----------------');
                logger.info(`[${req.method}] ${req.originalUrl}`, req.body.audit_usuario);
                console.log('----------------');
                return next();
              }).catch((error)=>{
                return next(error);
              });
            } else {
              throw new errors.UnauthorizedError('Usted no tiene acceso a dichos recursos.');
            }
          })
          .catch((error) => {
            return next(error);
          });
        } catch (err) {
          return next(err);
        }
      } else {
        return next(new errors.UnauthorizedError(`Verifique el formato del campo Authorization (authorization = 'Bearer TOKEN_ACCESO')`));
      }
    } else {
      return next(new errors.UnauthorizedError(`Su sesión ha expirado, debe autenticarse nuevamente.`));
    }
  });
  // Autenticación -- JWTOKEN - FIN
  // Iniciando API automático de CRUD
  const router = express.Router();
  sequelizeCrud.init(router, app.src.db.models, configuracion.api.crud);

  // Definiendo Rutas principales
  app.use(configuracion.api.main, router);
  app.api = router;

  // Definiendo Rutas públicas
  const router2 = express.Router();
  app.use(configuracion.api.public, router2);
  app.apiPublic = router2;

  // Autenticación -- JWTOKEN
  app.use(configuracion.servicio.khipus, (req, res, next) => {
    if (req.method === 'OPTIONS') return next();

    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      if (token) {
        let tokenDecoded;
        try {
          tokenDecoded = jwt.decode(token, app.get('secretAGETIC'));
          if (!tokenDecoded.exp) {
            throw new Error(`El token es inválido ó fue revocado.`);
          }
        } catch (error) {
          return next(new errors.UnauthorizedError(`El token es inválido ó fue revocado.`));
        }
        // verifies secret and checks exp
        try {
          if ((new Date(tokenDecoded.exp).getTime()) <= Date.now()) {
            throw new errors.UnauthorizedError(`Su llave ha expirado, necesita una actualización.`);
          }
          // console.log('Decodificado', tokenDecoded);
          req.body.audit_usuario = {
            id_usuario: tokenDecoded.data.id_usuario
          };
          console.log('');
          console.log('-----Khipus-----');
          logger.info(`[${req.method}] ${req.originalUrl}`, req.body.audit_usuario);
          console.log('----------------');

          return next();
        } catch (error) {
          return next(error);
        }
      } else {
        return next(new errors.UnauthorizedError(`El formato de la cabecera de autorización es incorrecta.`));
      }
    } else {
      return next(new errors.UnauthorizedError(`Se necesita una llave de autorización.`));
    }
  });
  // Definiendo Rutas servicio khipus
  const routerKhipus = express.Router();
  app.use(configuracion.servicio.khipus, routerKhipus);
  app.serviceKhipus = routerKhipus;

  // Definiendo controller
  app.controller = {};
  // Definiendo dao
  app.dao = {};

 // Iniciando la librería de internacionalización
  let pathLang = __dirname.replace('lib', 'lang');
 // Definiendo el idioma español
  i18n.init(pathLang, 'es');
};
