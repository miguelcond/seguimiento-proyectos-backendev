module.exports = (app) => {
  const _app = app;
  _app.dao.common = {};

  const sequelize = app.src.db.sequelize;

  /**
   * crearTransaccion - pasar la transaccion a todos los querys dentro de la function f
   * @param {function} f funcion donde se realizan todas las transacciones
   */
  const crearTransaccion = (f) => {
    const data = sequelize.transaction(f);
    return data;
  };
  _app.dao.common.crearTransaccion = crearTransaccion;
};
