const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  getByPoint: {
    params: {
      long: Joi.number().required(),
      lat: Joi.number().required()
    }
  }
};
