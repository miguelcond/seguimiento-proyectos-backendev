const validate = require('express-validation');
const paramValidation = require('./parametrica.validation');

module.exports = app => {
  /**
   * @apiVersion 1.0.0
   * @apiGroup Parametrica
   * @apiName Get parametricas
   * @api {get} /api/v1/parametricas/:tipo Obtiene datos de parametricas a partir de un tipo
   *
   * @apiDescription Get parametricas, obtiene datos de las parametricas
   * 
   * @apiParam (Params) {String} tipo Tipo de parámetrica
   * 
   * @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
   * @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
   * @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre las parametricas

   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Parametros obtenidos correctamente.",
      "datos": {
        "count": 15,
        "rows": [
          {
            "id_parametrica": 1,
            "codigo": "TUTOR_LEGAL",
            "descripcion": "TUTOR LEGAL"
          },
          {
            "id_parametrica": 12,
            "codigo": "TIO",
            "descripcion": "TIO/A"
          },
          {
            "id_parametrica": 13,
            "codigo": "SOBRINO",
            "descripcion": "SOBRINO/A"
          },
          {
            "id_parametrica": 15,
            "codigo": "PRIMO",
            "descripcion": "PRIMO/A"
          },
          ...
        ]
      }
    }
   */
  app.api.get('/parametricas/:tipo', validate(paramValidation.get), app.controller.parametrica.get);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Parametrica
   * @apiName Post parametricas
   * @api {post} /api/v1/parametricas/:tipo Crear registro de parametrica
   *
   * @apiDescription Post parametrica, agrega un nuevo registro de parametricas
   *
   * @apiParamExample {json} Ejemplo de petición:
    {
      "codigo": "UNIDAD_EJEMPLO",
      "descripcion": "Unidad ejemplo",
      "nombre": "unidad ejemplo"
    }
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Parametrica adicionada correctamente",
      "datos": {
        "codigo": "UNIDAD_EJEMPLO",
        "descripcion": "Unidad ejemplo",
        "nombre": "unidad ejemplo"
      }
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "No tiene los privilegios requeridos para realizar esta acción.",
      "datos": null
    }
   */
  app.api.post('/parametricas/:tipo', validate(paramValidation.post), app.controller.parametrica.post);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Parametrica
   * @apiName Put parametricas
   * @api {put} /api/v1/parametricas/:tipo Actualizar registro de parametrica
   * 
   * @apiDescription Put parametrica, actualiza los datos de un registro de parametrica
   *
   * @apiParamExample {json} Ejemplo de petición:
   *{
      "codigo": "UN_TONELADA",
      "codigo_ant": "UN_TONELADA",
      "descripcion": "Toneladas",
      "descripcion_ant": "Tonelada"
      "nombre": "t",
      "nombre_ant": "t"
    }
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Parametrica modificada correctamente",
      "datos": {
        "codigo": "UN_TONELADA"
      }
    }
   */
  app.api.put('/parametricas/:tipo', validate(paramValidation.put), app.controller.parametrica.put);
};
