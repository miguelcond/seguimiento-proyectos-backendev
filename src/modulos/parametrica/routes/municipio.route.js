import sequelizeFormly from 'sequelize-formly';
const validate = require('express-validation');
const paramValidation = require('./municipio.validation');
module.exports = (app) => {
  /**
    @apiVersion 1.0.0
    @apiGroup Municipio
    @apiName Get municipios
    @api {get} /municipios/:cod_provincia Obtiene datos de municipios

    @apiDescription Get parametricas, obtiene datos de municipios a partir de un cod_provincia

    @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
    @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
    @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre las municipios

    @apiSuccessExample {json} Respuesta:
    HTTP/1.1 200 OK
    [
        {
            "codigo": "010101",
            "nombre": "Sucre",
            "point": {
                "type": "Point",
                "coordinates": [
                    -65.2601679399999,
                    -19.0465195899999
                ]
            }
        },
        {
            "codigo": "010102",
            "nombre": "Yotala",
            "point": {
                "type": "Point",
                "coordinates": [
                    -65.2643797139999,
                    -19.1620850099999
                ]
            }
        },
        {
            "codigo": "010103",
            "nombre": "Poroma",
            "point": {
                "type": "Point",
                "coordinates": [
                    -65.4250414219999,
                    -18.5379123929999
                ]
            }
        }
    ]
  */
  app.api.get('/municipios/:cod_provincia', app.controller.municipio.get);
  /**
    @apiVersion 1.0.0
    @apiGroup Municipio
    @apiName Get Municipio
    @api {get} /municipios/:long/:lat Obtiene datos de un municipio

    @apiDescription Get parametricas, obtiene datos de un municipio a partir de un punto geográfico

    @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
    @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
    @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre municipio

    @apiSuccessExample {json} Respuesta:
    HTTP/1.1 200 OK
    {
        "finalizado": true,
        "mensaje": "Datos obtenidos correctamente.",
        "data": {
            "codigo": "010102",
            "nombre": "Yotala",
            "provincia": {
                "codigo": "0101",
                "nombre": "Oropeza",
                "municipio": [
                    {
                        "codigo": "010101",
                        "nombre": "Sucre",
                        "point": {
                            "type": "Point",
                            "coordinates": [
                                -65.2504832028469,
                                -18.9377067380833
                            ]
                        }
                    },
                    ...
                ],
                "departamento": {
                    "codigo": "01",
                    "nombre": "Chuquisaca",
                    "provincia": [
                        {
                            "codigo": "0110",
                            "nombre": "Luis Calvo"
                        },
                        {
                            "codigo": "0109",
                            "nombre": "Sud Cinti"
                        },
                        {
                            "codigo": "0108",
                            "nombre": "Belisario Boeto"
                        },
                        ...
                    ]
                }
            }
        }
    }
  */
  app.api.get('/municipios/:long/:lat', validate(paramValidation.getByPoint), app.controller.municipio.getByPoint);
  app.api.options('/municipios', sequelizeFormly.formly(app.src.db.models.municipio, app.src.db.models));
};
