import logger from '../../../lib/logger';

module.exports = (app) => {
  const _app = app;
  _app.controller.municipio = {};
  const municipioController = _app.controller.municipio;
  const municipioModel = app.src.db.models.municipio;
  const sequelize = app.src.db.sequelize;
  const util = app.src.lib.util;

  // Obtiene lista de municipios
  municipioController.get = (req, res) => {
    const consulta = util.formarConsulta(req.query, municipioModel, app.src.db.models);
    if (req.query.order) {
      if (req.query.order.charAt(0) === '-') {
        consulta.order = `${req.query.order.substring(1, req.query.order.length)} DESC`;
      } else {
        consulta.order = `${req.query.order}`;
      }
    }
    if (!consulta.condicionMunicipio) {
      consulta.condicionMunicipio = {};
    }
    consulta.condicionMunicipio.codigo_provincia = { $like: `${req.params.cod_provincia}%` };
    if (typeof(consulta.order) === 'string') {
      consulta.order = sequelize.literal(consulta.order);
    }
    municipioModel.findAll({
      attributes: ['codigo', 'nombre', 'point'],
      where: consulta.condicionMunicipio || {},
      offset: consulta.offset || 0,
      limit: consulta.limit || 90,
      order: consulta.order || []
    })
    .then((result) => {
      res.json(result);
    })
    .catch((error) => {
      logger.error(error);
      let msg = 'Error inesperado.';
      if (error.name === 'SequelizeConnectionError' ||
              error.name === 'SequelizeConnectionRefusedError' ||
              error.name === 'SequelizeHostNotReachableError') {
        msg = 'No se pudo conectar a la base de datos.';
      }
      res.json({
        finalizado: false,
        mensaje: msg,
        datos: null
      });
    });
  };

  // Obtiene datos de un municipio desde un punto geográfico
  municipioController.getByPoint = (req, res) => {
    const lat = req.params.lat;
    const long = req.params.long;
    municipioModel.findByPoint(long, lat)
    .then((result) => {
      if (result) {
        res.json({
          finalizado: true,
          mensaje: 'Datos obtenidos correctamente.',
          data: result
        });
      } else {
        res.status(412).send({
          finalizado: false,
          mensaje: 'No se encontraron datos.',
          datos: null
        });
      }
    })
    .catch((error) => {
      logger.error(error);
      let msg = 'Error inesperado.';
      if (error.name === 'SequelizeConnectionError' ||
              error.name === 'SequelizeConnectionRefusedError' ||
              error.name === 'SequelizeHostNotReachableError') {
        msg = 'No se pudo conectar a la base de datos.';
      }
      res.status(412).send({
        finalizado: false,
        mensaje: msg,
        datos: null
      });
    });
  };
};
