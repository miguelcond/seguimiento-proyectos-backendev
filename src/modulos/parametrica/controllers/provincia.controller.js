import logger from '../../../lib/logger';

module.exports = (app) => {
  const _app = app;
  _app.controller.provincia = {};
  const provinciaController = _app.controller.provincia;
  const provinciaModel = app.src.db.models.provincia;
  const sequelize = app.src.db.sequelize;
  const util = app.src.lib.util;

  provinciaController.get = (req, res) => {
    const consulta = util.formarConsulta(req.query, provinciaModel, app.src.db.models);
    if (!consulta.condicionProvincia) {
      consulta.condicionProvincia = {};
    }
    consulta.condicionProvincia.codigo_departamento = req.params.cod_departamento;
    if (req.query.order) {
      if (req.query.order.charAt(0) === '-') {
        consulta.order = `${req.query.order.substring(1, req.query.order.length)} DESC`;
      } else {
        consulta.order = `${req.query.order}`;
      }
    } else {
      consulta.order = 'nombre DESC';
    }
    if (typeof(consulta.order) === 'string') {
      consulta.order = sequelize.literal(consulta.order);
    }
    provinciaModel.findAll({
      attributes: ['codigo', 'nombre'],
      where: consulta.condicionProvincia || {},
      offset: consulta.offset || 0,
      limit: consulta.limit || 100,
      order: consulta.order || []
    }).then((result) => {
      res.json(result);
    }).catch((error) => {
      logger.error(error);
      let msg = 'Error inesperado.';
      if (error.name === 'SequelizeConnectionError' ||
              error.name === 'SequelizeConnectionRefusedError' ||
              error.name === 'SequelizeHostNotReachableError') {
        msg = 'No se pudo conectar a la base de datos.';
      }
      res.json({
        finalizado: false,
        mensaje: msg,
        datos: null
      });
    });
  };
};
