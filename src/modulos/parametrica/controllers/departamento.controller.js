import logger from '../../../lib/logger';

module.exports = (app) => {
  const _app = app;
  _app.controller.departamento = {};
  const departamentoController = _app.controller.departamento;
  const departamentoModel = app.src.db.models.departamento;
  const sequelize = app.src.db.sequelize;
  const util = app.src.lib.util;

  departamentoController.get = (req, res) => {
    const consulta = util.formarConsulta(req.query, departamentoModel, app.src.db.models);
    if (req.query.order) {
      if (req.query.order.charAt(0) === '-') {
        consulta.order = `${req.query.order.substring(1, req.query.order.length)} DESC`;
      } else {
        consulta.order = `${req.query.order}`;
      }
    } else {
      consulta.order = 'nombre ';
    }
    if (!consulta.condicionDepartamento) {
      consulta.condicionDepartamento = {};
    }
    consulta.condicionDepartamento.estado = 'ACTIVO';
    if (typeof(consulta.order) === 'string') {
      consulta.order = sequelize.literal(consulta.order);
    }
    departamentoModel.findAll({
      attributes: ['codigo', 'nombre'],
      where: consulta.condicionDepartamento,
      offset: parseInt(consulta.offset) || 0,
      limit: parseInt(consulta.limit) || 10,
      order: consulta.order || []
    }).then((result) => {
      res.json(result);
    }).catch((error) => {
      logger.error(error);
      let msg = 'Error inesperado.';
      if (error.name === 'SequelizeConnectionError' ||
              error.name === 'SequelizeConnectionRefusedError' ||
              error.name === 'SequelizeHostNotReachableError') {
        msg = 'No se pudo conectar a la base de datos.';
      }
      res.json({
        finalizado: false,
        mensaje: msg,
        datos: null
      });
    });
  };

  departamentoController.post = (req, res) => {
    const departamentoCrear = req.body;
    departamentoCrear._usuario_creacion = req.body.audit_usuario.id_usuario;
    departamentoModel.create(departamentoCrear)
    .then((departamento) => {
      res.json({
        finalizado: true,
        mensaje: 'Creación del departamento exitoso.',
        datos: departamento
      });
    }).catch((error) => {
      let msg = 'Error inesperado.';
      if (error.name === 'SequelizeConnectionError' ||
              error.name === 'SequelizeConnectionRefusedError' ||
              error.name === 'SequelizeHostNotReachableError') {
        msg = 'No se pudo conectar a la base de datos.';
      }
      res.json({
        finalizado: false,
        mensaje: msg,
        datos: null
      });
    });
  };

  departamentoController.put = (req, res) => {
    const idDepartamento = req.params.id.toString();
    const datos = req.body;
    datos._usuario_modificacion = req.body.audit_usuario.id_usuario;

    departamentoModel.findOne({
      where: { codigo: { $like: idDepartamento } }
    }).then((departamentoModificar) => {
      if (!departamentoModificar) {
        throw new Error('El departamento a modificar no se encuentra disponible.');
      }
      return departamentoModificar.updateAttributes(datos);
    }).then((departamento) => {
      res.json({
        finalizado: true,
        mensaje: 'Modificación del departamento exitoso.',
        datos: departamento
      });
    }).catch((error) => {
      let msg = 'Error inesperado.';
      if (error.name === 'SequelizeConnectionError' ||
              error.name === 'SequelizeConnectionRefusedError' ||
              error.name === 'SequelizeHostNotReachableError') {
        msg = 'No se pudo conectar a la base de datos.';
      }
      res.json({
        finalizado: false,
        mensaje: msg,
        datos: null
      });
    });
  };

  departamentoController.delete = (req, res) => {
    const idDepartamento = req.params.id;
    sequelize.transaction().then((t) => {
      const tr = { transaction: t };
      departamentoModel.findById(idDepartamento, tr)
      .then((departamentoEliminar) => {
        if (!departamentoEliminar) {
          throw new Error('El departamento a eliminar no se encuentra disponible.');
        }
        return departamentoEliminar.destroy(tr);
      }).then(() => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Eliminación del departamento exitoso.',
          datos: null
        });
      }).catch((error) => {
        t.rollback();
        let msg = 'Error inesperado';
        if (error.name === 'SequelizeConnectionError' ||
        error.name === 'SequelizeConnectionRefusedError' ||
        error.name === 'SequelizeHostNotReachableError') {
          msg = 'No se pudo conectar a la base de datos.';
        }
        res.json({
          finalizado: false,
          mensaje: msg,
          datos: null
        });
      });
    });
  };

  departamentoController.getBolivia = (req, res) => {
    departamentoModel.boundsBolivia()
    .then((result) => {
      let arr = result.dataValues.bounds.split('(')[1].split(')')[0].split(',');
      let bounds = {
        southWest: {
          lat: arr[0].split(' ')[1],
          long: arr[0].split(' ')[0]
        },
        northEast: {
          lat: arr[1].split(' ')[1],
          long: arr[1].split(' ')[0]
        }
      };
      res.json({
        finalizado: true,
        mensaje: 'Bounds recuperados correctamente.',
        datos: bounds
      });
    }).catch((error) => {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    });
  };
};
