module.exports = (app) => {
  app.dao.municipio = {};

  function getDpa (municipio) {
    let dpa = {};
    dpa.departamento = municipio.provincia.departamento;
    delete municipio.provincia.departamento;
    dpa.provincia = municipio.provincia;
    delete municipio.provincia;
    dpa.municipio = municipio;
    return dpa;
  }

  app.dao.municipio = {
    getDpa
  };
};
