import crypto from 'crypto';
import errors from '../../../lib/errors';
import util from '../../../lib/util';
import Reporte from '../../../lib/reportes';
import shortid from 'shortid';

module.exports = (app) => {
  const _app = app;
  _app.dao.autenticacion = {};

  const UsuarioModel = app.src.db.models.usuario;
  const PersonaModel = app.src.db.models.persona;
  const UsuarioRolModel = app.src.db.models.usuario_rol;
  const RolModel = app.src.db.models.rol;
  const RolMenuModel = app.src.db.models.rol_menu;
  const MenuModel = app.src.db.models.menu;

  async function buscarUsuario (condiciones) {
    const data = await UsuarioModel.findOne({
      where: condiciones,
      include: [{
        model: PersonaModel,
        as: 'persona',
        attributes: ['id_persona', 'nombres', 'primer_apellido', 'segundo_apellido', 'correo']
      }]
    });
    return data;
  }

  async function buscarUsuarioRol (idUsuario) {
    const data = await UsuarioRolModel.findAll({
      attributes: ['fid_rol'],
      where: {
        fid_usuario: idUsuario,
        estado: 'ACTIVO'
      },
      include: [{
        model: RolModel,
        as: 'rol',
        attributes: ['id_rol', 'nombre', 'peso', 'descripcion'],
        order: [
          ['peso', 'ASC']
        ]
      }]
    });
    return data;
  }

  async function buscarRolMenu (fIdRol) {
    const data = await RolMenuModel.findAll({
      attributes: ['method_get', 'method_post', 'method_put', 'method_delete'],
      where: {
        fid_rol: fIdRol,
        estado: 'ACTIVO'
      },
      include: [{
        model: MenuModel,
        as: 'menu',
        attributes: [
          ['nombre', 'label'],
          ['ruta', 'url'],
          ['icono', 'icon'], 'fid_menu_padre'
        ],
        include: [{
          model: MenuModel,
          as: 'menu_padre',
          attributes: ['id_menu', ['nombre', 'label'],
            ['ruta', 'url'],
            ['icono', 'icon']
          ]
        }]
      }]
    });
    return data;
  }

  async function restablecerContrasena(email) {
    let fechaActual = new Date();
    let data = await UsuarioModel.findOne({
      attributes: ['id_usuario','usuario'],
      where: {
        $or: [ {fecha_expiracion:{$is:null}}, {fecha_expiracion:{$lt:fechaActual}} ]
      },
      include: [{
        model: PersonaModel,
        as: 'persona',
        attributes: ['id_persona', 'nombres', 'primer_apellido', 'segundo_apellido', 'correo'],
        where: {
          correo: email
        },
        required: true
      }]
    });
    if (!data) {
      throw new errors.ValidationError('El correo es invalido!<br>O ya tiene una solicitud de recuperación en curso.');
    }
    const pass = shortid.generate().substr(0,8);
    const persona = {
        nombres: data.nombres,
        primer_apellido: data.primer_apellido,
        segundo_apellido: data.segundo_apellido
    }
    const PARA = email;
    const TITULO = 'Solicitud de Recuperacion de Contraseña';
    const reporte = new Reporte('correo_recuperar_contrasenia');
    const datos = {
      persona: persona,
      usuario: data.usuario,
      codigo: pass,
      urlLogo: app.src.config.config.sistema.urlLogo,
      urlBase: app.src.config.config.sistema.urlBase
    };
    const HTML = reporte.template(datos);
    await util.enviarEmail(PARA, TITULO, HTML);
    data = await UsuarioModel.update({
      codigo_contrasena: pass,
      fecha_expiracion: fechaActual.setDate(fechaActual.getDate()+1)
    }, {
      where: {
        id_usuario: data.id_usuario,
        $or: [ {fecha_expiracion:{$is:null}}, {fecha_expiracion:{$lt:fechaActual}} ]
      }
    });
    return data;
  }

  async function verificarCodigo(codigo) {
    let fechaActual = new Date();
    let data = await UsuarioModel.findOne({
      attributes: ['id_usuario','usuario','codigo_contrasena','fecha_expiracion'],
      where: {
        codigo_contrasena: codigo,
        $or: [ {fecha_expiracion:{$eq:fechaActual}}, {fecha_expiracion:{$gt:fechaActual}} ]
      }
    });
    if (!data) {
      throw new errors.ValidationError('El código indicado no existe');
    }
    return data;
  }

  async function cambiarContrasena(datos) {
    let fechaActual = new Date();
    let data = await UsuarioModel.findOne({
      attributes: ['id_usuario','usuario','codigo_contrasena','fecha_expiracion','contrasena'],
      where: {
        codigo_contrasena: datos.codigo,
        $or: [ {fecha_expiracion:{$eq:fechaActual}}, {fecha_expiracion:{$gt:fechaActual}} ]
      }
    });
    if (!data) {
      throw new errors.ValidationError('El código indicado no es un código válido');
    }

    const contrasenaNueva = crypto.createHash('md5').update(datos.nueva).digest('hex');

    if (datos.nueva!==datos.confirmada) {
      throw new errors.ValidationError('Las contraseñas introducidas no son iguales.');
    }
    if (contrasenaNueva===data.contrasena) {
      throw new errors.ValidationError('La nueva contraseña no puede ser igual a la contraseña anterior.');
    }
    
    let updateData = await UsuarioModel.update({
      codigo_contrasena: null,
      fecha_expiracion: null,
      contrasena: contrasenaNueva
    }, {
      where: {
        id_usuario: data.id_usuario,
        codigo_contrasena: datos.codigo,
        $or: [ {fecha_expiracion:{$eq:fechaActual}}, {fecha_expiracion:{$gt:fechaActual}} ]
      }
    });
    return updateData;
  }

  _app.dao.autenticacion.buscarUsuario = buscarUsuario;
  _app.dao.autenticacion.buscarUsuarioRol = buscarUsuarioRol;
  _app.dao.autenticacion.buscarRolMenu = buscarRolMenu;
  _app.dao.autenticacion.restablecerContrasena = restablecerContrasena;
  _app.dao.autenticacion.verificarCodigo = verificarCodigo;
  _app.dao.autenticacion.cambiarContrasena = cambiarContrasena;
};
