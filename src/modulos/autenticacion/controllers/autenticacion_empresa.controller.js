import jwt from 'jwt-simple';
import impuestos from '../../../services/impuestos/impuestos';
import fundempresa from '../../../services/fundempresa/fundempresa';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.controller.autenticacion_empresa = {};

  async function autenticar (req, res, next) {
    const NIT = req.body.nit;
    const USUARIO = req.body.usuario;
    const CLAVE = req.body.clave;
    if (!NIT || !USUARIO || !CLAVE) {
      return next(new errors.ValidationError('Los datos Nit, Usuario y Clave son obligatorios'));
    }
    return app.dao.common.crearTransaccion(async (t) => {
      let login = await impuestos.login(NIT, USUARIO, CLAVE);
      if (login.Estado === 'ACTIVO HABILITADO' && login.Autenticado === true) {
        const USUARIO_EMPRESA = await app.dao.usuario.crearRecuperarUsuarioEmpresa(NIT);
        let matricula = await fundempresa.obtenerMatriculas(NIT);
        if (!matricula ||
            !matricula.SrvMatriculaConsultaNitResult ||
            !matricula.SrvMatriculaConsultaNitResult.MatriculasResult) {
          throw new Error(`No se pudo obtener los datos desde FUNDEMPRESA. Inténtalo mas tarde.`);
        }
        matricula = matricula.SrvMatriculaConsultaNitResult.MatriculasResult;
        if (matricula.length === 1 && matricula[0].RazonSocial) {
          return obtenerDatos(NIT, USUARIO_EMPRESA.id_usuario, matricula[0].RazonSocial);
        } else {
          return obtenerDatos(NIT, USUARIO_EMPRESA.id_usuario);
        }
      } else {
        throw new errors.UnauthorizedError('Revise los datos introducidos, no coinciden.');
      }
    }).then(result => {
      return res.status(200).json(result);
    }).catch(error => {
      next(error);
    });
  }

  async function obtenerDatos (nit, idUsuario, razonSocial) {
    const ven = new Date();
    ven.setDate(ven.getDate() + 1);
    const datosUsuarioRol = await app.dao.autenticacion.buscarUsuarioRol(idUsuario);
    const ROL_EMPRESA = await app.dao.rol.getPorCodigo('EMPRESA');
    const payload = {
      id_usuario: idUsuario,
      id_rol: ROL_EMPRESA.id_rol,
      roles: [ROL_EMPRESA.id_rol],
      vencimiento: ven
    };
    const usuarioEnviar = {
      id_usuario: idUsuario,
      nit,
      nombres: razonSocial || nit,
      rol: datosUsuarioRol[0].rol,
      roles: [{ id_rol: ROL_EMPRESA.id_rol, nombre: ROL_EMPRESA.nombre, peso: ROL_EMPRESA.peso }]
    };
    const rolMenu = await app.dao.autenticacion.buscarRolMenu(payload.id_rol);
    let menuEntrar = null;
    const menusDevolverAux = [];
    for (let rm = 0; rm < rolMenu.length; rm += 1) {
      // Obteniendo al padre
      const padre = rolMenu[rm].menu.menu_padre;
      const objPadre = JSON.stringify(padre);
      let existe = false;
      for (let i = 0; i < menusDevolverAux.length; i += 1) {
        if (JSON.stringify(menusDevolverAux[i]) === objPadre) {
          existe = true;
          break;
        }
      }
      if (!existe) {
        menusDevolverAux.push(padre);
      }
    }
    const menusDevolver = [];
    for (let padreI = 0; padreI < menusDevolverAux.length; padreI += 1) {
      const padre = JSON.parse(JSON.stringify(menusDevolverAux[padreI]));
      padre.submenu = [];
      if (padre.url && !menuEntrar) {
        menuEntrar = `/${padre.url}`;
      }
      for (let rmI = 0; rmI < rolMenu.length; rmI += 1) {
        if (padre.id_menu === rolMenu[rmI].menu.fid_menu_padre) {
          const hijo = JSON.parse(JSON.stringify(rolMenu[rmI].menu));
          delete hijo.menu_padre;
          hijo.permissions = {};
          hijo.permissions.read = rolMenu[rmI].method_get;
          hijo.permissions.create = rolMenu[rmI].method_post;
          hijo.permissions.update = rolMenu[rmI].method_put;
          hijo.permissions.delete = rolMenu[rmI].method_delete;
          padre.submenu.push(hijo);
          if (!menuEntrar) {
            menuEntrar = `/${hijo.url}`;
          }
        }
      }
      menusDevolver.push(padre);
    }
    const resultado = {
      finalizado: true,
      mensaje: 'Obtención de datos exitosa.',
      token: jwt.encode(payload, app.settings.secretAGETIC),
      datos: {
        usuario: usuarioEnviar,
        menu: menusDevolver,
        menuEntrar: menuEntrar
      }
    };
    return resultado;
  }

  _app.controller.autenticacion_empresa.post = autenticar;
};
