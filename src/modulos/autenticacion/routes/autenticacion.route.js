import validate from 'express-validation';
import paramValidation from './autenticacion.validation';
import { apidoc as APIDOC, field as FIELD } from '../../../lib/apidoc-generator';

module.exports = (app) => {
  app.post('/autenticar', validate(paramValidation.login), app.controller.autenticacion.post);
  APIDOC.post('/autenticar', {
    name: 'Autenticar Usuario',
    group: 'Autenticacion',
    description: 'Servicio se autenticación para los usuarios. Devuelve un token de acceso e información acerca del usuario.',
    input: {
      body: {
        username: FIELD.usuario('usuario'),
        password: FIELD.usuario('contrasena'),
        id_rol: FIELD.rol('id_rol')
      }
    },
    output: {
      body: {
        finalizado: FIELD.custom('finalizado'),
        mensaje: FIELD.custom('mensaje'),
        token: FIELD.custom('token'),
        datos: {
          usuario: {
            id_usuario: FIELD.usuario('id_usuario'),
            nombres: FIELD.custom('nombres'),
            apellidos: FIELD.custom('apellidos'),
            usuario: FIELD.usuario('username'),
            rol: {
              id_rol: FIELD.rol('id_rol'),
              nombre: FIELD.rol('nombre'),
              peso: FIELD.rol('peso'),
              descripcion: FIELD.rol('descripcion')
            },
            roles: [{
              id_rol: FIELD.rol('id_rol'),
              nombre: FIELD.rol('nombre'),
              peso: FIELD.rol('peso'),
              descripcion: FIELD.rol('descripcion')
            }]
          },
          menu: [{
            id_menu: FIELD.menu('id_menu'),
            label: FIELD.menu('nombre'),
            url: FIELD.menu('ruta'),
            icon: FIELD.menu('icono'),
            submenu: [{
              label: FIELD.menu('nombre'),
              url: FIELD.menu('ruta'),
              icon: FIELD.menu('icono'),
              fid_menu_padre: FIELD.menu('fid_menu_padre'),
              permissions: {
                read: FIELD.menu('method_get'),
                create: FIELD.menu('method_post'),
                update: FIELD.menu('method_put'),
                delete: FIELD.menu('method_delete')
              }
            }]
          }],
          menuEntrar: FIELD.custom('menuEntrar')
        }
      }
    }
  });

  app.post('/autenticar/empresa', app.controller.autenticacion_empresa.post);
  APIDOC.post('/autenticar/empresa', {
    name: 'Autenticar Empresa',
    group: 'Autenticacion',
    description: 'Servicio se autenticación para las empresas. Devuelve un token de acceso e información acerca del usuario.',
    input: {
      body: {
        nit: FIELD.empresa('nit'),
        usuario: FIELD.custom('usuario'),
        clave: FIELD.custom('clave')
      }
    },
    output: {
      body: {
        finalizado: FIELD.custom('finalizado'),
        mensaje: FIELD.custom('mensaje'),
        token: FIELD.custom('token'),
        datos: {
          usuario: {
            id_usuario: FIELD.usuario('id_usuario'),
            nit: FIELD.empresa('nit'),
            nombres: FIELD.empresa('nombre'),
            rol: {
              id_rol: FIELD.rol('id_rol'),
              nombre: FIELD.rol('nombre'),
              peso: FIELD.rol('peso')
            }
          },
          menu: [{
            id_menu: FIELD.menu('id_menu'),
            label: FIELD.menu('nombre'),
            url: FIELD.menu('ruta'),
            icon: FIELD.menu('icono'),
            submenu: [{
              label: FIELD.menu('nombre'),
              url: FIELD.menu('ruta'),
              icon: FIELD.menu('icono'),
              fid_menu_padre: FIELD.menu('fid_menu_padre'),
              permissions: {
                read: FIELD.menu('method_get'),
                create: FIELD.menu('method_post'),
                update: FIELD.menu('method_put'),
                delete: FIELD.menu('method_delete')
              }
            }]
          }],
          menuEntrar: FIELD.custom('menuEntrar')
        }
      }
    }
  });

  app.post('/autenticar/restablecercontrasenia', validate(paramValidation.restablecerusuario), app.controller.autenticacion.restablecerContrasenia);
  APIDOC.post('/autenticar/restablecercontrasena', {
    name: 'Restablecer contraseña de usuario',
    group: 'Autenticacion',
    description: 'Servicio para generar un link y enviarlo al correo electronico para restablecer la contraseña de usuario.',
    input: {
      body: {
        email: FIELD.empresa('email'),
      }
    },
    output: {
      body: {
        finalizado: FIELD.custom('finalizado'),
        mensaje: FIELD.custom('mensaje'),
      }
    }
  });

  app.post('/autenticar/verificarcodigo', app.controller.autenticacion.verificarCodigo);
  APIDOC.post('/autenticar/verificarCodigo', {
    name: 'Verificar código para restablecer contraseña de usuario',
    group: 'Autenticacion',
    description: 'Servicio para verificar el código para restablecer contraseña de usuario.',
    input: {
      body: {
        codigo: FIELD.empresa('codigo'),
      }
    },
    output: {
      body: {
        finalizado: FIELD.custom('finalizado'),
        mensaje: FIELD.custom('mensaje'),
      }
    }
  });

  app.post('/autenticar/cambiarcontrasenia', app.controller.autenticacion.cambiarContrasenia);
  APIDOC.post('/autenticar/cambiarcontrasenia', {
    name: 'Cambiar la contraseña de usuario verificado',
    group: 'Autenticacion',
    description: 'Servicio para cambiar la contraseña de usuario verificado.',
    input: {
      body: {
        codigo: FIELD.empresa('codigo'),
        nueva: FIELD.empresa('contrasena'),
        confirmada: FIELD.empresa('confirmacion_contrasena')
      }
    },
    output: {
      body: {
        finalizado: FIELD.custom('finalizado'),
        mensaje: FIELD.custom('mensaje'),
      }
    }
  });
};
