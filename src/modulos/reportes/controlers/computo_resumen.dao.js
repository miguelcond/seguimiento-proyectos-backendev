import Reporte from '../../../lib/reportes';
import moment from 'moment';
import util from '../../../lib/util';
import _ from 'lodash';

module.exports = (app) => {
  app.dao.computo_resumen = {};

  async function crear (rutaComputos, datosProyecto, supervision, resumenItems) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`CMR_${supervision.nro_supervision}`);
    const datosReporte = await obtenerDatos(datosProyecto, supervision, resumenItems);
    const rep = new Reporte('computo_metrico_resumen');
    return rep.pdf(datosReporte, { orientation: 'portrait', output: `${rutaComputos}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto, supervision, resumenItems) {
    supervision = util.json(supervision);
    const SUPERVISOR = await app.dao.equipo_tecnico.obtener({where: {
      fid_proyecto: supervision.fid_proyecto,
      fid_adjudicacion: supervision.fid_adjudicacion,
      cargo: 'CG_SUPERVISOR',
      estado: 'ACTIVO'
    }});
    const FISCAL = await app.dao.equipo_tecnico.obtener({where: {
      fid_proyecto: supervision.fid_proyecto,
      cargo: 'CG_FISCAL',
      estado: 'ACTIVO'
    }});
    const datosReporte = {
      fecha_elaboracion_resumen: moment().format('DD/MM/YYYY'),
      datos_proyecto: {
        nro: supervision.nro_supervision,
        nombre: supervision.proyecto.nombre,
        // empresa: supervision.proyecto.empresa.nombre,
        departamento: supervision.proyecto.dpa.departamento.nombre,
        municipio: supervision.proyecto.dpa.municipio.nombre,
        supervisor: await app.dao.persona.obtenerApellidoNombre(SUPERVISOR.usuario.persona),
        fiscal: await app.dao.persona.obtenerApellidoNombre(FISCAL.usuario.persona)
      },
      items: resumenItems
    };

    return datosReporte;
  }

  app.dao.computo_resumen.crear = crear;
};
