import util from '../../../lib/util';
import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';

module.exports = (app) => {
  app.dao.formularioB1 = {};

  async function crear (rutaFormularios, datosItems) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`B1`);
    const datosFormulario = await obtenerDatos(datosItems);
    const rep = new Reporte('formulario_B1');
    return rep.pdf(datosFormulario, { orientation: 'portrait', output: `${rutaFormularios}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosItems) {
    if (datosItems.length === 0) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    let items = [];
    let precioTotal = 0;
    for (let i in datosItems) {
      let item = util.json(datosItems[i]);
      let total = util.multiplicar([item.precio_unitario, item.cantidad]);
      precioTotal += total;
      items.push({
        nro: item.nro_item,
        descripcion: item.nombre,
        unidad: item.unidad.nombre.toUpperCase(),
        cantidad: util.convertirNumeroAString(item.cantidad),
        precio_unitario: util.convertirNumeroAString(item.precio_unitario),
        literal: util.convertirNumeroALiteral(item.precio_unitario),
        total: util.convertirNumeroAString(total)
      });
    }
    const datosFormularioB1 = {
      items,
      totales: {
        precio_total_numeral: util.convertirNumeroAString(precioTotal),
        precio_total_literal: util.convertirNumeroALiteral(precioTotal)
      }
    };

    return datosFormularioB1;
  }

  app.dao.formularioB1.crear = crear;
};
