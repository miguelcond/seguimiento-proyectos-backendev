import Reporte from '../../../lib/reportes';
import moment from 'moment';
import util from '../../../lib/util';

module.exports = (app) => {
  app.dao.certificadoAvance = {};

  async function crear (rutaCertificados, datosProyecto, supervision, resumenItems) {
    const nroCertificado = supervision.nro_supervision;
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`CA_${nroCertificado}`);
    const datosReporte = await obtenerDatos(datosProyecto, supervision, resumenItems);
    const rep = new Reporte('certificado_avance');
    return rep.pdf(datosReporte, { orientation: 'portrait', output: `${rutaCertificados}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto, supervision) {
    supervision = util.json(supervision);
    const datosReporte = {
      fecha_elaboracion_certificado: moment().format('DD/MM/YYYY'),
      datos_proyecto: {
        nro: supervision.nro_supervision,
        nombre: supervision.proyecto.nombre,
        // empresa: supervision.proyecto.empresa.nombre,
        departamento: supervision.proyecto.dpa.departamento.nombre,
        municipio: supervision.proyecto.dpa.municipio.nombre,
        contratista: 'JUAN PEREZ',
        supervisor: 'ROSA FLORES',
        residente_de_obra: 'ANA MARIA'
      },
      dias_retraso: '1',
      descuento_por_multas: util.convertirNumeroAString(0),
      presupuestos: {
        monto_contractual: util.convertirNumeroAString(0),
        total_desembolsado: util.convertirNumeroAString(0),
        total_certificado_actual: util.convertirNumeroAString(0),
        descuento_por_anticipo: util.convertirNumeroAString(0),
        descuento_por_multas: util.convertirNumeroAString(0),
        monto_pagar_certificado_actual: util.convertirNumeroAString(0),
        total_acumulado_avance_financiero: util.convertirNumeroAString(0)
      }
    };
    return datosReporte;
  }

  app.dao.certificadoAvance.crear = crear;
};
