import fs from 'fs-extra';
import Util from '../../../lib/util';
import errors from '../../../lib/errors';
import moment from 'moment';
import shortid from 'shortid';

module.exports = (app) => {
  const sequelize = app.src.db.sequelize;
  const models = app.src.db.models;

  async function porMunicipio (req, res, next) {
    try {
      console.log('----------------------->>>>>>>>>>>>>>>>>>>>>>');
      console.log(req.query);      
      console.log('----------------------->>>>>>>>>>>>>>>>>>>>>>');
      req.query.gestion = !req.query.gestion? '': req.query.gestion;
      let departamento = req.params.departamento;
      let municipio = req.params.municipio || `%${departamento}`;

      let _departamento = Util.json(await models.departamento.findOne({where: {codigo: req.params.departamento}}));
      let titulo = `REPORTES - DEPARTAMENTO ${_departamento.nombre}`;

      if(req.params.municipio){
        let _municipio = Util.json(await models.municipio.findOne({where: {codigo: req.params.municipio}}));
        titulo += ` - MUNICIPIO ${_municipio.nombre}`;        
      }

      let options = {
        where: {
          estado_proyecto: {$notIn:['REGISTRO_SOLICITUD','ASIGNACION_TECNICO_FDI','REGISTRO_PROYECTO_FDI','RECHAZADO_FDI']},
          nro_convenio: {$like:`%${req.query.gestion}`},
          fcod_municipio: {$like:`%${municipio}`},
          estado: 'ACTIVO'
        },
        include: [{
          model: sequelize.models.formulario,
          attributes: ['form_model','codigo_estado','fid_plantilla','nombre'],
          as: 'formularios',
          where: {
            nombre: {$in: ['financiamiento','beneficiarios']}
          }
        }],
        order: ['nro_convenio']
      };

      // Proyectos con evaluación aprobada
      let datos = await models.proyecto.findAll(options);

      datos = Util.json(datos);

      // Parsear datos de los formularios obtenidos
      for (let i in datos) {
        let forms = datos[i].formularios;
        for (let j in forms) {
          datos[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datos[i].formularios;
      }
      options.where.estado_proyecto = {$in:['REGISTRO_SOLICITUD','ASIGNACION_TECNICO_FDI','REGISTRO_PROYECTO_FDI']};
      // Proyectos en fase de evaluación
      let datosEval = await models.proyecto.findAll(options);

      datosEval = Util.json(datosEval);

      // Parsear datosEval de los formularios obtenidos
      for (let i in datosEval) {
        let forms = datosEval[i].formularios;
        for (let j in forms) {
          datosEval[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datosEval[i].formularios;
      }

      // Proyectos en fase de rechazo
      options.where.estado_proyecto = {$in:['RECHAZADO_FDI']};
      let datosRechazados = await models.proyecto.findAll(options);
      datosRechazados = Util.json(datosRechazados);

      // Parsear datosEval de los formularios obtenidos
      for (let i in datosRechazados) {
        let forms = datosRechazados[i].formularios;
        for (let j in forms) {
          datosRechazados[i][forms[j].nombre] = forms[j].form_model;
        }
        delete datosRechazados[i].formularios;
      }

      return res.status(200).json({
        finalizado: true,
        mensaje: 'Datos obtenidos correctamente.',
        titulo: `${titulo}`,
        datos: datos,
        datosEval,
        datosRechazados
      });
    } catch(error) {
      return next(error);
    }
  }

  app.controller.reporteProyectos = {
    porMunicipio,
  };
};
