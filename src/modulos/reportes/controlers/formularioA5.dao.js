import util from '../../../lib/util';
import errors from '../../../lib/errors';
import Reporte from '../../../lib/reportes';
import moment from 'moment';

module.exports = (app) => {
  app.dao.formularioA5 = {};

  async function crear (rutaFormularios, datosProyecto, idEmpleado, cargo, nro) {
    const { nombreArchivo } = await app.dao.reporte.obtenerNombreArchivoYCarpeta(`A5_${nro}`);
    const datosFormulario = await obtenerDatos(datosProyecto, idEmpleado, cargo);
    const rep = new Reporte('formulario_A5');
    return rep.pdf(datosFormulario, { orientation: 'portrait', output: `${rutaFormularios}/${nombreArchivo}` });
  }

  async function obtenerDatos (datosProyecto, idEmpleado, cargo) {
    const datos = datosProyecto.documentacion_exp;
    if (!datos) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    let empleado;
    for (let i in datos.empleados) {
      if (['GERENTE', 'SUPERINTENDENTE', 'DIRECTOR', 'RESIDENTE'].includes(datos.empleados[i].cargo_actual)) {
        if (datos.empleados[i].id_empleado === idEmpleado && datos.empleados[i].cargo_actual === cargo) {
          empleado = datos.empleados[i];
          break;
        }
      }
    }
    if (!empleado) {
      throw new errors.ValidationError(`No se encontró el reporte solicitado.`);
    }
    let experienciasGenerales = [];
    let cnt = 1;
    for (let i in empleado.experiencias.generales) {
      let exp = empleado.experiencias.generales[i];
      experienciasGenerales.push({
        nro: cnt++,
        empresa: exp.nombre_empresa,
        objeto_obra: exp.objeto_obra,
        monto_obra_bs: util.convertirNumeroAString(exp.monto_obra),
        cargo: exp.cargo,
        desde: moment(exp.fecha_inicio, 'YYYY-MM-DD').format('DD/MM/YYYY'),
        hasta: moment(exp.fecha_fin, 'YYYY-MM-DD').format('DD/MM/YYYY')
      });
    }
    let experienciasEspecificas = [];
    cnt = 1;
    for (let i in empleado.experiencias.especificas) {
      let exp = empleado.experiencias.generales[i];
      experienciasEspecificas.push({
        nro: cnt++,
        empresa: exp.nombre_empresa,
        objeto_obra: exp.objeto_obra,
        monto_obra_bs: util.convertirNumeroAString(exp.monto_obra),
        cargo: exp.cargo,
        desde: moment(exp.fecha_inicio, 'YYYY-MM-DD').format('DD/MM/YYYY'),
        hasta: moment(exp.fecha_fin, 'YYYY-MM-DD').format('DD/MM/YYYY')
      });
    }
    const fechaNacimiento = new Date(empleado.persona.fecha_nacimiento);
    let datosFormularioA5 = {
      empresa: datos.datos_empresa.razon_social,
      entidad_beneficiaria: datosProyecto.tipo==='TP_CIFE'? 'UNIDAD DE PROYECTOS ESPECIALES - UPRE': datosProyecto.entidad_beneficiaria,
      objeto_contratacion: datosProyecto.nombre,
      cargo: empleado.cargo_actual,
      datos_generales: {
        nombre: await app.dao.persona.obtenerApellidoNombre(empleado.persona),
        ci: empleado.persona.documento_identidad,
        expedido_en: empleado.persona.lugar_expedicion_documento,
        edad: util.obtenerEdad(fechaNacimiento),
        nacionalidad: empleado.persona.nacionalidad,
        profesion: empleado.profesion,
        nro_registro_profesional: empleado.nro_registro_profesional
      },
      experiencias: {
        generales: experienciasGenerales,
        especificas: experienciasEspecificas
      },
      declaracion: {
        nombre: await app.dao.persona.obtenerNombreApellido(empleado.persona),
        lugar: datos.datos_empresa.ciudad,
        fecha: moment().locale('es').format('LL')
      }
    };

    return datosFormularioA5;
  }

  app.dao.formularioA5.crear = crear;
};
