import sequelizeFormly from 'sequelize-formly';
import validate from 'express-validation';
import paramValidation from './log.validation';
import { apidoc as APIDOC, field as FIELD } from '../../../lib/apidoc-generator';

module.exports = (app) => {
  const R = app.src.config.config.api.main;

  // app.api.options('/log/:codigo', sequelizeFormly.formly(app.src.db.models.log, app.src.db.models));
  app.api.get('/proyectos/:codigo/logs', validate(paramValidation.get), app.controller.log.get);
  APIDOC.get(`${R}/proyectos/:codigo/logs`, {
    name: 'Obtener log de estados de un proyecto.',
    group: 'Log',
    description: 'Devuelve la información de los estados, a partir del código de proyecto.',
    input: {
      params: {
        codigo: FIELD.log('objeto_tipo', { allowNull: false })
      }
    },
    output: {
      body: {
        codigo: FIELD.estado('id_proyecto'),
        observacion: FIELD.estado('observacion'),
        fid_usuario: FIELD.estado('fid_usuario'),
        fid_usuario_rol: FIELD.estado('fid_usuario_rol'),
      }
    }
  });
};
