const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  get: {
    params: {
      codigo: Joi.number().required(),
    },
    query: {
      limit: Joi.number(),
      page: Joi.number(),
    }
  },
};
