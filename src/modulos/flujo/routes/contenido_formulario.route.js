import sequelizeFormly from 'sequelize-formly';
import validate from 'express-validation';
// import paramValidation from './contenido_formulario.validation';
import { apidoc as APIDOC, field as FIELD } from '../../../lib/apidoc-generator';

module.exports = (app) => {
  const R = app.src.config.config.api.main;

  /**
   * @apiVersion 1.0.0
   * @apiGroup Contenido de Formularios
   * @apiName Listar contenidos
   * @api {get} /api/v1/contenidos/:nombre Obtiene el contenido de los formularios con un determinado nombre
   *
   * @apiDescription Listar contenidos por nombre de formulario
   * 
   * @apiParam (Params) {string} nombre Nombre del formulario
   * 
   * @apiSuccess (Respuesta) {boolean} finalizado Indica el estado del proceso solicitado
   * @apiSuccess (Respuesta) {string} mensaje Mensaje a ser visualizado
   * @apiSuccess (Respuesta) {object} datos Objeto que contiene los datos de los formularios
   * 
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Se obtuvo la lista exitosamente.",
      "datos": [
        {
          "seccion": 1,
          "estructura": [
            {
              "cols": [
                {
                  "rowspan": 34,
                  "titulo": "1. DIAGNOSTICO DE LA SITUACION ACTUAL"
                },
                {
                  "rowspan": 2,
                  "titulo": "1.1. DETERMINAR EL AREA DEL PROYECTO"
                }
                ...
              ]
            },
            ...
          ]
        },
        {
          "seccion": 2,
          "estructura": [
            {
              "cols": [
                {
                  "contenido": "El proyecto debe haber sido concebido de manera participativa.."
                },
                {
                  "contenido": "Acta de consenso sobre la  conformidad del EDTP, firmada..."
                },
                ...
              ]
            },
            ...
          ]
        },
        {
          "seccion": 3,
          "estructura": [
            {
              "key": "agricola",
              "cols": [
                {
                  "rowspan": 4,
                  "titulo": "Agricola"
                },
                {
                  "contenido": "Prácticas de Manejo Integral de plagas y enfermedades"
                },
                ...
              ]
            },
            ...
          ]
        },
        {
          "seccion": 4,
          "estructura": [
            {
              "key": "seccion_4_1",
              "cols": [
                {
                  "contenido": "Sustentación de la selección de la alternativa del proyecto (Únicamente para proyectos medianos)",
                  "rowspan": 2
                },
                {
                  "contenido": "Pertinencia de la aplicación del VAC o CAE"
                },
                ...
              ]
            },
            ...
          ]
        },
        {
          "seccion": 5,
          "estructura": [
            {
              "cols": [
                {
                  "contenido": "De acuerdo a la revisión realizada se recomienda:"
                },
                {
                  "contenido": {
                    "name": "APROBADO",
                    "type": "radio",
                    "value": false
                  }
                },
                ...
              ]
            }
          ]
        }
      ]
    }
   */
  app.api.get('/contenidos/:nombre/', app.controller.contenido_formulario.listarContenido);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Contenido de Formularios
   * @apiName Obtener contenido
   * @api {get} /api/v1/contenidos/:nombre/:seccion Obtiene el contenido de una sección de un formulario
   *
   * @apiDescription Obtener contenido, devuelve el contenido de una sección de un formulario según su nombre
   * 
   * @apiParam (Params) {string} nombre Nombre del formulario
   * @apiParam (Params) {string} seccion Sección del formulario
   * 
   * @apiSuccess (Respuesta) {boolean} finalizado Indica el estado del proceso solicitado
   * @apiSuccess (Respuesta) {string} mensaje Mensaje a ser visualizado
   * @apiSuccess (Respuesta) {object} datos Objeto que contiene los datos de una sección del formulario
   * 
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "COntenido de formulario recuperado correctamente",
      "datos": {
        "estructura": [
          {
            "className": "col-md-6 col-lg-4",
            "key": "agricola",
            "templateOptions": {
              "disabled": false,
              "label": "Agrícola"
            },
            "type": "checkbox"
          },
          {
            "className": "col-md-6 col-lg-4",
            "key": "prod_Agroeco_organica",
            "templateOptions": {
              "disabled": false,
              "label": "Producción Agroecológica y Orgánica"
            },
            "type": "checkbox"
          },
          {
            "className": "col-md-6 col-lg-4",
            "key": "integral_sustentable_bosque",
            "templateOptions": {
              "disabled": false,
              "label": "Manejo Integral y Sustentable de Bosques"
            },
            "type": "checkbox"
          },
          {
            "className": "col-md-6 col-lg-4",
            "key": "sis_agroforestales",
            "templateOptions": {
              "disabled": false,
              "label": "Manejo en SIstemas agroforestales"
            },
            "type": "checkbox"
          },
          {
            "className": "col-md-6 col-lg-4",
            "key": "aprovecha_biodiversidad",
            "templateOptions": {
              "disabled": false,
              "label": "Aprovechamiento sustentable de la biodiversidad"
            },
            "type": "checkbox"
          }
        ]
      }
    }
   */
  app.api.get('/contenidos/:nombre/:seccion', app.controller.contenido_formulario.obtenerContenido);
};
