import util from '../../../lib/util';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.controller.log = {};
  const logController = _app.controller.log;
  const logModel = app.src.db.models.log;

  async function get (req, res, next) {
    const USUARIO = req.body.audit_usuario;
    try {
      let proy = await app.dao.log.obtenerLog(req.params.codigo, USUARIO);
      if (!proy) {
        throw new errors.NotFoundError('No se ha encontrado el proyecto.');
      }
      for (let i in proy.logs) {
        if (proy.logs[i].usuario_rol.nombre==='EMPRESA' && !proy.logs[i].usuario.persona) {
          proy.logs[i].usuario.persona = {
            nombres: '' //proy.documentacion_exp.datos_empresa.razon_social,
          }
        }
      }
      res.json({
        finalizado: true,
        mensaje: 'Historial de estados del Proyecto recuperado correctamente.',
        datos: proy.logs
      });
    } catch (error) {
      next(error);
    }
  }

  _app.controller.log.get = get;
};
