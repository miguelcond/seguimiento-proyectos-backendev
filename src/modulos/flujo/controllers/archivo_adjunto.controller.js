import seqOpt from '../../../lib/sequelize-options';
import util from '../../../lib/util';
import moment from 'moment';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.controller.archivo_adjunto = {};
  const archivoController = _app.controller.archivo_adjunto;
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  archivoController.getArchivos = async (req, res) => {
    const ID_PROYECTO = req.params.id;
    try {
      let archivos = await models.archivo_adjunto.findAll({
        where: {
          estado: 'ACTIVO',
          registrable_id: ID_PROYECTO,
          registrable_tipo: 'proyecto',
        }
      });
      res.json({
        finalizado: true,
        mensaje: 'Archivos recuperados correctamente.',
        datos: { count:archivos.length, rows:archivos }
      });
    } catch (error) {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    }
  };

  archivoController.getArchivoId = async (req, res) => {
    const archivoDao = _app.dao.archivo_adjunto;
    const ID_PROYECTO = req.params.id;
    try {
      let archivo = util.json(await models.archivo_adjunto.findOne({
        where: {
          id_archivo_adjunto: req.params.id_archivo,
          registrable_id: ID_PROYECTO,
        }
      }));
      if (archivo) {
        archivo.registrable_id = undefined;
        archivo.registrable_tipo = undefined;
        archivo._usuario_creacion = undefined;
        archivo._usuario_modificacion = undefined;
        archivo._fecha_creacion = undefined;
        archivo._fecha_modificacion = undefined;
      }
      archivo.file = archivoDao.leerArchivo(archivo.ruta);
      res.json({
        finalizado: true,
        mensaje: 'Archivo recuperado correctamente.',
        datos: archivo
      });
    } catch (error) {
      res.status(412).json({
        finalizado: false,
        mensaje: error.message,
        datos: null
      });
    }
  };

  archivoController.crearArchivo = async (req, res, next) => {
    const archivoDao = _app.dao.archivo_adjunto;
    const ID_PROYECTO = req.params.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;

    return sequelize.transaction(async(t) => {
      // Validaciones
      let proyecto = util.json(await models.proyecto.findById(ID_PROYECTO));
      if (!proyecto) throw new errors.ValidationError('No tiene permisos en el proyecto');
      let body = req.body.file || {};
      if (!body || !body.base64) throw new errors.ValidationError('Falta la cadena base64 del archivo');
      let archivo = archivoDao.escribirBase64(null, body.base64);

      // Valores renombrados
      if (!body.roles) body.roles = [];
      if (!body.mime_type) body.mime_type = body.tipo;
      if (!body.nombre_original) body.nombre_original = body.nombre;
      if (!body.titulo) body.titulo = req.body.datos.titulo;
      if (!body.fecha_inicio) body.fecha_inicio = req.body.datos.fecha_inicio
      if (!body.fecha_fin) body.fecha_fin = req.body.datos.fecha_fin;

      // Valores por defecto
      body.codigo_estado = proyecto.estado_proyecto;
      body._usuario_creacion = ID_USUARIO;
      body.ruta = archivo.path;
      body.registrable_id = ID_PROYECTO;
      body.registrable_tipo = 'proyecto';

      let result = {};
      if (req.body.p == 'save' && !req.body.datos.id_archivo_adjunto) {
        result = await models.archivo_adjunto.findOne({
          where: {
            mime_type: body.mime_type,
            titulo: body.titulo,
            codigo_estado: body.codigo_estado,
            _usuario_creacion: body._usuario_creacion,
            ruta: body.ruta,
          }
        });
      }
      if (req.body.p == 'change' && req.body.datos.id_archivo_adjunto) {
        result = await models.archivo_adjunto.findOne({
          where: {
            id_archivo_adjunto: req.body.datos.id_archivo_adjunto,
          }
        });
        Object.assign(result, body);
      }
      if (!result) {
        result = util.json(await models.archivo_adjunto.create(body));
      } else {
        result.estado = 'ACTIVO';
        await result.save();
      }
      return result;
    }).then((result) => {
      res.json({
        finalizado: true,
        mensaje: 'Archivo guardado correctamente.',
        datos: result
      });
    }).catch((error) => {
      next(error);
    });
  };

  archivoController.modificarArchivoId = async (req, res, next) => {
    const archivoDao = _app.dao.archivo_adjunto;
    const ID_PROYECTO = req.params.id;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;

    return sequelize.transaction(async(t) => {
      // Validaciones
      let proyecto = util.json(await models.proyecto.findById(ID_PROYECTO));
      if (!proyecto) throw new errors.ValidationError('No tiene permisos en el proyecto');
      let body = req.body || {};
      if (!req.body.id_archivo_adjunto || req.body.id_archivo_adjunto!=req.params.id_archivo) throw new errors.ValidationError('Falta la cadena base64 del archivo');
      let result = {};
      result = await models.archivo_adjunto.findOne({
        where: {
          id_archivo_adjunto: req.body.id_archivo_adjunto,
        }
      });
      if (req.body.p == 'change') {

      } else if (req.body.p == 'delete') {
        if (result) {
          let shared = await models.archivo_adjunto.findOne({
            where: {
              ruta: result.ruta,
              estado: 'ACTIVO',
              $not: {
                id_archivo_adjunto: req.body.id_archivo_adjunto,
              }
            }
          });
          if (!shared) {
            await archivoDao.eliminarArchivo(result.ruta);
          }
          result._usuario_modificacion = ID_USUARIO;
          result.estado = 'ELIMINADO';
          await result.save();
        }
      }
      return result;
    }).then((result) => {
      res.json({
        finalizado: true,
        mensaje: 'Archivo guardado correctamente.',
        datos: result
      });
    }).catch((error) => {
      next(error);
    });
  };
};
