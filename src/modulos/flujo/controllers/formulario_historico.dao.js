import util from '../../../lib/util';
import errors from '../../../lib/errors';

module.exports = (app) => {
  const _app = app;
  _app.dao.formulario_historico = {};
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  async function crear (form, t) {
    // try {
      delete form._fecha_creacion;
      delete form._fecha_modificacion;
      form = await models.formulario_historico.create(form, { transaction: t });
      if (!form) {
        throw new errors.ValidationError('No se actualizó los datos del formulario.');
      }
      return util.json(form);
    // } catch(e) {
    //   console.log('ERROR', e);
    //   throw new errors.ValidationError('Error al persistir generar la pase de datos');
    // }
  }

  async function listarHistorico (form) {
    let options = {
      attributes: ['id_formulario','nombre','codigo_documento','fecha_documento', [sequelize.json('form_model.cod'),'id_proyecto'],'_fecha_creacion', 'estado'],
      where: {
        fid_proyecto: form.id_proyecto,
        fid_plantilla: form.id_plantilla,
        nombre: form.nombre,
      },
      order: ['_fecha_creacion','_fecha_modificacion']
    };
    let result = await models.formulario_historico.findAll(options);
    return util.json(result);
  }

  async function obtenerHistorico (where) {
    let options = {
      attributes: ['id_formulario','version','form_model','codigo_documento','_fecha_creacion','_fecha_modificacion'],
      where: where
    }
    let result = await models.formulario_historico.findAll(options);
    return util.json(result);
  }

  _app.dao.formulario_historico.crear = crear;
  _app.dao.formulario_historico.listarHistorico = listarHistorico;
  _app.dao.formulario_historico.obtenerHistorico = obtenerHistorico;
};
