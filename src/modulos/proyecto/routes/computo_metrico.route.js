const validate = require('express-validation');
const paramValidation = require('./computo_metrico.validation.js');
module.exports = (app) => {
  /**
   * @apiVersion 1.0.0
   * @apiGroup Computos Metricos
   * @apiName Get cómputos métricos
   * @api {get} /api/v1/computos_metricos/?item=1&supervision=1 Obtener cómputos métricos
   *
   * @apiDescription Recupera los datos de un computo metrico pertenieciente a un item y supervision
   *
   * @apiSuccessExample {json} Respuesta:
    {
      "finalizado": true,
      "mensaje": "Cómputo métricos obtenidos correctamente",
      "datos": {
        "cantidad_anterior": 0,
        "computos": [
          {
            "id_computo_metrico": 1,
            "fid_item": 3,
            "fid_supervision": 1,
            "orden": 1,
            "descripcion": "asdasdasd4",
            "cantidad": 7,
            "largo": 13,
            "ancho": null,
            "alto": 15,
            "factor_forma": 1,
            "area": 195,
            "volumen": null,
            "cantidad_parcial": 1365,
            "subtitulo": null,
            "uuid": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 494937017,
            "fecha_creacion": null,
            "_usuario_modificacion": 494937017,
            "fecha_modificacion": null,
            "_fecha_creacion": "2017-11-10T14:27:16.408Z",
            "_fecha_modificacion": "2017-11-10T15:09:33.133Z"
          },
          {
            "id_computo_metrico": 2,
            "fid_item": 3,
            "fid_supervision": 1,
            "orden": 2,
            "descripcion": "qweqwe1",
            "cantidad": 8,
            "largo": 10,
            "ancho": null,
            "alto": 12,
            "factor_forma": 1,
            "area": 120,
            "volumen": null,
            "cantidad_parcial": 960,
            "subtitulo": null,
            "uuid": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 494937017,
            "fecha_creacion": null,
            "_usuario_modificacion": null,
            "fecha_modificacion": null,
            "_fecha_creacion": "2017-11-10T15:45:30.212Z",
            "_fecha_modificacion": "2017-11-10T15:45:30.212Z"
          },
          {
            "id_computo_metrico": 3,
            "fid_item": 3,
            "fid_supervision": 1,
            "orden": 3,
            "descripcion": "zczxczxc",
            "cantidad": 1,
            "largo": 20,
            "ancho": null,
            "alto": 20,
            "factor_forma": -1,
            "area": -400,
            "volumen": null,
            "cantidad_parcial": -400,
            "subtitulo": null,
            "uuid": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 494937017,
            "fecha_creacion": null,
            "_usuario_modificacion": null,
            "fecha_modificacion": null,
            "_fecha_creacion": "2017-11-10T15:59:24.081Z",
            "_fecha_modificacion": "2017-11-10T15:59:24.081Z"
          }
        ],
        "fotos": [
          {
            "id_foto_supervision": 15,
            "nombre": "foto1_croquis.JPG"
          },
          {
            "id_foto_supervision": 16,
            "nombre": "foto2_croquis.JPG"
          }
        ]
      }
    }
   */
  app.api.get('/computos_metricos', validate(paramValidation.get), app.controller.computoMetrico.get);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Computos Metricos
   * @apiName Post cómputos métricos
   * @api {post} /api/v1/computos_metricos Crear cómputo métrico
   *
   * @apiDescription Registra un cómputo métrico
   *
   * @apiParamExample {json} Ejemplo petición:
   *{
   * GLB
     "fid_item": 1,
     "fid_supervision": 1,
     "orden": 1,
     "descripcion": "Salas",
     "cantidad": 1
   * PZA
     "fid_item": 2,
     "fid_supervision": 1,
     "orden": 2,
     "descripcion": "Piso 1",
     "cantidad": 2
   * ML
     "fid_item": 3,
     "fid_supervision": 1,
     "orden": 3,
     "descripcion": "Piso 1",
     "cantidad": 4,
     "largo": 10
   * M2
     "fid_item": 4,
     "fid_supervision": 1,
     "orden": 4,
     "descripcion": "Piso 1",
     "cantidad": 4,
     "largo": 6,
     "alto":3,
     "factor_forma": 1
  * M3
     "fid_item": 5,
     "fid_supervision": 1,
     "orden": 0,
     "descripcion": "Test",
     "cantidad": 5,
     "cantidad_parcial": "40.00",
     "largo": 2,
     "ancho": 4,
     "alto": 1,
     "area": "8.00",
     "factor_forma": 1,
     "subtitulo": null
   }
   * @apiSuccessExample {json} Respuesta:
   {
     "finalizado": true,
     "mensaje": "Cómputo métrico registrado correctamente",
     "datos": {
       "alto": 1,
       "ancho": 4,
       "area": null,
       "cantidad": 5,
       "cantidad_parcial": 40,
       "descripcion": "second test",
       "estado": "ACTIVO",
       "factor_forma": 1,
       "fecha_creacion": null,
       "fecha_modificacion": null,
       "fid_item": 2,
       "fid_supervision": 1,
       "id_computo_metrico": 2,
       "largo": 2,
       "orden": 0,
       "subtitulo": null,
       "uuid": null,
       "volumen": 8,
       "_fecha_creacion": "2018-07-11T19:37:32.204Z",
       "_fecha_modificacion": "2018-07-11T19:37:32.204Z",
       "_usuario_creacion": 37,
       "_usuario_modificacion": null
     }
   }
   */
  app.api.post('/computos_metricos', validate(paramValidation.create), app.controller.computoMetrico.post);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Computos Metricos
   * @apiName Put cómputos métricos
   * @api {put} /api/v1/computos_metricos/:id Actualizar cómputo métrico
   *
   * @apiDescription Actualiza el contenido de un cómputo métrico
   *
   * @apiParamExample {json} Ejemplo petición:
   *{
      "alto": 4,
      "area": "8.00",
      "cantidad": 2,
      "cantidad_parcial": "16.00",
      "descripcion": "test description",
      "estado": "ACTIVO",
      "factor_forma": 1,
      "fid_item": 1,
      "fid_supervision": 1,
      "id_computo_metrico": 1,
      "largo": 2,
      "orden": 0,
      "subtitulo": null
    }
   * @apiSuccessExample {json} Respuesta:
   *{
      "finalizado": true,
      "mensaje": "Cómputo métrico actualizado correctamente",
      "datos": {
        "alto": 4,
        "ancho": null,
        "area": 8,
        "cantidad": 2,
        "cantidad_parcial": 16,
        "descripcion": "test description",
        "estado": "ACTIVO",
        "factor_forma": 1,
        "fecha_creacion": null,
        "fecha_modificacion": null,
        "fid_item": 1,
        "fid_supervision": 1,
        "id_computo_metrico": 1,
        "largo": 2,
        "orden": 0,
        "subtitulo": null,
        "uuid": null,
        "volumen": null,
        "_fecha_creacion": "2018-07-11T19:36:33.942Z",
        "_fecha_modificacion": "2018-07-11T19:56:39.283Z",
        "_usuario_creacion": 37,
        "_usuario_modificacion": 37
      }
   *}
   */
  app.api.put('/computos_metricos/:id', validate(paramValidation.update), app.controller.computoMetrico.put);

  /**
  * @apiVersion 1.0.0
  * @apiGroup Computos Metricos
  * @apiName Put Computo Metrico Item
  * @api {put} /api/v1/computos_metricos/items/:id Actualización por bloque de cómputos métricos de un item
  *
  * @apiDescription Registra o actualiza cómputo(s) métrico(s) por bloque de un item determinado
  *
  * @apiParamExample {json} Ejemplo petición:
  * {
      "computos": [
        {
          "alto": 4,
          "area": "8.00",
          "cantidad": 2,
          "cantidad_parcial": "16.00",
          "descripcion": "first row",
          "estado": "ACTIVO",
          "factor_forma": 1,
          "fid_item": 1,
          "fid_supervision": 1,
          "id_computo_metrico": 1,
          "largo": 2,
          "orden": 0
        },
        {
          "alto": 2,
          "area": "10.00",
          "cantidad": 1,
          "cantidad_parcial": "10.00",
          "descripcion": "second row",
          "factor_forma": 5,
          "fid_item": 1,
          "fid_supervision": 1,
          "largo": 1,
          "orden": 1
        }
      ]
    }
  * @apiSuccessExample {json} Respuesta:
  * {
      "finalizado": true,
      "mensaje": "Computos metricos actualizados correctamente",
      "datos": [
        {
          "alto": 4,
          "ancho": null,
          "area": 8,
          "cantidad": 2,
          "cantidad_parcial": 16,
          "descripcion": "first row",
          "estado": "ACTIVO",
          "factor_forma": 1,
          "fecha_creacion": null,
          "fecha_modificacion": null,
          "fid_item": 1,
          "fid_supervision": 1,
          "id_computo_metrico": 1,
          "largo": 2,
          "orden": 0,
          "subtitulo": null,
          "uuid": null,
          "volumen": null,
          "_fecha_creacion": "2018-07-11T19:36:33.942Z",
          "_fecha_modificacion": "2018-07-11T21:01:12.695Z",
          "_usuario_creacion": 37,
          "_usuario_modificacion": 37
        },
        {
          "alto": 2,
          "ancho": null,
          "area": 10,
          "cantidad": 1,
          "cantidad_parcial": 10,
          "descripcion": "second row",
          "estado": "ACTIVO",
          "factor_forma": 5,
          "fecha_creacion": null,
          "fecha_modificacion": null,
          "fid_item": 1,
          "fid_supervision": 1,
          "id_computo_metrico": 3,
          "largo": 1,
          "orden": 1,
          "subtitulo": null,
          "uuid": null,
          "volumen": null,
          "_fecha_creacion": "2018-07-11T20:53:44.723Z",
          "_fecha_modificacion": "2018-07-11T21:01:12.703Z",
          "_usuario_creacion": 37,
          "_usuario_modificacion": 37
        }
      ]
    }
  */
  app.api.put('/computos_metricos/items/:id', validate(paramValidation.updatePorBloque), app.controller.computoMetrico.actualizarComputoMetricoItem);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Computos Metricos
   * @apiName Delete
   * @api {delete} /api/v1/computos_metricos/:id Eliminar cómputo métrico
   *
   * @apiDescription Elimina un registro de cómputo métrico
   *
   * @apiSuccessExample {json} Respuesta:
   *
    {
      "finalizado": true,
      "mensaje": "Cómputo métrico eliminado correctamente",
      "datos": true
    }
   */
  app.api.delete('/computos_metricos/:id', validate(paramValidation.delete), app.controller.computoMetrico.delete);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Computos Metricos
   * @apiName Delete All
   * @api {delete} /computos_metricos/:id/todos/:idItem Elimina todos los cómputos métricos de una supervision
   *
   * @apiDescription Elimina todos los registro de cómputos métricos de un item / supervision
   *
   * @apiSuccessExample {json} Respuesta:
   *
    {
      "finalizado": true,
      "mensaje": "Cómputos métricos eliminados correctamente",
      "datos": true
    }
   */
  app.api.delete('/computos_metricos/:id/todos/:idItem', validate(paramValidation.deleteAll), app.controller.computoMetrico.deleteAll);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Computos Metricos
   * @apiName Get supervision
   * @api {get} /api/v1/computos_metricos/supervision/:id Obtener cómputos métricos
   *
   * @apiDescription Obtiene compustos metricos de una planilla de supervision
   */
  app.api.get('/computos_metricos/supervision/:id', validate(paramValidation.getId), app.controller.computoMetrico.getSupervision);
};
