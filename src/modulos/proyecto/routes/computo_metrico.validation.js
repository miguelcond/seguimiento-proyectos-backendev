const BaseJoi = require('./../../../lib/joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  getId: {
    params: {
      id: Joi.number().required()
    }
  },
  get: {
    query: {
      item: Joi.number().required(),
      supervision: Joi.number().required()
    }
  },
  create: {
    body: {
      fid_item: Joi.number().required(),
      fid_supervision: Joi.number().required(),
      orden: Joi.number().optional(),
      descripcion: Joi.string().required(),
      cantidad: Joi.number().required(),
      factor_forma: Joi.number().optional(),
      largo: Joi.number().optional(),
      ancho: Joi.number().optional(),
      alto: Joi.number().optional()
    }
  },
  update: {
    params: {
      id: Joi.number().required()
    },
    body: {
      orden: Joi.number().optional(),
      descripcion: Joi.string().required(),
      cantidad: Joi.number().required(),
      factor_forma: Joi.number().optional(),
      largo: Joi.number().optional(),
      ancho: Joi.number().optional(),
      alto: Joi.number().optional()
    }
  },
  delete: {
    params: {
      id: Joi.number().required()
    }
  },
  deleteAll: {
    params: {
      id: Joi.number().required(),
      idItem: Joi.number().required()
    }
  },
  updatePorBloque: {
    params: {
      id: Joi.number().required()
    },
    body: {
      computos: Joi.array().items({
        fid_item: Joi.number().required(),
        fid_supervision: Joi.number().required(),
        orden: Joi.number().optional(),
        descripcion: Joi.string().required(),
        cantidad: Joi.number().required(),
        factor_forma: Joi.number().optional(),
        largo: Joi.number().optional(),
        ancho: Joi.number().optional(),
        alto: Joi.number().optional()
      })
    }
  }
};
