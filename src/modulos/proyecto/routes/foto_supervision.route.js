const validate = require('express-validation');
const paramValidation = require('./foto_supervision.validation');

module.exports = (app) => {
  /*
   * @apiVersion 1.0.0
   * @apiGroup foto_supervision fotos
   * @apiName Post foto_supervision fotos
   * @api {post} /api/v1/fotos Crear foto de la supervision
   *
   * @apiDescription Post para foto
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "fid_supervision": 2,
      "path_fotografia": "prueba.txt",
      "base64": "UHJvYmFuZG8="
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "La supervision ya fue aprobada.",
      "datos": null
    }
   */
  app.api.post('/fotos', validate(paramValidation.create), app.controller.fotoSupervision.post);
  /*
   * @apiVersion 1.0.0
   * @apiGroup foto_supervision fotos
   * @apiName Put foto_supervision fotos
   * @api {put} /api/v1/fotos Modificar foto de la supervision
   *
   * @apiDescription Put para foto
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "fid_supervision": 2,
      "path_fotografia": "prueba.txt",
      "base64": "UHJvYmFuZG8="
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "La supervision ya fue aprobada.",
      "datos": null
    }
   */
  app.api.put('/fotos/:id', validate(paramValidation.update), app.controller.fotoSupervision.update);
  /*
   * @apiVersion 1.0.0
   * @apiGroup foto_supervision fotos
   * @apiName Get foto_supervision fotos
   * @api {get} /api/v1/fotos/:id Crear foto de la supervision
   *
   * @apiDescription Get para foto
   *
   * @apiParamExample {id} Id de la foto:
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Foto recuperada correctamente.",
      "datos": "/9j/4AAQSkZJRgABAQEAYABgAAD/4QBgR.....ooAKKKKACiiigAooooA//Z"
    }
   */
  app.api.get('/fotos/:id', validate(paramValidation.getId), app.controller.fotoSupervision.getId);
  /*
   * @apiVersion 1.0.0
   * @apiGroup foto_supervision fotos
   * @apiName Get foto_supervision fotos
   * @api {delete} /api/v1/fotos/:id Eliminar foto de la supervision
   *
   * @apiDescription Delete para foto
   *
   * @apiParamExample {id} Id de la foto:
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Foto eliminada correctamente.",
      "datos": "null"
    }
   */
  app.api.delete('/fotos/:id', validate(paramValidation.delete), app.controller.fotoSupervision.delete);

  app.api.get('/fotos/supervision/:id', validate(paramValidation.getId), app.controller.fotoSupervision.getFotosSupervision);
  app.api.get('/fotos/items/:id', validate(paramValidation.getId), app.controller.fotoSupervision.getFotosSupervisionItem);
};
