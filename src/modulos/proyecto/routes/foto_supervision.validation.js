const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);

module.exports = {
  getId: {
    params: {
      id: Joi.number().required()
    }
  },
  get: {
    params: { }
  },
  create: {
    body: {
      fid_supervision: Joi.number().allow(null).allow('').optional(),
      fid_item: Joi.number().allow(null).allow('').optional(),
      path_fotografia: Joi.string().required(),
      hash: Joi.string().allow(null).allow('').optional(),
      coordenadas: Joi.object({
        type: Joi.string().required(),
        coordinates: Joi.array().required()
      }).optional(),
      uuid: Joi.string().allow(null).allow('').optional(),
      base64: Joi.string().required()
    }
  },
  update: {
    params: {
      id: Joi.number().required()
    },
    body: {
      fid_supervision: Joi.number().allow(null).allow('').optional(),
      fid_item: Joi.number().allow(null).allow('').optional(),
      path_fotografia: Joi.string().required(),
      hash: Joi.string().allow(null).allow('').optional(),
      coordenadas: Joi.object({
        type: Joi.string().required(),
        coordinates: Joi.array().required()
      }).optional(),
      uuid: Joi.string().allow(null).allow('').optional(),
      base64: Joi.string().required()
    }
  },
  delete: {
    params: {
      id: Joi.number().required()
    }
  }
};
