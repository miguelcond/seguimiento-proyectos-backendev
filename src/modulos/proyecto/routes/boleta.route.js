import validate from 'express-validation';
import paramValidation from './boleta.validation';

module.exports = (app) => {

  app.api.post('/proyectos/:id_proyecto/boletas', validate(paramValidation.crear), app.controller.boleta.crear);
  app.api.put('/proyectos/:id_proyecto/boletas/:id_boleta', validate(paramValidation.modificar), app.controller.boleta.modificar);
  app.api.post('/boletas/proyecto/:id_proyecto/', validate(paramValidation.adicionar), app.controller.boleta.adicionar);
};
