const validate = require('express-validation');
const paramValidation = require('./item.validation');
const sequelizeFormly = require('sequelize-formly');

module.exports = (app) => {
  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Get items
   * @api {get} /items Obtiene los items
   *
   * @apiDescription Get items, Obtiene los items y opcionalmente se puede indicar el estado
   *
   * @apiSuccess (Respuesta) {Boleano} finalizado Indica el estado del proceso solicitado
   * @apiSuccess (Respuesta) {Texto} mensaje Mensaje a ser visualizado
   * @apiSuccess (Respuesta) {Objeto} datos Objeto que contiene información sobre los items
   *
   * @apiSuccessExample {json} Respuesta:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Items recuperados correctamente.",
      "datos": {
        "count": 8,
        "rows": [
          {
            "id_item": 1,
            "fid_modulo": 1,
            "nro_item": 1,
            "nombre": "INSTALACION DE FAENAS",
            "unidad_medida": "UN_GLOBAL",
            "cantidad": 1,
            "precio_unitario": 67102.03,
            "costo_total": 67102.03,
            "tiempo_ejecucion": 24,
            "avance_total": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:19:49.074Z",
            "_fecha_modificacion": "2017-09-06T22:19:49.074Z"
          },
          {
            "id_item": 2,
            "fid_modulo": 1,
            "nro_item": 2,
            "nombre": "REPLANTEO Y TRAZADO",
            "unidad_medida": "UN_METRO_CUAD",
            "cantidad": 3125,
            "precio_unitario": 3.72,
            "costo_total": 11625,
            "tiempo_ejecucion": 40,
            "avance_total": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:20:02.450Z",
            "_fecha_modificacion": "2017-09-06T22:20:02.450Z"
          },
          {
            "id_item": 3,
            "fid_modulo": 2,
            "nro_item": 3,
            "nombre": "EXCAVACION CON MAQUINARIA",
            "unidad_medida": "UN_METRO_CUB",
            "cantidad": 4297.65,
            "precio_unitario": 43.27,
            "costo_total": 185959.3155,
            "tiempo_ejecucion": 12,
            "avance_total": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:21:06.896Z",
            "_fecha_modificacion": "2017-09-06T22:21:06.896Z"
          },
          {
            "id_item": 4,
            "fid_modulo": 2,
            "nro_item": 4,
            "nombre": "HORMIGON ARMADO ZAPATAS",
            "unidad_medida": "UN_METRO_CUB",
            "cantidad": 382.94,
            "precio_unitario": 2484.52,
            "costo_total": 951422.0888,
            "tiempo_ejecucion": 12,
            "avance_total": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:22:01.620Z",
            "_fecha_modificacion": "2017-09-06T22:22:01.620Z"
          },
          {
            "id_item": 5,
            "fid_modulo": 3,
            "nro_item": 5,
            "nombre": "EXCAVACION P/CIMIENTO W  co 0.50x0.50",
            "unidad_medida": "UN_METRO_CUB",
            "cantidad": 73.85,
            "precio_unitario": 87.49,
            "costo_total": 6461.1365,
            "tiempo_ejecucion": 12,
            "avance_total": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:22:52.071Z",
            "_fecha_modificacion": "2017-09-06T22:22:52.071Z"
          },
          {
            "id_item": 6,
            "fid_modulo": 3,
            "nro_item": 6,
            "nombre": "PISO CERAMICA ESMALTADA NACIONAL",
            "unidad_medida": "UN_METRO_CUAD",
            "cantidad": 337.46,
            "precio_unitario": 140.04,
            "costo_total": 47257.8984,
            "tiempo_ejecucion": 12,
            "avance_total": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:49:22.116Z",
            "_fecha_modificacion": "2017-09-06T22:49:22.116Z"
          },
          {
            "id_item": 7,
            "fid_modulo": 3,
            "nro_item": 7,
            "nombre": "VENTANA ALUMINIO CNIDRIO DOBLE",
            "unidad_medida": "UN_METRO_CUAD",
            "cantidad": 206.6,
            "precio_unitario": 490.92,
            "costo_total": 101424.072,
            "tiempo_ejecucion": 12,
            "avance_total": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": null,
            "_fecha_creacion": "2017-09-06T22:51:46.682Z",
            "_fecha_modificacion": "2017-09-06T22:51:46.682Z"
          },
          {
            "id_item": 8,
            "fid_modulo": 3,
            "nro_item": 8,
            "nombre": "BARANDA METALICA FG",
            "unidad_medida": "UN_METRO",
            "cantidad": 165.71,
            "precio_unitario": 255.6,
            "costo_total": 42355.476,
            "tiempo_ejecucion": 12,
            "avance_total": null,
            "estado": "ACTIVO",
            "_usuario_creacion": 1,
            "_usuario_modificacion": 1,
            "_fecha_creacion": "2017-09-06T22:58:14.045Z",
            "_fecha_modificacion": "2017-09-06T22:59:10.044Z"
          }
        ]
      }
    }
  */
  app.api.get('/items', validate(paramValidation.get), app.controller.item.get);
  app.api.options('/items', sequelizeFormly.formly(app.src.db.models.item, app.src.db.models));

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Get items por modulo
   * @api {get} /api/v1/items/modulo/:id Obtiene los items pertenecientes a un modulo
   *
   * @apiParam id {number} Identificador del modulo
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Items recuperados correctamente.",
      "datos": Object
    }
   */
  app.api.get('/items/modulo/:id', validate(paramValidation.getId), app.controller.item.getModulo);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Get proyecto
   * @api {get} /api/v1/items/proyectos/:id Obtiene los items pertenecientes a un proyecto
   *
   * @apiParam id {number} Identificador del item
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Items recuperados correctamente.",
      "datos": Object
    }
   */
  app.api.get('/items/proyectos/:id', validate(paramValidation.getId), app.controller.item.getProyecto);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Get item
   * @api {get} /api/v1/items/:id Obtiene el item
   *
   * @apiParam (Parámetro) {string} id Identificador del item
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Item recuperado correctamente.",
      "datos": {
        "id_item": 8,
        "fid_modulo": 3,
        "nro_item": 8,
        "nombre": "BARANDA METALICA FG",
        "unidad_medida": "UN_METRO",
        "cantidad": 165.71,
        "precio_unitario": 255.6,
        "costo_total": 42355.476,
        "tiempo_ejecucion": 12,
        "avance_total": null,
        "estado": "ACTIVO",
        "_usuario_creacion": 1,
        "_usuario_modificacion": 1,
        "_fecha_creacion": "2017-09-06T22:58:14.045Z",
        "_fecha_modificacion": "2017-09-06T22:59:10.044Z"
      }
    }
   */
  app.api.get('/items/:id', validate(paramValidation.getId), app.controller.item.getId);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Post items
   * @api {post} /api/v1/items Crear item
   *
   * @apiDescription Post para item
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *{
      "fid_modulo": 1,
      "nombre": "INSTALACION DE FAENAS",
      "unidad_medida": "UN_GLOBAL",
      "cantidad": 1
    }
   */
  app.api.post('/items', validate(paramValidation.create), app.controller.item.guardar);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Post items desde archivo base64
   * @api {post} /api/v1/items/file Crear items desde archivo base64
   *
   * @apiDescription Post para items desde archivo
   *
   * @apiParamExample {json} Ejemplo para enviar:
   *{
      "id_proyecto": 1,
      "base64": "bW9kdWxvCW5vbWJyZQl1bmlkYWQJY2FudGlk..."
    }
   */
  app.api.post('/items/file', app.controller.item.importarCsv);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Post items desde archivo binario
   * @api {post} /api/v1/items/binary Crear items desde archivo binario
   *
   * @apiDescription Post para items desde archivo
   *
   * @apiParam (Peticion) {integer} id_proyecto Identificador del proyecto
   * @apiParam (Peticion) {file} file csv, ods o xlsx con los items
   */
  app.api.post('/items/binary', app.controller.item.postBinary);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Put items
   * @api {put} /api/v1/items/:id Modificar item
   *
   * @apiDescription Put para item
   *
   * @apiParam (Parámetro) {numeric} id Identificador del item a modificar
   *
   * @apiParamExample {json} Ejemplo para enviar:
    {
      "fid_modulo": 1,
      "nombre": "REPLANTEO Y TRAZADO",
      "unidad_medida": "UN_METRO_CUAD",
      "cantidad": 3125,
      "precio_unitario": 3.72,
      "tiempo_ejecucion": 40,
      "especificaciones": {
        "path": "especificaciones.txt",
        "base64": "cHJvYmFuZG8="
      }
    }
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "fid_modulo": 1,
      "nombre": "REPLANTEO Y TRAZADO",
      "unidad_medida": "UN_METRO_CUAD",
      "cantidad": 3125,
      "precio_unitario": 3.72,
      "tiempo_ejecucion": 40
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "No se encontró el item.",
      "datos": null
    }
   */
  app.api.put('/items/:id', validate(paramValidation.update), app.controller.item.actualizar);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Delete items
   * @api {delete} /api/v1/items/:id Borrar item
   *
   * @apiDescription Delete para item
   *
   * @apiParam (Parámetro) {numeric} id Identificador del item a eliminar
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
    {
      "finalizado": false,
      "mensaje": "No se encontró el item.",
      "datos": null
    }
   */
  app.api.delete('/items/:id', validate(paramValidation.getId), app.controller.item.eliminar);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items documentos
   * @apiName Get documentos
   * @api {get} /api/v1/items/:id/especificaciones Obtiene las especificaciones del item
   *
   * @apiParam id {number} Identificador del item
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Especificaciones recuperadas correctamente.",
      "datos": {
        "tipo": "text/plain",
        "base64": "cHJvYmFuZG8="
      }
    }
   */
  app.api.get('/items/:id/especificaciones', validate(paramValidation.getId), app.controller.item.getDocumento);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Get supervisiones
   * @api {get} /api/v1/items/:id/supervisiones Obtiene las supervisiones en que existe avance de un item
   *
   * @apiParam id {number} Identificador del item
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
    {
      "finalizado": true,
      "mensaje": "Supervisiones del item obtenidas correctamente.",
      "datos": [
        {
          "estado_supervision": "EN_ARCHIVO_FDI",
          "id_supervision": 1,
          "nombre": "Planilla 1",
          "nro_supervision": 1
        },
        {
          "estado_supervision": "REGISTRO_PLANILLA_FDI",
          "id_supervision": 2,
          "nombre": "En proceso"
        }
      ]
    }
   */
  app.api.get('/items/:id/supervisiones', validate(paramValidation.getId), app.controller.item.getSupervisiones);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Get items
   * @api {get} /api/v1/items/:id/resumen Obtener datos de un item más un resumen
   *
   * @apiDescription Get para item, permite obtener sus datos más el atributo completado respecto a su avance físico (en los cómputos métricos)
   *
   * @apiParam (Parámetro) {numeric} id Identificador del item a obtener
   *
   * @apiSuccessExample {json} Respuesta del Ejemplo:
   * HTTP/1.1 200 OK
   *{
      "finalizado": true,
      "mensaje": "Item recuperado correctamente.",
      "datos": {
        "id_item": 1,
        "fid_modulo": 1,
        "nro_item": 1,
        "nombre": "INSTALACION DE FAENAS",
        "unidad_medida": "UN_GLOBAL",
        "cantidad": 1,
        "precio_unitario": 26,
        "tiempo_ejecucion": null,
        "avance_total": null,
        "especificaciones": null,
        "estado": "ACTIVO",
        "modulo": {
          "nombre": "M01 - OBRAS COMPLEMENTARIAS",
          "proyecto": {
              "nombre": "CONSTRUCCIÓN DE ESCUELAS PARA EL MUNICIPIO DE COPACABANA"
          }
        },
        "completado": true
      }
    }
   * @apiErrorExample {json} Error-Response:
   * HTTP/1.1 412 Petición incorrecta
   */
  app.api.get('/items/:id/resumen', validate(paramValidation.getId), app.controller.item.getResumen);

  /**
   * @apiVersion 1.0.0
   * @apiGroup Items
   * @apiName Descargar CSV Modelo
   * @api {get} /api/v1/items/csv/manuales Descarga el archivo modelo para carga de items
   */
  app.api.get('/items/csv/manuales', app.controller.item.descargarCsvModelo);
};
