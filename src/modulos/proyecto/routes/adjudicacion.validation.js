import BaseJoi from '../../../lib/joi';
import Extension from 'joi-date-extensions';
const Joi = BaseJoi.extend(Extension);

module.exports = {
  listar: {
    params: {
      id_proyecto: Joi.number().required()
    }
  },
  crear: {
    params: {
      id_proyecto: Joi.number().required()
    },
    body: {
      monto: Joi.number().required(),
      referencia: Joi.string().max(50).required(),
      doc_adjudicacion: Joi.object({
        path: Joi.string().required(),
        base64: Joi.string().required()
      }),
      estado: Joi.string().max(100)
    }
  },
  actualizar: {
    params: {
      id_proyecto: Joi.number().required(),
      id_adjudicacion: Joi.number().required()
    },
    body: {
      monto: Joi.number().required(),
      referencia: Joi.string().max(50).required(),
      estado: Joi.string().max(100)
    }
  },
  procesar: {
    params: {
      id_proyecto: Joi.number().required(),
      id_adjudicacion: Joi.number().required()
    }
  },
  eliminar: {
    params: {
      id_proyecto: Joi.number().required(),
      id_adjudicacion: Joi.number().required()
    }
  },

};
