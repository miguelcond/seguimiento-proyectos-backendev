import LZString from 'lz-string';

module.exports = (app) => {
  const _app = app;
  _app.controller.fotoSupervision = {};
  const fotoSupervisionController = _app.controller.fotoSupervision;
  const sequelize = app.src.db.sequelize;
  const models = app.src.db.models;

  fotoSupervisionController.post = (req, res) => {
    const fotoSupervision = req.body;
    fotoSupervision._usuario_creacion = req.body.audit_usuario.id_usuario;
    const ID_ROL = req.body.audit_usuario.id_rol;

    // Descomprimir
    if (req.query.lz==='utf16') {
      req.body.base64 = LZString.decompressFromUTF16(req.body.base64);
    }

    req.body.base64 = req.body.base64.trim();
    
    // Validar texto en base64
    //if (req.body.base64.length%4!=0) {
    //  res.status(412).json({
    //    finalizado: false,
    //    mensaje: 'El formato enviado no esta en base64.',
    //    datos: null
    //  });
    //  return;
    //}
    sequelize.transaction().then((t) => {
      _app.dao.fotoSupervision.guardar(fotoSupervision, fotoSupervision._usuario_creacion, ID_ROL, t)
      .then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Item guardado correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    });
  };
  fotoSupervisionController.update = fotoSupervisionController.post;

  fotoSupervisionController.getId = (req, res) => {
    const idFotoSupervision = req.params.id;
    const idUsuario = req.body.audit_usuario.id_usuario;
    sequelize.transaction().then((t) => {
      _app.dao.fotoSupervision.obtenetFotoId(idFotoSupervision, idUsuario, t)
      .then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Foto recuperada correctamente.',
          datos: (req.query.lz==='utf16')?LZString.compressToUTF16(result.base64):result.base64,
          coordenadas: result.coordenadas
        });
      }).catch((error) => {
        t.rollback();
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    });
  };

  fotoSupervisionController.delete = (req, res) => {
    const idFoto = req.params.id;
    const ID_ROL = req.body.audit_usuario.id_rol;
    sequelize.transaction().then((t) => {
      _app.dao.fotoSupervision.borrar(idFoto, ID_ROL, req.query.supervision, t)
      .then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Item guardado correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    });
  };

  fotoSupervisionController.getFotosSupervisionItem = (req, res, next) => {
    const id_supervision = req.params.id;
    sequelize.transaction().then((t) => {
      _app.dao.fotoSupervision.getFotosSupervisionItem(id_supervision)
      .then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Fotos obtenidas correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    });
  };

  fotoSupervisionController.getFotosSupervision = (req, res, next) => {
    const id_supervision = req.params.id;
    sequelize.transaction().then((t) => {
      _app.dao.fotoSupervision.getFotosSupervision(id_supervision)
      .then((result) => {
        t.commit();
        res.json({
          finalizado: true,
          mensaje: 'Fotos obtenidas correctamente.',
          datos: result
        });
      }).catch((error) => {
        t.rollback();
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    });
  };

};
