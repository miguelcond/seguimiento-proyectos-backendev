import util from '../../../lib/util';
import errors from '../../../lib/errors';
import shortid from 'shortid';

module.exports = (app) => {
  const _app = app;
  _app.controller.supervision = {};
  const supervisionController = _app.controller.supervision;
  const supervisionModel = app.src.db.models.supervision;
  const sequelize = app.src.db.sequelize;

  supervisionController.get = (req, res) => {
    if (req.query.origen === 'bandeja') {
      const consulta = app.src.lib.util.formarConsulta(req.query, supervisionModel, app.src.db.models);
      consulta.where = { fid_proyecto: req.query.fid_proyecto };
      supervisionModel.findAndCountAll(consulta)
      .then((result) => {
        res.json({
          finalizado: true,
          mensaje: 'Supervisiones recuperadas correctamente.',
          datos: result
        });
      }).catch((error) => {
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    } else if (req.query.origen === 'aplicacion') {
      supervisionModel.supervisionIncluye(app.src.lib.util.formarConsulta(req.query, supervisionModel, app.src.db.models).condicionSupervision)
      .then((result) => {
        res.json({
          finalizado: true,
          mensaje: 'Supervisiones recuperadas correctamente.',
          datos: result
        });
      }).catch((error) => {
        res.status(412).json({
          finalizado: false,
          mensaje: error.message,
          datos: null
        });
      });
    } else {
      res.status(412).json({
        finalizado: false,
        mensaje: 'parametro mal definido',
        datos: null
      });
    }
  };

  const getId = async (req, res, next) => {
    const ID_ROL = req.body.audit_usuario.id_rol;
    const ID_USUARIO = req.body.audit_usuario.id_usuario;
    const ID_SUPERVISION = req.params.id;
    try {
      await app.dao.supervision.verificarPermisos(ID_SUPERVISION, ID_USUARIO, ID_ROL);
      const SUPERVISION = await app.dao.supervision.getDatos(ID_SUPERVISION, ID_ROL);
      // console.log('4 ***************************');
      res.json({
        finalizado: true,
        mensaje: 'Supervisión recuperada correctamente.',
        datos: SUPERVISION
      });
    } catch (e) {
      next(e);
    }
  };

  async function post (req, res, next) {
    const idUsuario = req.body.audit_usuario.id_usuario;
    const idRol = req.body.audit_usuario.id_rol;
    const supervision = req.body;
    const idAdjudicacion = req.params.id_adjudicacion;
    return sequelize.transaction(async(t) => {
      let adjudicacion = util.json(await _app.dao.adjudicacion.obtenerPorId(idAdjudicacion));
      if (adjudicacion.estado_adjudicacion !== 'ADJUDICADO_FDI') {
        throw new errors.ValidationError(`El componente ${adjudicacion.referencia} tiene una modificación pendiente de aprobación.`);
      }
      if (await app.dao.item.estanCompletados(supervision.fid_proyecto, undefined, idAdjudicacion)) {
        throw new errors.ValidationError(`El componente ${adjudicacion.referencia} tiene un avance completo de sus items, por lo cual no corresponde crear nuevas planillas de avance.`);
      }
      const SUPERVISION = await _app.dao.supervision.crearRecuperar(supervision, idAdjudicacion, idUsuario, t);
      const ESTADO_INICIAL = util.json(await _app.dao.estado.getEstadoInicial('SUPERV-FDI')).codigo;
      let logObs = '';
      if (idRol===8) {
        let proy = await app.dao.proyecto.obtenerDatosProyecto(supervision.fid_proyecto)
        logObs = proy.documentacion_exp.datos_empresa.razon_social || '';
      }
      await app.dao.transicion.registrarTransicion(SUPERVISION.id_supervision, 'supervision', ESTADO_INICIAL, undefined, idUsuario, idRol, logObs);
      return SUPERVISION;
    }).then(result => {
      res.json({
        finalizado: true,
        mensaje: 'Planilla de avance registrada correctamente.',
        datos: result
      });
    }).catch((error) => {
      next(error);
    });
  };

  async function put (req, res, next) {
    const idRol = req.body.audit_usuario.id_rol;
    const idUsuario = req.body.audit_usuario.id_usuario;
    let supervision = req.body;
    let SUPERVISION_ID = req.params.id;
    return sequelize.transaction(async(t) => {
      let _supervision = await app.dao.supervision.obtenerPorId(SUPERVISION_ID);
      _supervision = util.json(_supervision);

      if (supervision.path_comprobante) {
        let codigoDocumento = 'SupCP_' + `PS${SUPERVISION_ID}-` + shortid.generate();
        if (supervision.path_comprobante.base64) {
          let ID_PROYECTO = _supervision.fid_proyecto;
          supervision.path_comprobante = await app.dao.proyecto.guardarArchivo(supervision.path_comprobante, ID_PROYECTO, codigoDocumento);
        }
      }
      if (supervision.fecha_pago) {
          let fechaPagoArray = supervision.fecha_pago.split("-");
          let fecha_pago = fechaPagoArray[0] + "," + fechaPagoArray[1] + "," + fechaPagoArray[2];
          supervision.fecha_pago = new Date(fecha_pago).getTime();
      }
      const SUPERV_VALIDA = await app.dao.supervision.validar(supervision, idRol, idUsuario, t, SUPERVISION_ID);
      const CAMBIO_ESTADO = SUPERV_VALIDA.cambio;
      SUPERV_VALIDA.cambio = undefined;
      SUPERV_VALIDA._usuario_modificacion = idUsuario;
      const SUPERV_ACTUALIZADA = util.json(await SUPERV_VALIDA.save());
      await app.dao.estado.validarSiguiente(SUPERV_ACTUALIZADA, CAMBIO_ESTADO, idRol);
      await app.dao.estado.ejecutarMetodos(SUPERV_ACTUALIZADA, CAMBIO_ESTADO, idRol, app.dao.supervision, idUsuario);
      await app.dao.transicion.registrarTransicion(SUPERV_ACTUALIZADA.id_supervision, 'supervision', SUPERV_ACTUALIZADA.estado_supervision, SUPERV_VALIDA.observacion, idUsuario, idRol);
      await verificarEstadoFinal(SUPERV_ACTUALIZADA, idRol, idUsuario);
      return SUPERV_ACTUALIZADA;
    }).then(result => {
      res.json({
        finalizado: true,
        mensaje: 'Los datos fueron actualizados correctamente.',
        datos: result
      });
    }).catch((error) => {
      next(error);
    });
  }

  async function verificarEstadoFinal (supervision, idRol, idUsuario) {
    const ESTADO_FINAL = util.json(await app.dao.estado.getEstadoFinal('SUPERV-FDI')).codigo;
    if (supervision.estado_supervision === ESTADO_FINAL && await app.dao.item.estanCompletados(supervision.fid_proyecto)) {
      console.log("REVISAR CIERRE DE PROYECTO");
      //await app.dao.proyecto.cerrarProyecto(supervision.fid_proyecto, idRol, idUsuario, true);
    }
  }

  // supervisionController.sendCorreo = (req, res) => {
  //   var correo = require('./../../../lib/correo');
  //   console.log(correo);
  //   const datos = {
  //     urlLogoUpre: 'http://localhost:4000/imagen/logoUpre.jpg',
  //     urlSistemaFrontend: 'http://localhost:8080/#!/login',
  //     usuario: {
  //       nombre_completo: 'Ernesto Che Guevara',
  //     },
  //     observador: {
  //       nombre_completo: 'Eusebio Calamina Volqueta'
  //     },
  //     supervision: {
  //       nro_supervision: 4,
  //       observacion: 'Los computos del item nro 4, no se completaron como lo especifica en la planilla. Por favor ingresar los datos reales. atte Supeviosr Eusebio',
  //       fecha_observacion: '10/12/2017'
  //     },
  //     proyecto: {
  //       nombre: 'Proyecto en IXIAMAS DEL FONDO CATACORA XD XD XD XD XD'
  //     }
  //   };
  //   const rutaFile = `${_path}/src/templates/correo_observacion_avance.html`;
  //   let html = require('fs').readFileSync(rutaFile).toString();
  //   const email = require('handlebars').compile(html)(datos);

  //   correo.enviar({
  //     para: 'jloza@agetic.gob.bo',
  //     titulo: 'Observación de planilla',
  //     html: email
  //   });

  //   res.json({
  //     finalizado: true,
  //     mensaje: 'Exitaso en esta petición',
  //     datos: {}
  //   });
  // };
  supervisionController.post = post;
  supervisionController.put = put;
  supervisionController.getId = getId;
};
