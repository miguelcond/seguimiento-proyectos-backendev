import util from '../../../lib/util';
import errors from '../../../lib/errors';
import _ from 'lodash';

module.exports = (app) => {
  const _app = app;
  const models = app.src.db.models;
  const sequelize = app.src.db.sequelize;

  const obtener = async (idProyecto, version) => {
    let query = app.dao.proyecto.filtrosPlanilla(idProyecto);
    if (version) {
      query.where.version = version;
    }
    let proyecto = await models.proyecto_historico.findOne(query);
    proyecto.dataValues.empresa = proyecto.dataValues.contrato[0].empresa;
    delete proyecto.dataValues.contrato;
    proyecto.dataValues.dpa = app.dao.municipio.getDpa(util.json(proyecto.municipio));
    delete proyecto.dataValues.municipio;
    let fechasAjustadas = await app.dao.proyecto.calcularFechasRecepcion(proyecto);
    Object.assign(proyecto.dataValues, fechasAjustadas);
    return proyecto;
  };

  const guardar = async (proyecto) => {
    await models.proyecto_historico.create(proyecto);
  };

  const obtenerHistoricoModificaciones = async (idProyecto, tipoModificacion, version) => {
    let query = {
      where: {
        id_proyecto: idProyecto
      },
      order: [['version', 'ASC']],
      attributes: ['version', 'tipo_modificacion', 'estado_proyecto', 'monto_total', 'fecha_modificacion', 'plazo_ampliacion',
        [sequelize.literal(`COALESCE((monto_total - (SELECT ph.monto_total FROM proyecto_historico as ph WHERE ph.id_proyecto = ${idProyecto} and version = ("proyecto_historico".version - 1)))::Decimal(12, 2)::float, 0)`), 'variacion']],
      include: [
        {
          model: models.estado,
          as: 'estado_actual',
          attributes: ['nombre']
        }, {
          model: models.parametrica,
          as: 'tipo_modificacion_proyecto',
          attributes: ['nombre']
        }
      ]
    };
    if (version) {
      query.where.version = { $lt: version };
    }
    if (!_.isEmpty(tipoModificacion)) {
      // query.where.tipo_modificacion = tipoModificacion;
      query.where.tipo_modificacion = { $not: null };
    }
    return models.proyecto_historico.findAll(query);
  };

  const obtenerDatosModificadosVersion = async (idProyecto, version = 0) => {
    let consulta = {
      include: {
        model: models.parametrica,
        as: 'tipo_modificacion_proyecto',
        attributes: ['nombre', [sequelize.literal(`(select count(ph.*)
          from proyecto_historico ph
          where ph.tipo_modificacion="proyecto_historico".tipo_modificacion and ph.id_proyecto=${idProyecto} AND ph.version <= ${version})`), 'numero']]
      },
      where: {
        id_proyecto: idProyecto,
        version: version
      },
      attributes: ['monto_total', 'plazo_ampliacion', 'doc_respaldo_modificacion', 'fecha_modificacion']
    };
    return models.proyecto_historico.findOne(consulta);
  };

  const obtenerVariaciones = async (idProyecto, version) => {
    let consulta = {
      where: {
        id_proyecto: idProyecto,
        version: {
          $ne: 1
        }
      },
      attributes: ['tipo_modificacion', [sequelize.literal(`(SUM (monto_total - (SELECT ph.monto_total FROM proyecto_historico as ph WHERE ph.id_proyecto = ${idProyecto} and version = ("proyecto_historico".version - 1))))::Decimal(12, 2)::float`), 'variacion']],
      group: 'tipo_modificacion'
    };
    let variaciones = util.json(await models.proyecto_historico.findAll(consulta));
    for (var i = 0; i < variaciones.length; i++) {
      variaciones[i].variacion = util.redondear(variaciones[i].variacion);
    }
    const VERSION_INICIAL = await obtenerDatosModificadosVersion(idProyecto, 1);
    const VERSION_ANTERIOR = await obtenerDatosModificadosVersion(idProyecto, version);
    return {
      monto_total_inicial: VERSION_INICIAL.monto_total,
      monto_total_anterior: VERSION_ANTERIOR.monto_total,
      porcentajes: {
        5: util.valorPorcentual(VERSION_INICIAL.monto_total, 5),
        10: util.valorPorcentual(VERSION_INICIAL.monto_total, 10),
        15: util.valorPorcentual(VERSION_INICIAL.monto_total, 15)
      },
      variaciones
    };
  };

  const cancelarModificacion = async (proyectoActual) => {
    await app.dao.modulo_historico.cancelarModificacion(proyectoActual.id_proyecto, proyectoActual.version - 1);
    await sequelize.query(`UPDATE proyecto p
      SET monto_total = ph.monto_total, version = ph.version, plazo_ampliacion = ph.plazo_ampliacion, doc_respaldo_modificacion = ph.doc_respaldo_modificacion,
          tipo_modificacion = ph.tipo_modificacion, fecha_modificacion = ph.fecha_modificacion, estado_proyecto = ph.estado_proyecto
      FROM proyecto_historico ph
      WHERE p.id_proyecto = $1 AND ph.id_proyecto = $1 AND ph.version = $2`,
      { bind: [proyectoActual.id_proyecto, proyectoActual.version - 1], type: sequelize.QueryTypes.UPDATE }
    );
    await sequelize.query(`DELETE FROM proyecto_historico
      WHERE id_proyecto = $1 AND version = $2;`,
      { bind: [proyectoActual.id_proyecto, proyectoActual.version - 1], type: sequelize.QueryTypes.UPDATE }
    );
  };

  const obtenerDatosParaPlanilla = async (idProyecto, idRol, conEstadoActual = false, conHistorial = false, version) => {
    let consulta = app.dao.proyecto.filtrosPlanilla(idProyecto);
    consulta.where.version = version;
    consulta.attributes.push([sequelize.literal(`(CASE WHEN (SELECT version FROM proyecto WHERE id_proyecto = ${idProyecto}) = 1 THEN
      (SELECT monto_total FROM proyecto WHERE id_proyecto = ${idProyecto}) ELSE
      (SELECT monto_total FROM proyecto_historico WHERE id_proyecto = ${idProyecto} AND version = 1) END)`), 'monto_contractual']);
    if (conEstadoActual) {
      consulta.include.push({
        model: models.estado,
        as: 'estado_actual',
        attributes: ['codigo', 'nombre', 'tipo', 'acciones', 'atributos', 'requeridos', 'areas', ['fid_rol', 'roles']]
      });
    }
    if (conHistorial) {
      consulta.include.push({
        model: models.parametrica,
        as: 'tipo_modificacion_proyecto',
        attributes: ['nombre',
          [sequelize.literal(`(SELECT COUNT(ph.*) FROM proyecto_historico ph
            WHERE ph.tipo_modificacion="proyecto_historico".tipo_modificacion AND ph.id_proyecto=${idProyecto} AND ph.version <= ${version})`), 'numero']
        ]
      });
      consulta.include.push({
        model: models.proyecto_historico,
        as: 'historicos',
        where: {
          version: {
            $lt: version
          }
        },
        required: false,
        attributes: ['version', 'tipo_modificacion'],
        include: {
          model: models.parametrica,
          as: 'tipo_modificacion_proyecto',
          attributes: ['nombre', [sequelize.literal(`(SELECT COUNT(ph.*)
                  FROM proyecto_historico ph
                  WHERE ph.tipo_modificacion="historicos".tipo_modificacion AND ph.id_proyecto=${idProyecto} AND ph.version <= "historicos".version )`), 'numero']]
        }
      });
      consulta.order = [[{model: models.proyecto_historico, as: 'historicos'}, 'version', 'ASC']];
    }
    let proyecto = await models.proyecto_historico.findOne(consulta);
    proyecto.dataValues.proyecto_historico = proyecto.historicos;
    delete proyecto.dataValues.historicos;
    if (conEstadoActual) {
      await app.dao.estado.prepararDatosPermitidos(proyecto, idRol);
    }
    proyecto.dataValues.empresa = proyecto.dataValues.contrato[0].empresa;
    delete proyecto.dataValues.contrato;
    proyecto.dataValues.dpa = app.dao.municipio.getDpa(util.json(proyecto.municipio));
    delete proyecto.dataValues.municipio;
    let fechasAjustadas = await app.dao.proyecto.calcularFechasRecepcion(proyecto);
    Object.assign(proyecto.dataValues, fechasAjustadas);
    return proyecto;
  };

  _app.dao.proyecto_historico = {
    obtener,
    guardar,
    obtenerHistoricoModificaciones,
    obtenerDatosModificadosVersion,
    obtenerVariaciones,
    cancelarModificacion,
    obtenerDatosParaPlanilla
  };
};
