import BaseJoi from '../../../lib/joi';
import Extension from 'joi-date-extensions';
const Joi = BaseJoi.extend(Extension);

module.exports = {
  obtenerProyectos: {
    query: {
      codigo: Joi.string().min(1).max(32),
      page: Joi.number(),
      limit: Joi.number(),
    }
  },
  obtenerDesembolsos: {
    params: {
      codigo: Joi.string().min(1).max(32).required()
    },
    query: {
      page: Joi.number(),
      limit: Joi.number(),
    }
  },
  obtenerFotografia: {
    params: {
      codigo: Joi.string().min(1).max(32).required(),
      nombre: Joi.string().min(5).max(255).required()
    }
  }
};
