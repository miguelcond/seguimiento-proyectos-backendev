module.exports = {
  up(queryInterface) {
    let random = function(max) {
      return parseInt(Math.random()*max);
    };
    let abc = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','N'];
    let codigo = function() {
      return abc[random(15)]+abc[random(15)]+abc[random(15)]+abc[random(15)]+abc[random(15)]+abc[random(15)];
    }
    let proyectos = [];
    for(let i=0;i<1000;i++) {
      proyectos.push({
        version: 1,
        codigo: codigo(),
        tipo: 'TP_CIF',
        nombre: 'PROYECTO '+(i+1),
        nro_convenio: 'C000000'+(i+1),
        monto_fdi: 2000000.25,
        monto_contraparte: 250000.30,
        monto_total_convenio: 2250000.55,
        plazo_ejecucion_convenio: 60,
        entidad_beneficiaria: 'Municipio de Tolata',
        fecha_suscripcion_convenio: '01/01/2018',
        doc_convenio: '[C] Convenio.pdf',
        desembolso: 400000,
        porcentaje_desembolso: 20,
        fcod_municipio: '020101',
        estado_proyecto: 'ASIGNACION_TECNICO',
        estado: 'ACTIVO',
        _usuario_creacion: '2', //legal
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
      // Proyectos asignados a tecnico1
      if(i<900) {
        proyectos[i].estado_proyecto = 'ASIGNACION_PROYECTO_REGISTRO_FINANCIERO';
        proyectos[i].fid_usuario_asignado = 20;
        proyectos[i]._usuario_modificacion = 8;
        proyectos[i]._fecha_modificacion = new Date();
      }
      // Proyectos asignados a financiero
      if(i<800) {
        proyectos[i].sector = 'ST_DEPORTE';
        proyectos[i].fid_autoridad_beneficiaria = 30 + i; // 30 porque ya existen usuarios registrados
        proyectos[i].cargo_autoridad_beneficiaria = 'ALCALDE '+(i+1);
        proyectos[i].fax_autoridad_beneficiaria = '2800123';
        proyectos[i].doc_especificaciones_tecnicas = '[ET] Especificaciones técnicas.pdf';
        proyectos[i].estado_proyecto = 'REGISTRO_FINANCIERO';
        proyectos[i].fid_usuario_asignado = 5;
        proyectos[i]._usuario_modificacion = 20;
        proyectos[i]._fecha_modificacion = new Date();
      }
      // Proyectos para adjudicacion
      if(i<700) {
        proyectos[i].doc_cert_presupuestaria = '[CIP] Certificado de inscripción presupuestaria.pdf';
        proyectos[i].fecha_desembolso = new Date();
        proyectos[i].codigo_acceso = 'acceso'+(i+100);
        proyectos[i].estado_proyecto = 'ADJUDICACION';
        proyectos[i].fid_usuario_asignado = 20;
        proyectos[i]._usuario_modificacion = 5;
        proyectos[i]._fecha_modificacion = new Date();
      }
    }
    return queryInterface.bulkInsert('proyecto', proyectos, {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
