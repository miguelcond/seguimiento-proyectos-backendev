module.exports = {
  up(queryInterface) {
    let modulos = [];
    // 3 modulos asignados a proyectos
    for(let i=0;i<900;i++) {
      modulos.push({
        fid_proyecto: i+1, //proyecto
        nombre: 'MODULO 1',
        estado: 'ACTIVO',
        _usuario_creacion: 20, //encargado regional
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
      modulos.push({
        fid_proyecto: i+1, //proyecto
        nombre: 'MODULO 2',
        estado: 'ACTIVO',
        _usuario_creacion: 20, //encargado regional
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
      modulos.push({
        fid_proyecto: i+1, //proyecto
        nombre: 'MODULO 3',
        estado: 'ACTIVO',
        _usuario_creacion: 20, //encargado regional
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
    }

    return queryInterface.bulkInsert('modulo', modulos, {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
