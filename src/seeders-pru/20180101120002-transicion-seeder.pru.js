module.exports = {
  up(queryInterface) {
    let transiciones = [];
    // Proyectos iniciados por legal
    for(let i=0;i<1000;i++) {
      transiciones.push({
        objeto_transicion: i+1, //proyecto
        codigo_estado: 'REGISTRO_CONVENIO',
        fid_usuario: 2,
        fid_usuario_asignado: 2,
        estado: 'ACTIVO',
        _usuario_creacion: '2', //legal
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
      transiciones.push({
        objeto_transicion: i+1,
        codigo_estado: 'ASIGNACION_TECNICO',
        fid_usuario: 2,
        estado: 'ACTIVO',
        _usuario_creacion: '2', //legal
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
    }
    // Proyectos asignados a tecnico1
    // Tomar en cuenta que son pares
    for(let i=0;i<900;i++) {
      if(i<900){
        transiciones[i*2+1].fid_usuario_asignado = 8;
      }
      transiciones.push({
        objeto_transicion: i+1, //proyecto
        codigo_estado: 'ASIGNACION_PROYECTO_REGISTRO_FINANCIERO',
        fid_usuario: 8,
        estado: 'ACTIVO',
        _usuario_creacion: 8, //encargado regional
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
    }
    // Proyectos asignados a financiero
    // Tomar en cuenta que empieza desde 2 * 1000 proyectos
    for(let i=0;i<800;i++) {
      if(i<800){
        transiciones[20+i].fid_usuario_asignado = 20;
        transiciones[20+i]._fecha_modificacion = new Date();
      }
      transiciones.push({
        objeto_transicion: i+1, //proyecto
        codigo_estado: 'REGISTRO_FINANCIERO',
        fid_usuario: 20,
        estado: 'ACTIVO',
        _usuario_creacion: 20, //tecnico1
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
    }
    // Proyectos asignados para adjudicacion
    // Tomar en cuenta que empieza desde 2 * 1000 proyectos
    for(let i=0;i<700;i++) {
      if(i<700){
        transiciones[29+i].fid_usuario_asignado = 5;
        transiciones[29+i]._fecha_modificacion = new Date();
      }
      transiciones.push({
        objeto_transicion: i+1, //proyecto
        codigo_estado: 'ADJUDICACION',
        fid_usuario: 5,
        estado: 'ACTIVO',
        _usuario_creacion: 5, //tecnico1
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date(),
      });
    }

    return queryInterface.bulkInsert('transicion', transiciones, {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
