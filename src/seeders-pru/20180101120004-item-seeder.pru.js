module.exports = {
  up(queryInterface) {
    let items = [];
    // items de los modulos
    // tomar en cuanta que son 3 por proyectos 800
    let num;
    for(let i=0;i<900;i++) {
      num=0;
      for(let j=0;j<3;j++) {
        items.push({
          fid_modulo: i*3+1, //modulo 1 del proyecto 1
          nro_item: (num+1),
          nombre: 'ITEM '+(num+1),
          unidad_medida: 'UN_METRO',
          cantidad: 1,
          estado: 'ACTIVO',
          _usuario_creacion: 20, //tecnico
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date(),
        });
        num++;
      }
      for(let j=0;j<5;j++) {
        items.push({
          fid_modulo: i*3+2, //modulo 2 del proyecto 1
          nro_item: (num+1),
          nombre: 'ITEM '+(num+1),
          unidad_medida: 'UN_METRO',
          cantidad: 1,
          estado: 'ACTIVO',
          _usuario_creacion: 20, //tecnico
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date(),
        });
        num++;
      }
      for(let j=0;j<1;j++) {
        items.push({
          fid_modulo: i*3+3, //modulo 3 del proyecto 1
          nro_item: (num+1),
          nombre: 'ITEM '+(num+1),
          unidad_medida: 'UN_METRO',
          cantidad: 1,
          estado: 'ACTIVO',
          _usuario_creacion: 20, //tecnico
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date(),
        });
        num++;
      }
    }

    return queryInterface.bulkInsert('item', items, {});
  },

  down() {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
