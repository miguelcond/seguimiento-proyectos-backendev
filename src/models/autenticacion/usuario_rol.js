/**
 * Módulo que mapea los USUARIOS_ROLES.
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const usuarioRol = sequelize.define('usuario_rol', {
    id_usuario_rol: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'Id usuario rol'
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'usuario_rol',
    classMethods: {}
  });

  Object.assign(usuarioRol, {
    associate: (models) => {
      models.usuario_rol.belongsTo(models.usuario, {
        as: 'usuario',
        foreignKey: { name: 'fid_usuario', allowNull: false },
        targetKey: 'id_usuario'
      });
      models.usuario_rol.belongsTo(models.rol, {
        as: 'rol',
        foreignKey: { name: 'fid_rol', targetKey: 'id_rol', allowNull: false }
      });
    }
  });

  return usuarioRol;
};
