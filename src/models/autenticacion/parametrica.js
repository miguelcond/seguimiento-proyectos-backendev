/**
 * Módulo que mapea los PARAMETROS existentes, conjunto de valores parametricos
 * que son asignados a distintas tablas.
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const parametrica = sequelize.define('parametrica', {
    codigo: {
      type: DataType.STRING(50),
      primaryKey: true,
      xlabel: 'Codigo'
    },
    nombre: {
      type: DataType.STRING(50),
      field: 'nombre',
      xlabel: 'Nombre',
      allowNull: false
    },
    descripcion: {
      type: DataType.STRING(100),
      field: 'descripcion',
      xlabel: 'Descripción'
    },
    orden: {
      type: DataType.INTEGER,
      field: 'orden',
      xlabel: 'Orden',
      allowNull: false
    },
    _usuario_creacion: {
      type: DataType.STRING(50),
      field: '_usuario_creacion',
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.STRING(50),
      field: '_usuario_modificacion',
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'parametrica',
    classMethods: {}
  });

  return parametrica;
};
