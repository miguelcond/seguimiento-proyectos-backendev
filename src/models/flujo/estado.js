/**
 * Módulo para estado
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const estado = sequelize.define('estado', {
    codigo: {
      type: DataType.STRING(50),
      primaryKey: true,
      xlabel: 'Código'
    },
    codigo_proceso: {
      type: DataType.STRING(12),
      xlabel: 'Código Proceso',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING(150),
      xlabel: 'Nombre',
      allowNull: false
    },
    tipo: {
      type: DataType.STRING(50),
      xlabel: 'Tipo',
      allowNull: false
    },
    fid_rol: {
      type: DataType.ARRAY(DataType.INTEGER),
      xlabel: 'Rol',
      allowNull: false
    },
    fid_rol_asignado: {
      type: DataType.ARRAY(DataType.INTEGER),
      xlabel: 'Rol asignado',
      allowNull: false
    },
    acciones: {
      type: DataType.JSONB,
      xlabel: 'Acciones',
      allowNull: false
    },
    atributos: {
      type: DataType.JSONB,
      xlabel: 'Atributos',
      allowNull: false
    },
    requeridos: {
      type: DataType.JSONB,
      xlabel: 'Atributos requeridos',
      allowNull: false
    },
    automaticos: {
      type: DataType.JSONB,
      xlabel: 'Atributos automáticos',
      allowNull: false
    },
    areas: {
      type: DataType.JSONB,
      xlabel: 'Areas',
      allowNull: false
    },
    atributos_detalle: {
      type: DataType.JSONB,
      xlabel: 'Atributos',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'estado',
    classMethods: {}
  });

  Object.assign(estado, {
    associate: (models) => {
      // models.proyecto.belongsTo(models.parametrica, {
      //   as: 'tipo_proyecto',
      //   foreignKey: { name: 'tipo', xchoice: 'tipos', xlabel: 'Tipo' },
      //   targetKey: 'codigo'
      // });
      // models.proyecto.hasMany(models.equipo_tecnico, {
      //   as: 'equipo_tecnico',
      //   foreignKey: 'fid_proyecto'
      // });
    },
    estadoInicial: (tipo) => sequelize.models.estado.findOne({
      attributes: ['codigo', 'fid_rol'],
      where: {
        codigo_proceso: tipo,
        tipo: 'INICIO'
      }
    })
  });

  return estado;
};
