/**
 * Módulo par equipo_tecnico
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const equipoTecnico = sequelize.define('equipo_tecnico', {
    id_equipo_tecnico: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_proyecto: {
      type: DataType.INTEGER,
      xlabel: 'Proyecto',
      allowNull: false
    },
    fid_adjudicacion: {
      type: DataType.INTEGER,
      xlabel: 'Adjudicación',
      allowNull: true
    },
    tipo_equipo: {
      type: DataType.STRING(50),
      xlabel: 'Tipo equipo',
      allowNull: false
    },
    fid_usuario: {
      type: DataType.INTEGER,
      xlabel: 'Usuario',
      allowNull: false
    },
    fid_rol: {
      type: DataType.INTEGER,
      xlabel: 'Rol',
      allowNull: true
    },
    cargo: {
      type: DataType.STRING(50),
      xlabel: 'Cargo',
      allowNull: true
    },
    doc_designacion: {
      type: DataType.STRING,
      xlabel: 'Documento designación',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'equipo_tecnico',
    classMethods: {},
    indexes: [{
      unique: 'true',
      fields: ['fid_proyecto', 'fid_usuario', 'fid_adjudicacion', 'cargo']
    }, {
      unique: 'true',
      fields: ['fid_proyecto', 'fid_usuario', 'fid_adjudicacion', 'estado']
    }]
  });

  Object.assign(equipoTecnico, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.equipo_tecnico.belongsTo(models.proyecto, {
        as: 'proyecto',
        foreignKey: { name: 'fid_proyecto', xchoice: 'proyectos', xlabel: 'Proyecto' },
        targetKey: 'id_proyecto'
      });
      models.equipo_tecnico.belongsTo(models.adjudicacion, {
        as: 'adjudicacion',
        foreignKey: { name: 'fid_adjudicacion', xchoice: 'adjudicaciones', xlabel: 'Adjudicacion' },
        targetKey: 'id_adjudicacion',
        constraints: false
      });
      models.equipo_tecnico.belongsTo(models.parametrica, {
        as: 'equipo',
        foreignKey: { name: 'tipo_equipo', xchoice: 'equipos', xlabel: 'Tipo equipo' },
        targetKey: 'codigo'
      });
      models.equipo_tecnico.belongsTo(models.usuario, {
        as: 'usuario',
        foreignKey: { name: 'fid_usuario', xchoice: 'usuarios', xlabel: 'Usuario' },
        targetKey: 'id_usuario'
      });
      models.equipo_tecnico.belongsTo(models.parametrica, {
        as: 'cargo_usuario',
        foreignKey: { name: 'cargo', xchoice: 'cargos', xlabel: 'Cargo' },
        targetKey: 'codigo'
      });
    }
  });

  return equipoTecnico;
};
