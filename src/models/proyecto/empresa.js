/**
 * Módulo par empresa
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const empresa = sequelize.define('empresa', {
    id_empresa: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    matricula: {
      type: DataType.STRING(50),
      xlabel: 'Matrícula',
      allowNull: true,
      unique: true
    },
    nit: {
      type: DataType.STRING(20),
      xlabel: 'NIT',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING,
      xlabel: 'Nombre',
      allowNull: true
    },
    tipo: {
      type: DataType.STRING(50),
      xlabel: 'Tipo',
      allowNull: true
    },
    otro: {
      type: DataType.STRING(50),
      xlabel: 'Otro',
      allowNull: true
    },
    pais: {
      type: DataType.STRING(100),
      xlabel: 'País',
      allowNull: false
    },
    ciudad: {
      type: DataType.STRING(100),
      xlabel: 'Ciudad',
      allowNull: false
    },
    direccion: {
      type: DataType.STRING,
      xlabel: 'Codigo',
      allowNull: true
    },
    telefono: {
      type: DataType.STRING(50),
      xlabel: 'Teléfono',
      allowNull: true
    },
    estado: {
      type: DataType.STRING,
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'empresa',
    classMethods: {}
  });

  return empresa;
};
