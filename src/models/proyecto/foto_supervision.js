/**
 * Módulo par documento
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const fotoSupervision = sequelize.define('foto_supervision', {
    id_foto_supervision: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_item: {
      type: DataType.INTEGER,
      allowNull: true,
      xlabel: 'Item'
    },
    fid_supervision: {
      type: DataType.INTEGER,
      allowNull: false,
      xlabel: 'Supervision'
    },
    path_fotografia: {
      type: DataType.STRING(),
      allowNull: false,
      xlabel: 'Fotografía'
    },
    hash: {
      type: DataType.STRING(),
      allowNull: true,
      xlabel: 'Hash'
    },
    tipo: {
      type: DataType.STRING(50),
      allowNull: false,
      xlabel: 'Tipo'
    },
    coordenadas: {
      type: DataType.GEOMETRY('POINT'),
      allowNull: true,
      xlabel: 'Coordenadas'
    },
    uuid: {
      type: DataType.STRING(36),
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    fecha_creacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de creación movil',
      allowNull: true
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    },
    fecha_modificacion: {
      type: DataType.DATE,
      xlabel: 'Fecha de modificación movil',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'foto_supervision',
    classMethods: {}
  });

  Object.assign(fotoSupervision, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.foto_supervision.belongsTo(models.supervision, {
        as: 'supervision',
        foreignKey: { name: 'fid_supervision', xchoice: 'supervisiones', xlabel: 'Supervisión' },
        targetKey: 'id_supervision'
      });
      models.foto_supervision.belongsTo(models.parametrica, {
        as: 'tipo_foto_supervision',
        foreignKey: { name: 'tipo', xchoice: 'tipos', xlabel: 'Tipo' },
        targetKey: 'codigo'
      });
    }
  });

  return fotoSupervision;
};
