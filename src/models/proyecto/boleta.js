/**
 * Módulo para boleta
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const boleta = sequelize.define('boleta', {
    id_boleta: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_proyecto: {
      type: DataType.INTEGER,
      xlabel: 'Proyecto'
    },
    tipo_boleta: {
      type: DataType.STRING(50),
      xlabel: 'Tipo de boleta',
      allowNull: false
    },
    tipo_documento: {
      type: DataType.STRING(15),
      allowNull: false,
      validate: {
        isIn: {
          args: [['BOLETA', 'POLIZA']],
          msg: 'El campo tipo documento sólo permite valores: BOLETA o POLIZA.'
        }
      }
    },
    numero: {
      type: DataType.STRING(150),
      xlabel: 'Nro de la boleta',
      allowNull: true
    },
    monto: {
      type: DataType.FLOAT,
      xlabel: 'Monto',
      allowNull: true
    },
    fecha_inicio_validez: {
      type: DataType.DATE,
      xlabel: 'Fecha de inicio boleta',
      allowNull: true
    },
    fecha_fin_validez: {
      type: DataType.DATE,
      xlabel: 'Fecha de cumplimiento boleta',
      allowNull: true
    },
    doc_boleta: {
      type: DataType.STRING(100),
      xlabel: 'Documento boleta de garantía',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(10),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'boleta',
    classMethods: {}
  });

  Object.assign(boleta, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.boleta.belongsTo(models.parametrica, {
        // as: 'tipo_boleta',
        foreignKey: { name: 'tipo_boleta', xchoice: 'tipos', xlabel: 'Tipo' },
        targetKey: 'codigo'
      });
      models.boleta.belongsTo(models.proyecto, {
        as: 'proyecto',
        foreignKey: { name: 'fid_proyecto', xchoice: 'proyectos', xlabel: 'Proyecto' },
        targetKey: 'id_proyecto'
      });
    }
  });

  return boleta;
};
