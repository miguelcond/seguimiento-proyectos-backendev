/**
 * Módulo para contrato
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const contrato = sequelize.define('contrato', {
    id_contrato: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    fid_empresa: {
      type: DataType.INTEGER,
      xlabel: 'Empresa',
      allowNull: false
    },
    fid_proyecto: {
      type: DataType.INTEGER,
      xlabel: 'Proyecto',
      allowNull: true
    },
    nro_invitacion: {
      type: DataType.STRING(50),
      xlabel: 'Número de invitación',
      allowNull: true
    },
    nro_cuce: {
      type: DataType.STRING(50),
      xlabel: 'Número de cuce',
      allowNull: true
    },
    fecha_inscripcion: {
      type: DataType.DATE,
      xlabel: 'Fecha inscripción',
      allowNulle: true
    },
    nombre_representante_legal: {
      type: DataType.STRING(50),
      xlabel: 'Nombre representante legal',
      allowNull: true
    },
    ci_representante_legal: {
      type: DataType.STRING(10),
      xlabel: 'Documento representante legal',
      allowNull: true
    },
    numero_testimonio: {
      type: DataType.STRING(20),
      xlabel: 'Número de testimonio',
      allowNull: true
    },
    lugar_emision: {
      type: DataType.STRING(50),
      xlabel: 'Lugar de emisión',
      allowNull: true
    },
    fecha_expedicion: {
      type: DataType.DATE,
      xlabel: 'Fecha de expedición',
      allowNull: true
    },
    fax: {
      type: DataType.STRING(20),
      xlabel: 'Fax',
      allowNull: true
    },
    correo: {
      type: DataType.STRING(50),
      xlabel: 'Correo electrónico',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'contrato',
    classMethods: {}
  });

  Object.assign(contrato, {
    // Creando asociaciones para la entidad
    associate: (models) => {
      models.contrato.belongsTo(models.proyecto, {
        as: 'proyecto',
        foreignKey: { name: 'fid_proyecto', xchoice: 'proyectos', xlabel: 'Proyecto' },
        targetKey: 'id_proyecto'
      });
      models.contrato.belongsTo(models.empresa, {
        as: 'empresa',
        foreignKey: { name: 'fid_empresa', xchoice: 'empresas', xlabel: 'Empresa' },
        targetKey: 'id_empresa'
      });
    }
  });

  return contrato;
};
