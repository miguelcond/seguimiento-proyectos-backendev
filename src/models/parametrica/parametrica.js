/**
 * Módulo que mapea los PARAMETROS existentes, conjunto de valores parametricos
 * que son asignados a distintas tablas.
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const parametrica = sequelize.define('parametrica', {
    codigo: {
      type: DataType.STRING(50),
      primaryKey: true,
      xlabel: 'Codigo'
    },
    tipo: {
      type: DataType.STRING(25),
      xlabel: 'Tipo de parametrica',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING(50),
      xlabel: 'Nombre',
      allowNull: false
    },
    descripcion: {
      type: DataType.STRING(100),
      xlabel: 'Descripción',
      allowNull: true
    },
    orden: {
      type: DataType.INTEGER,
      xlabel: 'Orden',
      allowNull: false
    },
    referencia: {
      type: DataType.STRING(50),
      xlabel: 'Referencia',
      allowNull: true
    },
    tag: {
      type: DataType.STRING(50),
      xlabel: 'Tag',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'parametrica',
    classMethods: {}
  });

  Object.assign(parametrica, {
    associate: (models) => {
    },
    buscarPorTipo: (Parametrica, condicion) => Parametrica.findAndCountAll({
      attributes: ['codigo', 'nombre', 'descripcion'],
      where: condicion.condicionParametrica || {},
      offset: condicion.offset || 0,
      limit: condicion.limit || 100,
      order: condicion.order || []
    })
  });

  return parametrica;
};
