/**
 * Módulo departamento
 *
 * @module
 *
 **/
module.exports = (sequelize, DataType) => {
  const departamento = sequelize.define('departamento', {
    codigo: {
      type: DataType.STRING(2),
      primaryKey: true,
      xlabel: 'Código de departamento',
      allowNull: false,
      validate: {
        len: {
          args: [2],
          msg: 'El campo \'código de departamento\' permite caracteres de 2.'
        },
        is: {
          args: /^[0-9]+$/i,
          msg: 'El campo \'código de departamento\' permite sólo numeros.'
        }
      }
    },
    nombre: {
      type: DataType.STRING(64),
      xlabel: 'Nombre de Departamento',
      allowNull: false,
      validate: {
        len: {
          args: [5, 25],
          msg: 'El campo \'Nombre de Departamento\' permite un mínimo de 5 caracter y un máximo de 25 caracteres'
        },
        is: {
          args: /^[0-9|A-Z|-|-|.]+$/i,
          msg: 'El campo \'Nombre de Departamento\' permite sólo letras, números, guiones y puntos.'
        }
      }
    },
    abreviacion: {
      type: DataType.STRING(4),
      xlabel: 'Descripción del informe médico',
      allowNull: false,
      validate: {
        len: {
          args: [2, 4],
          msg: 'El campo \'código de departamento\' permite caracteres minimos de 2 y maximo de 4.'
        },
        is: {
          args: /^[A-Z]+$/i,
          msg: 'El campo \'código de departamento\' permite sólo letras.'
        }
      }
    },
    geom: {
      type: DataType.GEOMETRY('MULTIPOLYGON', 4326),
      xlabel: 'Área del municipio',
      allowNull: false
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      field: '_usuario_creacion',
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      field: '_usuario_modificacion',
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'departamento',
    classMethods: {}
  });

  Object.assign(departamento, {
    associate: (models) => {
      departamento.hasMany(models.provincia, {
        as: 'provincia',
        foreignKey: {name: 'codigo_departamento', allowNull: true}
      });
    },
    filtrarCalles: (codigo, calles) => sequelize.query(`SELECT json_agg(value) result
      FROM departamento, jsonb_array_elements(?::JsonB)
      WHERE ST_Within(ST_SetSRID(ST_GeomFromGeoJSON(value::Text), 4326), geom)
      AND codigo = ?`, {
        replacements: [JSON.stringify(calles), codigo],
        type: sequelize.QueryTypes.SELECT
      }
    ).then(result => {
      if (result.length === 1) {
        return result[0];
      } else {
        return null;
      }
    })
  });

  return departamento;
};
