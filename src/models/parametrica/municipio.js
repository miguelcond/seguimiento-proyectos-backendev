/**
 * Módulo municipio
 *
 * @module
 *
 **/
module.exports = (sequelize, DataType) => {
  const municipio = sequelize.define('municipio', {
    codigo: {
      type: DataType.STRING(6),
      primaryKey: true,
      xlabel: 'Código de municipio',
      allowNull: false,
      validate: {
        len: {
          args: [6],
          msg: 'El campo \'código de municipio\' permite caracteres de 6.'
        },
        is: {
          args: /^[0-9]+$/i,
          msg: 'El campo \'código de municipio\' permite sólo numeros.'
        }
      }
    },
    codigo_gam: {
      type: DataType.STRING(4),
      xlabel: 'Código GAM GAIOC',
      allowNull: true,
      validate: {
        len: {
          args: [4],
          msg: 'El campo \'código GAM GAIOC\' permite caracteres de 4.'
        },
        is: {
          args: /^[0-9]+$/i,
          msg: 'El campo \'código GAM GAIOC\' permite sólo numeros.'
        }
      }
    },
    nombre: {
      type: DataType.STRING(64),
      xlabel: 'Nombre de Municipio',
      allowNull: false,
      validate: {
        len: {
          args: [5, 100],
          msg: 'El campo \'Nombre de municipio\' permite un mínimo de 5 caracter y un máximo de 100 caracteres'
        },
        is: {
          args: /^[0-9|A-Z|-|-|.]+$/i,
          msg: 'El campo \'Nombre de municipio\' permite sólo letras, números, guiones y puntos.'
        }
      }
    },
    codigo_provincia: {
      type: DataType.STRING(4),
      xlabel: 'Código de provincia',
      allowNull: false,
      validate: {
        len: {
          args: [4],
          msg: 'El campo \'código de provincia\' permite caracteres de 4.'
        },
        is: {
          args: /^[0-9]+$/i,
          msg: 'El campo \'código de provincia\' permite sólo numeros.'
        }
      }
    },
    geom: {
      type: DataType.GEOMETRY('MULTIPOLYGON', 4326),
      xlabel: 'Área del municipio',
      allowNull: false
    },
    point: {
      type: DataType.GEOMETRY('POINT', 4326),
      xlabel: 'Capital',
      allowNull: false
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      field: '_usuario_creacion',
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      field: '_usuario_modificacion',
      xlabel: 'Usuario de modificación',
      allowNull: true
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'municipio',
    classMethods: {}
  });

  Object.assign(municipio, {
    associate: function (models) {
      municipio.belongsTo(models.provincia, {
        as: 'provincia',
        foreignKey: 'codigo_provincia',
        targetKey: 'codigo'
      });
      municipio.hasOne(models.techo_presupuestario, {
        as: 'techo_presupuestario',
        foreignKey: {name: 'codigo', allowNull: false}
      });
    },
    findByPoint: (long, lat) => municipio.findOne({
      attributes: ['codigo', 'nombre'],
      where: sequelize.fn('ST_Within', sequelize.fn('ST_GeomFromText', `POINT(${long} ${lat})`, 4326), sequelize.col('municipio.geom')),
      include: {
        attributes: ['codigo', 'nombre'],
        model: sequelize.models.provincia,
        as: 'provincia',
        required: true,
        include: [{
          attributes: ['codigo', 'nombre', [sequelize.cast(sequelize.fn('ST_AsGeoJSON', sequelize.fn('ST_Centroid', sequelize.col('geom'))), 'json'), 'point']],
          model: sequelize.models.municipio,
          as: 'municipio',
          required: true
        }, {
          attributes: ['codigo', 'nombre'],
          model: sequelize.models.departamento,
          as: 'departamento',
          required: true,
          include: {
            attributes: ['codigo', 'nombre'],
            model: sequelize.models.provincia,
            as: 'provincia',
            required: true
          }
        }]
      }
    }),
    consultaInclude: (cod) => municipio.findOne({
      subquery: false,
      attributes: ['codigo', 'codigo_gam', 'nombre'],
      include: {
        attributes: ['codigo', 'nombre'],
        model: sequelize.models.provincia,
        as: 'provincia',
        include: {
          attributes: ['codigo', 'nombre'],
          model: sequelize.models.departamento,
          as: 'departamento'
        }
      },
      where: {
        codigo: cod
      }
    })
  });

  return municipio;
};
