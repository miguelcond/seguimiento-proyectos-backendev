/**
 * Módulo para instituciones
 *
 * @module
 *
 **/

module.exports = (sequelize, DataType) => {
  const institucion = sequelize.define('institucion', {
    id_institucion: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      xlabel: 'ID'
    },
    sigla: {
      type: DataType.STRING(64),
      xlabel: 'Sigla',
      allowNull: false
    },
    nombre: {
      type: DataType.STRING(256),
      xlabel: 'Nombre',
      allowNull: false
    },
    direccion: {
      type: DataType.STRING(256),
      xlabel: 'Dirección',
      allowNull: true
    },
    coordenadas: {
      type: DataType.GEOMETRY('POINT'),
      xlabel: 'Coordenadas',
      allowNull: true
    },
    telefono: {
      type: DataType.STRING(50),
      xlabel: 'Telefono',
      allowNull: true
    },
    estado: {
      type: DataType.STRING(),
      defaultValue: 'ACTIVO',
      allowNull: false,
      validate: {
        isIn: {
          args: [['ACTIVO', 'INACTIVO', 'ELIMINADO']],
          msg: 'El campo estado sólo permite valores: ACTIVO, INACTIVO, ELIMINADO.'
        }
      }
    },
    _usuario_creacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de creación',
      allowNull: false
    },
    _usuario_modificacion: {
      type: DataType.INTEGER,
      xlabel: 'Usuario de modificación'
    }
  }, {
    createdAt: '_fecha_creacion',
    updatedAt: '_fecha_modificacion',
    freezeTableName: true,
    tableName: 'institucion',
    classMethods: {}
  });

  Object.assign(institucion, {
    associate: (models) => {
      models.institucion.hasMany(models.usuario, {
        as: 'usuarios',
        foreignKey: 'fid_institucion'
      });
    }
  });

  return institucion;
};
