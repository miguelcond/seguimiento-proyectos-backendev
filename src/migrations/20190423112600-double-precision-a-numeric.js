'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    const sql = (
      `
      DO $$
      DECLARE t RECORD;
      BEGIN
          FOR t IN (SELECT column_name, table_name
                  FROM information_schema.columns
                  WHERE table_schema = 'public'
                      AND data_type = 'double precision'
                      AND is_updatable = 'YES')
          LOOP
              EXECUTE 'alter table ' || t.table_name || ' alter column ' || t.column_name || ' type numeric using ' || t.column_name || '::numeric';
          END LOOP;
      END $$;
      `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `
      DO $$
      DECLARE t RECORD;
      BEGIN
          FOR t IN SELECT column_name, table_name
                  FROM information_schema.columns
                  WHERE table_schema = 'public'
                      AND is_updatable = 'YES'
                      AND data_type = 'numeric'
                      AND numeric_precision = 16
                      AND numeric_scale = 6
          LOOP
              EXECUTE 'alter table ' || t.table_name || ' alter column ' || t.column_name || ' type float';
          END LOOP;
      END $$;
      `
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
