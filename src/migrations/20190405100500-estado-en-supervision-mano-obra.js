'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
        nombre: 'Planilla en supervisión',
        atributos: '{"16":["informe","estado_supervision","observacion","fotos_supervision", "mano_obra_calificada", "mano_obra_no_calificada"]}',
        areas: '["INFORME", "FOTOGRAFIAS", "DEUDA_ANTICIPO", "MANO_OBRA"]'
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_SUPERVISION_FDI'
    },{
      returning: true, raw: true
    }).then((rows) => {
      console.timeEnd('estado');
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');

    console.time('estado');
    return migration.bulkUpdate('estado', {
        nombre: 'Planilla enviada al/a la supervisor/a',
        atributos: '{"16":["informe","estado_supervision","observacion","fotos_supervision"]}',
        areas: '["INFORME", "FOTOGRAFIAS", "DEUDA_ANTICIPO"]'
    },{
      codigo_proceso: 'SUPERV-FDI',
      codigo: 'EN_SUPERVISION_FDI'
    },{
      returning: true, raw: true
    }).then((rows) => {
      console.timeEnd('estado');
    }).finally(done);
  },
};
