'use strict';

module.exports = {
  up(migration, DataTypes, done) {
    // ADMIN
    let estadosArray = [];
    let objs = [
      {
        codigo_proceso: 'ADJ-FDI',
        codigo: 'ADJUDICADO_FDI',
        nombre: 'Estado inicial de adjudicacion',
        tipo: 'FIN',
        fid_rol: '{}',
        fid_rol_asignado: '{}',
        acciones: '[{"tipo":"ventana","ruta":"/temporal"},{"tipo":"boton","label":"Modificar","class":"btn-success","icon":"fa-send","mensaje":"Ejecutar modificación","estado":"MODIFICADO_FDI"}]',
        atributos: '{"16":["estado_proyecto"]}',
        requeridos: '[]',
        automaticos: '{}',
        areas: '{"16":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","ORDEN_PROCEDER","MODULOS_ITEMS"], "17":["DATOS_GENERALES", "ORDEN_PROCEDER"],"18":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","EQUIPO_TECNICO","CONVENIO","DESEMBOLSO"]}',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      },
      // {
      //   codigo_proceso: 'ADJ-FDI',
      //   codigo: 'MODIFICADO_FDI',
      //   nombre: 'En modificación',
      //   tipo: 'INICIO',
      //   fid_rol: '{16}',
      //   fid_rol_asignado: '{16}',
      //   acciones: '[{"tipo":"ventana","ruta":"/temporal"},{"tipo":"boton","label":"Cancelar","class":"btn-secondary","icon":"fa-close","mensaje":"Cancelar la modificación","estado":"ADJUDICADO_FDI", "ejecutar":["$cancelarModificacion"]},{"tipo":"boton","label":"Modificar","class":"btn-success","icon":"fa-send","mensaje":"Ejecutar modificación","estado":"REVISION_MODIFICADO_FDI", "ejecutar": ["$validarModificacion"]}]',
      //   atributos: '{"16":[{"condicion":"tipo_modificacion TM_ORDEN_CAMBIO ==", "valor": "plazo_ampliacion"}, {"condicion":"tipo_modificacion TM_CONTRATO_MODIFICATORIO ==", "valor": "plazo_ampliacion"},"doc_respaldo_modificacion","fecha_modificacion","estado_proyecto"]}',
      //   requeridos: '[]',
      //   automaticos: '{}',
      //   atributos_detalle: '{"modulo":{"acciones":["post"],"post":["fid_proyecto","nombre"]},"item":{"acciones":["post","put"],"post":["fid_modulo","nombre","unidad_medida","cantidad","precio_unitario","especificaciones","analisis_precios"],"put":["fid_modulo","nombre","unidad_medida","cantidad","precio_unitario","especificaciones","analisis_precios"]}}',
      //   areas: '{"14":["DATOS_GENERALES","CONVENIO"],"12":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS", {"condicion":"tipo TP_CIFE ==","valor":"DOCUMENTO_BASE_CONTRATACION"}, "FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"],"13":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS", {"condicion":"tipo TP_CIFE ==","valor":"DOCUMENTO_BASE_CONTRATACION"}, "FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"],"15":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","GARANTIAS"],"8":["PROYECTO_ADJUDICADO","FORMULARIOS", "GARANTIAS"],"16":["DATOS_GENERALES", "ORDEN_PROCEDER"],"17":["DATOS_GENERALES", "ORDEN_PROCEDER"],"18":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS", {"condicion":"tipo TP_CIFE ==","valor":"DOCUMENTO_BASE_CONTRATACION"}, "FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"]}',
      //   _usuario_creacion: 1,
      //   _fecha_creacion: new Date(),
      //   _fecha_modificacion: new Date()
      // },
      // {
      //   codigo_proceso: 'ADJ-FDI',
      //   codigo: 'REVISION_MODIFICADO_FDI',
      //   nombre: 'Revisión de la modificación',
      //   tipo: 'FLUJO',
      //   fid_rol: '{17}',
      //   fid_rol_asignado: '{17}',
      //   acciones: '[{"tipo":"ventana","ruta":"/temporal"},{"tipo":"boton","label":"Observar","observacion":true,"estado":"MODIFICADO_FDI","class":"btn-warning","icon":"fa-rotate-left"},{"tipo":"boton","label":"Aprobar modificación","class":"btn-success","icon":"fa-check","estado":"ADJUDICADO_FDI", "ejecutar": ["$cerrarItems"]}]',
      //   atributos: '{"17":["monto_total","monto_contraparte","observacion","estado_proyecto"]}',
      //   requeridos: '[]',
      //   automaticos: '{}',
      //   areas: '{"14":["DATOS_GENERALES","CONVENIO"],"12":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS", {"condicion":"tipo TP_CIFE ==","valor":"DOCUMENTO_BASE_CONTRATACION"}, "FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"],"13":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS", {"condicion":"tipo TP_CIFE ==","valor":"DOCUMENTO_BASE_CONTRATACION"}, "FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"],"15":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","GARANTIAS"],"8":["PROYECTO_ADJUDICADO","FORMULARIOS", "GARANTIAS"],"16":["DATOS_GENERALES", "ORDEN_PROCEDER"],"17":["DATOS_GENERALES", "ORDEN_PROCEDER"],"18":["DATOS_GENERALES","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","MODULOS_ITEMS", {"condicion":"tipo TP_CIFE ==","valor":"DOCUMENTO_BASE_CONTRATACION"}, "FORMULARIOS","GARANTIAS","EQUIPO_TECNICO","ORDEN_PROCEDER"]}',
      //   _usuario_creacion: 1,
      //   _fecha_creacion: new Date(),
      //   _fecha_modificacion: new Date()
      // }
    ];
    estadosArray = estadosArray.concat(objs);
    // queryInterface.sequelize.query("DELETE FROM estado WHERE codigo like 'ADJUDICADO_FDI'");
    // queryInterface.sequelize.query("DELETE FROM estado WHERE codigo like 'MODIFICADO_FDI'");
    // queryInterface.sequelize.query("DELETE FROM estado WHERE codigo like 'REVISION_MODIFICADO_FDI'");
    return migration.bulkInsert('estado', estadosArray, {})
    .finally(done);
  },

  down(migration, DataTypes, done) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return migration.sequelize.query('');
  }
};
