module.exports = {
  up: (migration, DataTypes, done) => {
    let rol = migration.sequelize.import('../models/autenticacion/rol');
    let menu = migration.sequelize.import('../models/autenticacion/menu');
    let rol_menu = migration.sequelize.import('../models/autenticacion/rol_menu');
    let data = {};
    return rol.findOne({
      where: {
        id_rol: 19,
        nombre: 'MAE_FDI',
      },
      returning: true, raw: true
    }).then(resp => {
      if(!resp) {
        return migration.bulkInsert('rol',
          [{ id_rol:19, nombre: 'MAE_FDI', descripcion: 'MAE Institucional', peso: 0, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {returning: true, raw: true});
      }
      return resp;
    }).then(resp=>{
      resp = resp[0] || resp;
      data.id_rol = resp.id_rol;
      return menu.findOne({
        where: {
          nombre: 'PROYECTOS',
          ruta: 'lista-proyectos'
        },
        returning: true, raw: true
      });
    }).then(resp=>{
      if(!resp) {
        return migration.bulkInsert('menu',
          [{ nombre: 'PROYECTOS', descripcion: 'Lista de proyectos', orden: 5, ruta: 'lista-proyectos', icono: 'list', method_get: true, fid_menu_padre: 6, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {returning: true, raw: true});
      }
      return resp;
    }).then(resp=>{
      resp = resp[0] || resp;
      data.id_menu = resp.id_menu;
      return menu.findOne({
        where: {
          nombre: 'PROYECTOS',
          ruta: 'lista-proyectos-municipio'
        },
        returning: true, raw: true
      });
    }).then(resp=>{
      if(!resp) {
        return migration.bulkInsert('menu',
          [{ nombre: 'LISTA POR MUNICIPIO', descripcion: 'Lista de proyectos por municipio', orden: 6, ruta: 'lista-proyectos-municipio', icono: 'list', method_get: true, fid_menu_padre: 6, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {returning: true, raw: true});
      }
      return resp;
    }).then(resp=>{
      resp = resp[0] || resp;
      data.id_menu2 = resp.id_menu;
      migration.bulkInsert('rol_menu',
        [{ method_get: true, fid_menu: data.id_menu, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
         { method_get: true, fid_menu: data.id_menu2, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
      , {returning: true, raw: true}).catch(e=>{console.error(e.message);});
      migration.bulkInsert('rol_ruta',
        [{ method_get: true, fid_ruta: 8, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_get: true, fid_ruta: 13, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_put: true, fid_ruta: 22, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_get: true, fid_ruta: 26, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        // departamentos, provincias, municipios
        { method_get: true, fid_ruta: 14, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_get: true, fid_ruta: 15, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_get: true, fid_ruta: 16, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
      , {returning: true, raw: true}).catch(e=>{console.error(e.message);});
      migration.bulkUpdate('estado', {
        areas: '{"11":["DATOS_GENERALES", "DATOS_FINANCIAMIENTO"], "12":["DATOS_GENERALES","DATOS_FINANCIAMIENTO","ASIGNACION_RESPONSABLE"], "13":["DATOS_GENERALES","DATOS_FINANCIAMIENTO"], "19":["DATOS_GENERALES","DATOS_FINANCIAMIENTO"]}'
      },{
        codigo: 'ASIGNACION_TECNICO_FDI'
      },{returning: true, raw: true}).catch(e=>{console.error(e.message);});
      migration.bulkUpdate('estado', {
        areas: '{"11":["DATOS_GENERALES","DATOS_FINANCIAMIENTO"], "12":["DATOS_GENERALES","ASIGNACION_RESPONSABLE","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA"], "13":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA"], "19":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA"]}'
      },{
        codigo: 'REGISTRO_PROYECTO_FDI'
      },{returning: true, raw: true}).catch(e=>{console.error(e.message);});
      migration.bulkUpdate('estado', {
        areas: '{"11":["DATOS_GENERALES","DATOS_FINANCIAMIENTO"], "12":["DATOS_GENERALES","ASIGNACION_RESPONSABLE","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA"], "13":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA"], "14":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO"], "18":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA"], "19":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO"]}'
      },{
        codigo: 'REGISTRO_CONVENIO_FDI'
      },{returning: true, raw: true}).catch(e=>{console.error(e.message);});
      migration.bulkUpdate('estado', {
        areas: '{"11":["DATOS_GENERALES","DATOS_FINANCIAMIENTO"], "12":["DATOS_GENERALES","ASIGNACION_RESPONSABLE","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO","DESEMBOLSO","EQUIPO_TECNICO"], "13":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO","DESEMBOLSO","EQUIPO_TECNICO"], "14":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO"], "15":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO","DESEMBOLSO"], "18":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO","DESEMBOLSO","EQUIPO_TECNICO"], "19":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO","DESEMBOLSO"]}'
      },{
        codigo: 'ASIGNACION_EQUIPO_TECNICO_FDI'
      },{returning: true, raw: true}).catch(e=>{console.error(e.message);});
      return migration.bulkUpdate('estado', {
        areas: '{"11":["DATOS_GENERALES","DATOS_FINANCIAMIENTO"], "12":["DATOS_GENERALES","ASIGNACION_RESPONSABLE","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO"], "13":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO"], "14":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO"], "15":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO","DESEMBOLSO"], "18":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","AUTORIDAD_BENEFICIARIA","CONVENIO"], "19":["DATOS_GENERALES","DATOS_GENERALES_EXTRA","DATOS_FINANCIAMIENTO","CONVENIO","DESEMBOLSO","AUTORIDAD_BENEFICIARIA","INFORME"]}'
      },{
        codigo: 'REGISTRO_DESEMBOLSO_FDI'
      },{
        returning: true, raw: true
      });
    }).then(resp=>{
      console.log('OK');
    }).finally(done);
  },

  down: (migration, DataTypes) => {
    return migration.sequelize.query('');
  }
};
