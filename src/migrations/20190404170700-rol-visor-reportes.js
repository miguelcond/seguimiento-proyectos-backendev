module.exports = {
  up: (migration, DataTypes, done) => {
    let rol = migration.sequelize.import('../models/autenticacion/rol');
    let menu = migration.sequelize.import('../models/autenticacion/menu');
    let rol_menu = migration.sequelize.import('../models/autenticacion/rol_menu');
    let data = {};
    return rol.findOne({
      where: {
        id_rol: 20,
        nombre: 'VISOR_REPORTES',
      },
      returning: true, raw: true
    }).then(resp => {
      if(!resp) {
        return migration.bulkInsert('rol',
          [{ id_rol:20, nombre: 'VISOR_REPORTES', descripcion: 'Visor de reportes de proyectos', peso: 0, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {returning: true, raw: true});
      }
      return resp;
    }).then(resp => {
      resp = resp[0] || resp;
      data.id_rol = resp.id_rol;
      return menu.findOne({
        where: {
          nombre: 'CSV',
          ruta: 'reporte-general'
        },
        returning: true, raw: true
      });
    }).then(resp => {
      resp = resp[0] || resp;
      data.id_menu = resp.id_menu;
      migration.bulkInsert('rol_menu',
        [{ method_get: true, fid_menu: data.id_menu, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
      , {returning: true, raw: true}).catch(e => { console.error(e.message); });
      return migration.bulkInsert('rol_ruta',
        [{ method_get: true, fid_ruta: 13, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        // departamentos, provincias, municipios
        { method_get: true, fid_ruta: 14, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_get: true, fid_ruta: 15, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_get: true, fid_ruta: 16, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_put: true, fid_ruta: 22, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_get: true, fid_ruta: 25, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        { method_get: true, fid_ruta: 26, fid_rol: data.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]

      , {returning: true, raw: true}).catch(e => { console.error(e.message); });
    }).then(resp => {
      console.log('OK');
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('');
  }
};
