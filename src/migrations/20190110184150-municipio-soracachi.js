module.exports = {
  up: (migration, DataTypes, done) => {
    return migration.bulkUpdate('municipio', {
        nombre: 'Soracachi'
    },{
      codigo: '040104'
    },{
      returning: true, raw: true
    }).then((rows)=>{
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.bulkUpdate('municipio', {
      nombre: 'Paria'
    },{
      codigo: '040104'
    },{
      returning: true, raw: true
    }).then((rows)=>{
    }).finally(done);
  }
};
