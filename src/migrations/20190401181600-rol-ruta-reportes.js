'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    let rol_ruta = migration.sequelize.import('../models/autenticacion/rol_ruta');
    return rol_ruta.findOne({
      where: {
        fid_rol: 19,
        fid_ruta: 25
      }
    }).then((result) => {
      console.log('result', result);
      if (!result) {
        return migration.bulkInsert('rol_ruta',
          [{ fid_ruta: 25, fid_rol: 19, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {});
      } else {
        return migration.bulkUpdate('rol_ruta', { fid_ruta: 25, fid_rol: 19, method_get: true, method_post: false, method_put: false, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        {
          fid_rol: 19,
          fid_ruta: 25
        }, {
          returning: true, raw: true
        });
      }
    }).catch((e) => {
      console.error('E', e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `DELETE FROM rol_ruta WHERE fid_rol = 19 AND fid_ruta = 25;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
