'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        atributos_detalle: '{"item": {"put": ["nombre", "unidad_medida", "cantidad", "especificaciones", "precio_unitario", "peso_ponderado"], "post": ["fid_modulo", "nombre", "unidad_medida", "cantidad", "especificaciones", "precio_unitario", "peso_ponderado"], "acciones": ["post", "put"]}, "modulo": {"put": ["nombre"], "post": ["fid_proyecto", "fid_adjudicacion", "nombre"], "acciones": ["post", "put"]}}',
    }, {
      codigo_proceso: 'PROYECTO-FDI',
      codigo: 'EN_EJECUCION_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {

    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        atributos_detalle: '{"item": {"put": ["nombre", "unidad_medida", "cantidad", "especificaciones", "precio_unitario"], "post": ["fid_modulo", "nombre", "unidad_medida", "cantidad", "especificaciones", "precio_unitario"], "acciones": ["post", "put"]}, "modulo": {"put": ["nombre"], "post": ["fid_proyecto", "fid_adjudicacion", "nombre"], "acciones": ["post", "put"]}}',
    }, {
      codigo_proceso: 'PROYECTO-FDI',
      codigo: 'EN_EJECUCION_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {

    }).finally(done);
  },
};
