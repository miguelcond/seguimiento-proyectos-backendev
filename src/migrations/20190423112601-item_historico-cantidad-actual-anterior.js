'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    console.time('item');
    return migration.addColumn('item_historico', 'cantidad_anterior_fis', {
      type: DataTypes.FLOAT,
      xlabel: 'Cantidad física acumulada en computos previos',
      allowNull: false,
      defaultValue: 0
    }, {
      returning: true, raw: true
    }).then((resp) => {
      return migration.addColumn('item_historico', 'cantidad_actual_fis', {
        type: DataTypes.FLOAT,
        xlabel: 'Cantidad física actual de computos metricos',
        allowNull: false,
        defaultValue: 0
      }, {
        returning: true, raw: true
      });
    }).then((resp) => {
      const sql = (
        `
        DO $$
        DECLARE t RECORD;
        BEGIN
            FOR t IN (SELECT column_name, table_name
                    FROM information_schema.columns
                    WHERE table_schema = 'public'
                        AND data_type = 'double precision'
                        AND is_updatable = 'YES')
            LOOP
                EXECUTE 'alter table ' || t.table_name || ' alter column ' || t.column_name || ' type numeric using ' || t.column_name || '::numeric';
            END LOOP;
        END $$;
        `
      );
      return migration.sequelize.query(sql);
      console.timeEnd('item');
    }).catch((e) => {
      console.log(e.message);
    }).finally(()=>{
      console.timeEnd('item');
    });
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE item DROP COLUMN IF EXISTS cantidad_anterior_fis;
      ALTER TABLE item DROP COLUMN IF EXISTS cantidad_actual_fis;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
