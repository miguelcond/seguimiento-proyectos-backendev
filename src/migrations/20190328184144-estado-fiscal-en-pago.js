module.exports = {
  up(queryInterface) {
    return queryInterface.bulkInsert('estado', [
      {
        codigo_proceso: 'SUPERV-FDI',
        codigo: 'PENDIENTE_FDI',
        nombre: 'Planilla enviada al/a la fiscal',
        tipo: 'FLUJO',
        fid_rol: '{17}',
        fid_rol_asignado: '{17}',
        acciones: '[{"tipo":"ventana","ruta":"/temporal"},{"tipo":"boton","label":"Observar","observacion":true,"estado":"EN_SUPERVISION_FDI","class":"btn-warning","icon":"fa-rotate-left"},{"tipo":"boton","label":"Aprobar planilla","class":"btn-success","icon":"fa-check","estado":"EN_FISCAL_FDI","ejecutar": ["$registrarCoordenadas","$cerrarItemsFinalizados","$generarReportePlanilla"]}]',
        atributos: '{"17":["estado_supervision","observacion"]}',
        requeridos: '[]',
        // automaticos: '{}',
        // automaticos: '{"desembolso":"$calculoDesembolso", "descuento_anticipo":"$calculoDescuentoPorAnticipo", "version_proyecto":"$versionProyecto"}',
        automaticos: '{"desembolso":"$calculoDesembolso", "descuento_anticipo":"$calculoDescuentoPorAnticipo", "version_proyecto":"$versionProyecto", "version_adjudicacion":"$versionAdjudicacion"}',
        areas: '["INFORME", "FOTOGRAFIAS", "DEUDA_ANTICIPO"]',
        _usuario_creacion: 1,
        _fecha_creacion: new Date(),
        _fecha_modificacion: new Date()
      }
    ], {});
  },

  down: (migration, DataTypes) => {
    return migration.sequelize.query('');
  }
};
