'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
        `ALTER TABLE adjudicacion ADD COLUMN estado_adjudicacion VARCHAR(50) NOT NULL DEFAULT 'ADJUDICADO_FDI';`
    );

    return migration.sequelize.query(sql)
      .catch((e)=>{
        console.log('E', e.message);
      })
      .finally(done);
  },

  down: (migration, DataTypes, done) => {

    const sql = (
      `ALTER TABLE adjudicacion DROP COLUMN IF EXISTS estado_adjudicacion;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
