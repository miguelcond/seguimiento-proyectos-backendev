'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    console.time('supervision');
    return migration.addColumn('supervision', 'fecha_pago',{
      type: DataTypes.DATE,
      xlabel: 'Fecha de Pago',
      allowNull: true
    },{
      returning: true, raw: true
    }).then((resp)=>{
      console.timeEnd('supervision');
    }).catch((e)=>{
      console.log(e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {

    const sql = (
      `ALTER TABLE supervision DROP COLUMN IF EXISTS fecha_pago;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
