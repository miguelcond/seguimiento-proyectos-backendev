'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    return migration.bulkUpdate('menu', {
      nombre: `REPORTES`,
      descripcion: `Reportes de proyectos`
    }, {
      nombre: 'REPORTE GENERAL',
      orden: '300'
    }, {
      returning: true, raw: true
    }).then((rows) => {
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    console.time('menu');
    return migration.bulkUpdate('menu', {
      nombre: `REPORTE GENERAL`,
      descripcion: `Reporte General`
    }, {
      nombre: 'REPORTES',
      orden: '300'
    }, {
      returning: true, raw: true
    }).then((rows) => {
      console.timeEnd('menu');
    }).finally(done);
  },
};
