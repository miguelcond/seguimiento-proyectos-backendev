'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let contenido_formulario = migration.sequelize.import('../models/flujo/contenido_formulario');
    console.time('contenido_formulario');
    return migration.bulkUpdate('contenido_formulario', {
      estructura: `[ {"cols": [{"rowspan": 2, "contenido": "Sostenibilidad técnica"}, {"contenido": "Requerimiento de componentes necesarios para la operación y/o funcionamiento del proyecto."}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]},
        {"cols": [{"contenido": "Distribución porcentual de los costos de inversión fija, capital de trabajo e inversión diferida."}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]}, 
        {"cols": [{"contenido": "Sostenibilidad social"}, {"contenido": "Actas de consenso y socialización"}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]}, 
        {"cols": [{"contenido": "Sostenibilidad organizativo institucional"}, {"contenido": "Actores y roles definidos para la operación, mantenimiento y administración del proyecto"}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]}, 
        {"cols": [{"rowspan": 3, "contenido": "Sostenibilidad económica"}, {"contenido": "Tiempo de operación del proyecto"}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]}, 
        {"cols": [{ "contenido": "Valor VACP / VANE" },{ "colspan": 2, "contenido": { "type": "inputMoney", "value": 0, "name": "valor_vane" } }]},
        {"cols": [{ "contenido": "Valor VACS / VANF" },{ "colspan": 2, "contenido": { "type": "inputMoney", "value": 0, "name": "valor_vanf" } }]},
        {"cols": [{"contenido": "Sostenibilidad ambiental"},{"contenido": "Dispensación o Evaluación del Impacto Ambiental"}, {"colspan": 2, "contenido": {"type": "select", "options":[{"codigo":"0","nombre":"El proyecto cuenta con certificado de dispensación ambiental"}, {"codigo":"1","nombre":"Se encuentra en proceso de tramite"}], "label":"Dispensación o Evaluación del Impacto Ambiental", "value": "El proyecto cuenta con certificado de dispensación ambiental"}}]},
        {"cols": [{"contenido": "Trabajo conjunto FPS"},{"contenido": "La evaluación de este proyecto fue realizada en colaboración con el FPS"}, {"colspan": 2, "contenido": {"type": "select", "options":[{"codigo":"0","nombre":"No"}, {"codigo":"1","nombre":"Si"}], "label":"evaluación realizada por el FPS", "value": "No"}}]}]`
    },{
      nombre: 'informe_evaluacion',
      seccion: '1'
    },{
      returning: true, raw: true
    }).then((rows) => {
      console.timeEnd('contenido_formulario');
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    let contenido_formulario = migration.sequelize.import('../models/flujo/contenido_formulario');
    console.time('contenido_formulario');
    return migration.bulkUpdate('contenido_formulario', {
      estructura: `[ {"cols": [{"rowspan": 2, "contenido": "Sostenibilidad técnica"}, {"contenido": "Requerimiento de componentes necesarios para la operación y/o funcionamiento del proyecto."}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]},
        {"cols": [{"contenido": "Distribución porcentual de los costos de inversión fija, capital de trabajo e inversión diferida."}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]}, 
        {"cols": [{"contenido": "Sostenibilidad social"}, {"contenido": "Actas de consenso y socialización"}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]}, 
        {"cols": [{"contenido": "Sostenibilidad organizativo institucional"}, {"contenido": "Actores y roles definidos para la operación, mantenimiento y administración del proyecto"}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]}, 
        {"cols": [{"rowspan": 3, "contenido": "Sostenibilidad económica"}, {"contenido": "Tiempo de operación del proyecto"}, {"contenido": {"name": "CUMPLE", "type": "radio", "value": false}}, {"contenido": {"name": "NO_CUMPLE", "type": "radio", "value": true}}]}, 
        {"cols": [{ "contenido": "Valor VACP / VANE" },{ "colspan": 2, "contenido": { "type": "inputMoney", "value": 0, "name": "valor_vane" } }]},
        {"cols": [{ "contenido": "Valor VACS / VANF" },{ "colspan": 2, "contenido": { "type": "inputMoney", "value": 0, "name": "valor_vanf" } }]},
        {"cols": [{"contenido": "Sostenibilidad ambiental"},{"contenido": "Dispensación o Evaluación del Impacto Ambiental"}, {"colspan": 2, "contenido": {"type": "select", "options":[{"codigo":"0","nombre":"El proyecto cuenta con certificado de dispensación ambiental"}, {"codigo":"1","nombre":"Se encuentra en proceso de tramite"}], "label":"Dispensación o Evaluación del Impacto Ambiental", "value": "El proyecto cuenta con certificado de dispensación ambiental"}}]}]`
    },{
      nombre: 'informe_evaluacion',
      seccion: '1'
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('contenido_formulario');
    }).finally(done);
  },
};
