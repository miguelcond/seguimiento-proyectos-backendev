'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        atributos_detalle: '{"modulo":{"acciones":["post","put"],"post":["fid_proyecto","fid_adjudicacion","nombre"],"put":["nombre"]},"item":{"acciones":["post","put"],"post":["fid_modulo","nombre","unidad_medida","cantidad","especificaciones","precio_unitario","peso_ponderado"],"put":["nombre","unidad_medida","cantidad","especificaciones","precio_unitario","peso_ponderado"]}}',
    }, {
      codigo_proceso: 'ADJ-FDI',
      codigo: 'ADJUDICADO_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {

    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        atributos_detalle: '{"modulo":{"acciones":["post","put"],"post":["fid_proyecto","fid_adjudicacion","nombre"],"put":["nombre"]},"item":{"acciones":["post","put"],"post":["fid_modulo","nombre","unidad_medida","cantidad","especificaciones","precio_unitario"],"put":["nombre","unidad_medida","cantidad","especificaciones","precio_unitario"]}}',
    }, {
      codigo_proceso: 'ADJ-FDI',
      codigo: 'ADJUDICADO_FDI'
    }, {
      returning: true, raw: true
    }).then((rows) => {

    }).finally(done);
  },
};
