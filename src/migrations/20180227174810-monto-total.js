'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
       `CREATE OR REPLACE FUNCTION public.fn_calcularMontoTotal(idProyecto integer)
      RETURNS text AS
      $BODY$
      DECLARE total double precision;
      DECLARE item RECORD;

      BEGIN
          -- verificando si existe el proyecto
          IF NOT EXISTS (select id_proyecto from proyecto WHERE id_proyecto = idProyecto)  THEN
             raise exception 'No existe el proyecto.';
          END IF;

          total = 0;

          -- recorriendo los items de un proyecto para calcular el monto total
          FOR item IN (select i.*
            from item as i
            inner join modulo as m on (i.fid_modulo = m.id_modulo)
            inner join proyecto as p on (m.fid_proyecto = p.id_proyecto)
            where i.estado <> 'ELIMINADO' and p.id_proyecto = idProyecto)
          LOOP

          RAISE NOTICE 'Start expensive step %: time %:to run=%', item.cantidad, item.precio_unitario, round(CAST(float8(item.cantidad * item.precio_unitario) as numeric), 2);

             total = round(CAST(float8 (total + round(CAST(float8(item.cantidad * item.precio_unitario) as numeric), 2)) as numeric),2);
        END LOOP;
       return round(CAST(float8(total) as numeric), 2);
      EXCEPTION WHEN OTHERS THEN
          return SQLERRM;
      END;
      $BODY$
        LANGUAGE plpgsql VOLATILE
        COST 100;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  }
};
