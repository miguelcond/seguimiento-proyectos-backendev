'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    let menu = migration.sequelize.import('../models/autenticacion/menu');
    menu.findOne({
      where: {
        nombre: 'ESTADOS'
      }
    }).then((result) => {
      if (!result) {
        migration.bulkInsert('menu',
          [{ nombre: 'ESTADOS', descripcion: 'Administración de estados del flujo', orden: 8, ruta: 'admin-estados', icono: 'list', method_get: true, method_post: false, method_put: true, method_delete: false, fid_menu_padre: 1, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {});
      } else {
        migration.bulkUpdate('menu', { nombre: 'ESTADOS', descripcion: 'Administración de estados del flujo', orden: 8, ruta: 'admin-estados', icono: 'list', method_get: true, method_post: false, method_put: true, method_delete: false, fid_menu_padre: 1, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        {
          nombre: 'ESTADOS'
        },{
          returning: true, raw: true
        });
      }
    });
    const sql = (
      ``
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `DELETE FROM menu WHERE nombre = 'ESTADOS';`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
