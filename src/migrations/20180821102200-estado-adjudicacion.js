'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
        `ALTER TABLE adjudicacion ADD COLUMN tipo_modificacion character varying(50);
        ALTER TABLE adjudicacion ADD COLUMN plazo_ampliacion json;
        ALTER TABLE adjudicacion ADD COLUMN doc_respaldo_modificacion character varying(100);
        ALTER TABLE adjudicacion ADD COLUMN fecha_modificacion timestamp with time zone;`
    );

    return migration.sequelize.query(sql)
      .catch((e)=>{
        console.log('E', e.message);
      })
      .finally(done);
  },

  down: (migration, DataTypes, done) => {

    const sql = (
      `ALTER TABLE adjudicacion DROP COLUMN IF EXISTS tipo_modificacion;
      ALTER TABLE adjudicacion DROP COLUMN IF EXISTS plazo_ampliacion;
      ALTER TABLE adjudicacion DROP COLUMN IF EXISTS doc_respaldo_modificacion;
      ALTER TABLE adjudicacion DROP COLUMN IF EXISTS fecha_modificacion;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
