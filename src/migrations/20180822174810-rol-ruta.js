'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {
    let rol_ruta = migration.sequelize.import('../models/autenticacion/rol_ruta');

    console.time('rol_ruta');
    return migration.bulkUpdate('rol_ruta',
      { fid_ruta: 8, fid_rol: 17, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: '1', _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      {
        fid_ruta: 8,
        fid_rol: 17
    },{
      returning: true, raw: true
    }).then((rows)=>{
      console.timeEnd('rol_ruta');
    }).finally(done);

  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};
