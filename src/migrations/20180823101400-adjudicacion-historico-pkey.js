'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {

    const sql = (
      ``
      // `ALTER TABLE adjudicacion_historico DROP CONSTRAINT IF EXISTS adjudicacion_historico_pkey;
      //  ALTER TABLE adjudicacion_historico ADD CONSTRAINT adjudicacion_historico_pkey PRIMARY KEY (id_adjudicacion, version);
      //  ALTER TABLE adjudicacion_historico ALTER COLUMN id_adjudicacion DROP DEFAULT;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {

    const sql = (
      ``
      // `ALTER TABLE adjudicacion_historico DROP CONSTRAINT IF EXISTS adjudicacion_historico_pkey;
      //  ALTER TABLE adjudicacion_historico ADD CONSTRAINT adjudicacion_historico_pkey PRIMARY KEY (id_adjudicacion);
      //  ALTER TABLE adjudicacion_historico ALTER COLUMN id_adjudicacion ADD DEFAULT nextval('adjudicacion_id_adjudicacion_seq'::regclass);`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
