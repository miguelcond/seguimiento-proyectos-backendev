'use strict';


module.exports = {
  up: (migration, DataTypes, done) => {

    let estado = migration.sequelize.import('../models/flujo/estado');
    return migration.bulkUpdate('estado', {
        atributos: '{"16":["orden_proceder","doc_especificaciones_tecnicas","modulos","estado_proyecto","adj"]}',
        atributos_detalle: '{"modulo":{"acciones":["post","put"],"post":["fid_proyecto","fid_adjudicacion","nombre"],"put":["nombre"]},"item":{"acciones":["post","put"],"post":["fid_modulo","nombre","unidad_medida","cantidad","especificaciones","precio_unitario"],"put":["nombre","unidad_medida","cantidad","especificaciones","precio_unitario"]}}',
        requeridos: '{"16":["orden_proceder","modulos"]}'
    },{
      codigo_proceso: 'ADJ-FDI',
      codigo: 'ADJUDICADO_FDI'
    },{
      returning: true, raw: true
    }).then((rows)=>{

    }).finally(done);

  },

  down: (migration, DataTypes, done) => {
    return migration.sequelize.query('')
      .finally(done);
  },
};
