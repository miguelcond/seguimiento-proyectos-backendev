'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    let rol_menu = migration.sequelize.import('../models/autenticacion/rol_menu');
    rol_menu.findOne({
      where: {
        fid_menu: 17,
        fid_rol: 1
      }
    }).then((result) => {
      if (!result) {
        migration.bulkInsert('rol_menu',
          [{ fid_menu: 17, fid_rol: 1, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() }]
        , {});
      } else {
        migration.bulkUpdate('rol_menu', { fid_menu: 17, fid_rol: 1, method_get: true, method_post: false, method_put: true, method_delete: false, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
        {
          fid_menu: 17,
          fid_rol: 1
        }, {
          returning: true, raw: true
        });
      }
    });
    const sql = (
      ``
    );

    return migration.sequelize.query(sql)
      .finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `DELETE FROM rol_menu WHERE fid_menu = 17 AND fid_rol = 1;`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
