'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    console.time('item');
    return migration.addColumn('item', 'cantidad_anterior_fis', {
      type: DataTypes.FLOAT,
      xlabel: 'Cantidad física acumulada en computos previos',
      allowNull: false,
      defaultValue: 0
    }, {
      returning: true, raw: true
    }).then((resp) => {
      return migration.addColumn('item', 'cantidad_actual_fis', {
        type: DataTypes.FLOAT,
        xlabel: 'Cantidad física actual de computos metricos',
        allowNull: false,
        defaultValue: 0
      }, {
        returning: true, raw: true
      });
    }).then((resp) => {
      console.timeEnd('item');
    }).catch((e) => {
      console.log(e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `ALTER TABLE item DROP COLUMN IF EXISTS cantidad_anterior_fis;
      ALTER TABLE item DROP COLUMN IF EXISTS cantidad_actual_fis;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
