'use strict';

module.exports = {
  up: async (migration, DataTypes, done) => {
    const Menu = migration.sequelize.import('../models/autenticacion/menu');
    const Rol = migration.sequelize.import('../models/autenticacion/rol');
    const RolMenu = migration.sequelize.import('../models/autenticacion/rol_menu');
    const menu1 = await Menu.findOne({
      attributes: ['id_menu'],
      where: {
        estado: 'ACTIVO',
        ruta: 'lista-proyectos',
      },
      raw: true,
    });
    const menu2 = await Menu.findOne({
      attributes: ['id_menu'],
      where: {
        estado: 'ACTIVO',
        ruta: 'lista-proyectos-municipio',
      },
      raw: true,
    });
    const rol1 = await Rol.findOne({
      attributes: ['id_rol'],
      where: {
        estado: 'ACTIVO',
        nombre: 'VISOR_REPORTES',
      },
      raw: true,
    });
    console.log('++++', menu1, menu2, rol1);
    const res = await migration.bulkInsert('rol_menu', [
      { method_get: true, fid_menu: menu1.id_menu, fid_rol: rol1.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
      { method_get: true, fid_menu: menu2.id_menu, fid_rol: rol1.id_rol, estado: 'ACTIVO', _usuario_creacion: 1, _fecha_creacion: new Date(), _fecha_modificacion: new Date() },
    ], { returning: true, raw: true }).catch(e => { console.error(e.message); });
    console.log('====', res);
  },

  down: (migration, DataTypes, done) => {
    const sql = ``;
    return migration.sequelize.query(sql)
      .finally(done);
  }
};