'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    let parametrica = migration.sequelize.import('../models/parametrica/parametrica');
    return parametrica.findOne({
      where: {
        codigo: 'OP_TP_MAQUINARIA'
      }
    }).then((result) => {
      if (!result) {
        migration.bulkInsert('parametrica',
          [{
            codigo: 'OP_TP_MAQUINARIA',
            tipo: 'OP_TIPO_PROYECTO',
            nombre: 'Maquinaria',
            descripcion: 'Proyecto de adquisición de maquinaria',
            orden: 4,
            _usuario_creacion: 1,
            _fecha_creacion: new Date(),
            _fecha_modificacion: new Date()
          }]
        , {});
      } else {
        migration.bulkUpdate('parametrica',
        {
          tipo: 'OP_TIPO_PROYECTO',
          nombre: 'Maquinaria',
          descripcion: 'Proyecto de adquisición de maquinaria',
          orden: 4,
          _usuario_creacion: 1,
          _fecha_creacion: new Date(),
          _fecha_modificacion: new Date()
        },
        {
          codigo: 'OP_TP_MAQUINARIA'
        }, {
          returning: true, raw: true
        });
      }
    }).catch((e) => {
      console.error('E', e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {
    const sql = (
      `DELETE FROM parametrica WHERE codigo = 'OP_TP_MAQUINARIA';`
    );

    return migration.sequelize.query(sql)
      .finally(done);
  }
};
