'use strict';

module.exports = {
  up: (migration, DataTypes, done) => {
    console.time('supervision');
    return migration.addColumn('supervision', 'version_adjudicacion',{
      type: DataTypes.INTEGER,
      xlabel: 'Versión de la Supervisión',
      allowNull: true
    },{
      returning: true, raw: true
    }).then((resp)=>{
      console.timeEnd('supervision');
    }).catch((e)=>{
      console.log(e.message);
    }).finally(done);
  },

  down: (migration, DataTypes, done) => {

    const sql = (
      `ALTER TABLE supervision DROP COLUMN IF EXISTS version_adjudicacion;`
    );
    return migration.sequelize.query(sql)
      .finally(done);
  }
};
