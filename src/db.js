import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';
import cls from 'continuation-local-storage'; // added by fcarreno
import { field as FIELD } from './lib/apidoc-generator';

let db = null;
module.exports = (app) => {
  const nameSpace = cls.createNamespace('transaccion'); // added by fcarreno
  const config = app.src.config.config.database;
  if (!db) {
    Sequelize.useCLS(nameSpace) // Sequelize.cls = nameSpace; // added by fcarreno
    Sequelize.postgres.DECIMAL.parse = function (value) { return parseFloat(value); };
    config.params.operatorsAliases = Sequelize.Op.Aliases;
    const sequelize = new Sequelize(
      config.name,
      config.username,
      config.password,
      config.params
    );

    db = {
      sequelize,
      Sequelize,
      models: {}
    };

    const dirModels = path.join(__dirname, 'models');
    // Obtiene los modelos del directorio 'models'.
    fs.readdirSync(dirModels).forEach((dir) => {
      if (fs.statSync(`${dirModels}/${dir}`).isDirectory()) {
        const subDirModels = path.join(dirModels, dir);
        fs.readdirSync(subDirModels).forEach((file) => {
          const pathFile = path.join(subDirModels, file);
          const model = sequelize.import(pathFile);
          // Almacena los objetos modelo en un JSON.
          db.models[model.name] = model;
        });
      } else {
        const pathFile = path.join(dirModels, dir);
        const model = sequelize.import(pathFile);
        // Almacena los objetos modelo en un JSON.
        db.models[model.name] = model;
      }
    });

    // console.log('cargando asociaciones....');
    Object.keys(db.models).forEach((key) => {
      // console.log(`---->${key + db.models[key]}`);
      // Control de relaciones(associate) de los modelos.
      if (db.models[key].associate !== undefined) {
        db.models[key].associate(db.models);
      }
    });

    FIELD.import(sequelize);
    FIELD.add('custom', {
      finalizado: { type: Sequelize.BOOLEAN(), comment: 'Estado que indica si la tarea se completó correctamente o hubo algun error.', example: true },
      mensaje: { type: Sequelize.STRING(), comment: 'Mensaje que explica el resultado.', example: 'Tarea completada exitosamente.' },
      token: { type: Sequelize.STRING(), comment: 'Token de acceso.', example: 'eyJ0eXAiOiJKV1Q...' },
      usuario: { type: Sequelize.STRING(), comment: 'Usuario MASI.', example: 'LABOCAST' },
      nombres: { type: Sequelize.STRING(), comment: 'Nombre(s) de la persona.', example: 'JHON' },
      apellidos: { type: Sequelize.STRING(), comment: 'Apellido(s) de la persona.', example: 'SMITH SMITH' },
      clave: { type: Sequelize.STRING(), comment: 'Clave del usuario MASI.', example: 'abc.123' },
      menuEntrar: { type: Sequelize.STRING(), comment: 'Menú al que accederá el usuario.', exmaple: '/mis-proyectos' },
      codigo_documento: {
        type: Sequelize.STRING(10),
        comment: 'Código asignado a un documento, a partir de su nombre y un número secuencial en caso de que exista mas de un documento.<br>' +
          'Ejemplos:<br>' +
          '- CIP  = Certificado de Inscripción Presupuestaria.<br>' +
          '- PA_1 = Planilla de Avance de obra Nro 1.<br>',
        example: 'CIP' },
      tipo_documento: { type: Sequelize.STRING(), comment: 'Tipo de documento.', example: 'application/pdf' },
      base64_documento: { type: Sequelize.STRING(), comment: 'Contenido del documento en formato Base 64.', example: 'JVBERi0xLjQKJcOkw7zDts' },
      nombre_documento: { type: Sequelize.STRING(), comment: 'Nombre completo del documento.', example: 'Boleta de pago' },
      ruta_documento: { type: Sequelize.STRING(), comment: 'Nombre del archivo del documento.', example: 'boleta_pago.pdf' }
    });
  }

  return db;
};
